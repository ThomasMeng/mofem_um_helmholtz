#!/bin/sh
# " chmod -x FILE_NAME " before run the bach script
DIR_NAME="error_mesh_file"
NB_PROC=1
ERROR_TYPE="l2"
WAVE_NUMBER="3"

# for WAVE_NUMBER in {3,5,10}
# do


#ERROR_TYPE="h1"

# BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
# SRC_PATH="@CMAKE_CURRENT_SOURCE_DIR@"
BIN_PATH="./"
SRC_PATH="./src"

MPIRUN="mpirun -np $NB_PROC"

cd $BIN_PATH

for WAVE_NUMBER in 3 5 10
do

# echo LOG: plane wave guide P convergence test
# echo | tee -a wave_guide_p.txt
# echo "Start p convergence test for plane wave guide ..." | tee -a wave_guide_p.txt
# $MPIRUN $BIN_PATH/best_approximation \
# -my_file plane_wave_cube.cub \
# -my_is_partitioned false \
# -wave_number $WAVE_NUMBER \
# -wave_direction 0.7071,0.7071,0 \
# -analytical_solution_type plane_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 7 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -wave_guide_angle 45
#
# for ODISP in {1..3..1};
# do
#   let NB_PROC=${ODISP}+1
#   #echo $NB_PROC
#   #BEGIN analytical solution
#   # $MPIRUN $BIN_PATH/best_approximation \
#   # -my_file plane_wave_cube.cub \
#   # -my_is_partitioned false \
#   # -wave_number $WAVE_NUMBER \
#   # -wave_direction 0.7071,0.7071,0 \
#   # -analytical_solution_type plane_wave \
#   # -save_postproc_mesh false \
#   # -ksp_type fgmres \
#   # -pc_type lu \
#   # -pc_factor_mat_solver_package superlu_dist \
#   # -ksp_monitor \
#   # -my_order $ODISP \
#   # -my_max_post_proc_ref_level 0 \
#   # -add_incident_wave false \
#   # -wave_guide_angle 45
#
#   #BEGIN numerical solution
#   $MPIRUN $BIN_PATH/fe_approximation \
#   -my_file analytical_solution.h5m \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 0.7071,0.7071,0 \
#   -analytical_solution_type plane_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps \
#   -ksp_monitor \
#   -my_order $ODISP \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -wave_guide_angle 45 |
#   grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =" | tee -a -i wave_guide_p.txt
#   #BEGIN error calculation
#
#   $MPIRUN $BIN_PATH/error_norm \
#   -my_file $BIN_PATH/fe_solution.h5m \
#   -norm_type $ERROR_TYPE \
#   -relative_error false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package superlu_dist  \
#   -ksp_monitor  \
#   -my_order 1 \
#   -my_max_post_proc_ref_level 0 \
#   -save_postproc_mesh true \
#   2>&1 | grep --line-buffered -i " $ERROR_TYPE realtive error " | tee -a -i wave_guide_p.txt
#   #RENAME files inot their order and wave number.
#   mbconvert norm_error.h5m norm_error.vtk
#   mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ODISP}.vtk
# done
#
# #SAVE the results to different directory
# mkdir ${DIR_NAME}_${ERROR_TYPE}_waveguide | mv norm_error_k${WAVE_NUMBER}_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_waveguide
rm -rf ${DIR_NAME}_${ERROR_TYPE}_sphere_k${WAVE_NUMBER}_h
rm -rf hard_sphere_h_$WAVE_NUMBER.txt | rm -rf norm_error_k${WAVE_NUMBER}_h_order_*.vtk
echo LOG: sound hard sphere P convergence test
echo | tee -a hard_sphere_h_$WAVE_NUMBER.txt
echo "Start h convergence test for sound hard sphere with k = $WAVE_NUMBER ..." | tee -a hard_sphere_h_$WAVE_NUMBER.txt

# for batman in `seq 1 1`;
# do

  #echo $NB_PROC
  #BEGIN analytical solution
# mpirun -np 3 ./fe_approximation \
# -my_file impinging_sphere.cub \
# -my_is_partitioned false \
# -wave_number 3 \
#  -wave_direction 1,0,0 \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 2 \
# -my_max_post_proc_ref_level 0 \
# -amplitude_of_incident_wave 1 \
# -add_incident_wave false
# done

# for ODISP in {1..7..1};
for ODISP in `seq 1 7`;
do
 # let NB_PROC+=${ODISP}
 NB_PROC=`expr $ODISP + 1`
 MPIRUN="mpirun -np $NB_PROC"

$MPIRUN $BIN_PATH/best_approximation \
   -my_file impinging_sphere_$ODISP.cub \
   -my_is_partitioned false \
   -wave_number $WAVE_NUMBER \
   -wave_direction 1,0,0 \
   -analytical_solution_type hard_sphere_scatter_wave \
   -save_postproc_mesh false \
   -ksp_type fgmres \
   -pc_type lu \
   -pc_factor_mat_solver_package superlu_dist \
   -ksp_monitor \
   -my_order 2 \
   -my_max_post_proc_ref_level 0 \
   -add_incident_wave false \
   -amplitude_of_incident_wave 1

#
#BEGIN numerical solution
echo "                                   "| tee -a hard_sphere_h_$WAVE_NUMBER.txt
echo "                                   "| tee -a hard_sphere_h_$WAVE_NUMBER.txt
echo "Start numerical calculation with $NB_PROC processes, and order $ODISP ..." | tee -a hard_sphere_h_$WAVE_NUMBER.txt

$MPIRUN $BIN_PATH/fe_approximation \
-my_file analytical_solution.h5m \
-my_is_partitioned false \
-wave_number $WAVE_NUMBER \
 -wave_direction 1,0,0 \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order 2 \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-add_incident_wave false 2>&1 |
grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =" | tee -a -i hard_sphere_h_$WAVE_NUMBER.txt
echo "   "
echo "                       begin error calculation           "
echo "   "
#BEGIN error calculation
mpirun -np 2 $BIN_PATH/error_norm \
-my_file $BIN_PATH/fe_solution.h5m \
-norm_type $ERROR_TYPE \
-relative_error false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist  \
-ksp_monitor  \
-my_order 1 \
-my_max_post_proc_ref_level 0 \
-save_postproc_mesh true 2>&1 | grep --line-buffered -i " realtive error " | tee -a -i hard_sphere_h_$WAVE_NUMBER.txt
#RENAME files inot their order and wave number.
mbconvert norm_error.h5m norm_error.vtk |
mv norm_error.vtk norm_error_k${WAVE_NUMBER}_h_order_${ODISP}.vtk
done

#SAVE the results to different directory
mkdir ${DIR_NAME}_${ERROR_TYPE}_sphere_k${WAVE_NUMBER}_h
mv norm_error_k${WAVE_NUMBER}_h_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_sphere_k${WAVE_NUMBER}_h

# done
#foo="Hello"
#foo="$foo World"
#echo $foo
#> Hello World
done
