#!/bin/sh
BEST_NB_PRC="8"
# " chmod -x FILE_NAME " before run the bach script
DIR_NAME="error_mesh_file"
NB_PROC="8"
ERROR_TYPE="h1"
WAVE_NUMBER="0"
ODISP="1"
ORDER="adaptivity"

# for WAVE_NUMBER in {3,5,10}
# do
partition="false"
DUFFY="true"
LOBATTO="true"
ERRORLV="0"
PRIORI_TYPE="1"
# file1="impinging_cylinder_$ODISP.cub"
# file2="analytical_solution.h5m"
#ERROR_TYPE="h1"

# BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
# SRC_PATH="@CMAKE_CURRENT_SOURCE_DIR@"
# BIN_PATH="./"
# SRC_PATH="./src"

MPIRUN="mpirun -np $NB_PROC"

# cd $BIN_PATH



# echo LOG: plane wave guide P convergence test
# echo | tee -a wave_guide_p.txt
# echo "Start p convergence test for plane wave guide ..." | tee -a wave_guide_p.txt
# $MPIRUN $BIN_PATH/best_approximation \
# -my_file plane_wave_cube.cub \
# -my_is_partitioned false \
# -wave_number $WAVE_NUMBER \
# -wave_direction 0.7071,0.7071,0 \
# -analytical_solution_type plane_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package mumps \
# -ksp_monitor \
# -my_order 7 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -wave_guide_angle 45
#
# for ODISP in {1..3..1};
# do
#   let NB_PROC=${ODISP}+1
#   #echo $NB_PROC
#   #BEGIN analytical solution
#   # $MPIRUN $BIN_PATH/best_approximation \
#   # -my_file plane_wave_cube.cub \
#   # -my_is_partitioned false \
#   # -wave_number $WAVE_NUMBER \
#   # -wave_direction 0.7071,0.7071,0 \
#   # -analytical_solution_type plane_wave \
#   # -save_postproc_mesh false \
#   # -ksp_type fgmres \
#   # -pc_type lu \
#   # -pc_factor_mat_solver_package mumps \
#   # -ksp_monitor \
#   # -my_order $ODISP \
#   # -my_max_post_proc_ref_level 0 \
#   # -add_incident_wave false \
#   # -wave_guide_angle 45
#
#   #BEGIN numerical solution
#   $MPIRUN $BIN_PATH/fe_approximation \
#   -my_file analytical_solution.h5m \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 0.7071,0.7071,0 \
#   -analytical_solution_type plane_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps \
#   -ksp_monitor \
#   -my_order $ODISP \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -wave_guide_angle 45 |
#   grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =" | tee -a -i wave_guide_p.txt
#   #BEGIN error calculation
#
#   $MPIRUN $BIN_PATH/error_norm \
#   -my_file $BIN_PATH/fe_solution.h5m \
#   -norm_type $ERROR_TYPE \
#   -relative_error false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps  \
#   -ksp_monitor  \
#   -my_order 1 \
#   -my_max_post_proc_ref_level 0 \
#   -save_postproc_mesh true \
#   2>&1 | grep --line-buffered -i " $ERROR_TYPE realtive error " | tee -a -i wave_guide_p.txt
#   #RENAME files inot their order and wave number.
#   mbconvert norm_error.h5m norm_error.vtk
#   mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ODISP}.vtk
# done
#
# #SAVE the results to different directory
# mkdir ${DIR_NAME}_${ERROR_TYPE}_waveguide | mv norm_error_k${WAVE_NUMBER}_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_waveguide
rm -rf test_best_approximation.txt
rm -rf ${DIR_NAME}_${ERROR_TYPE}_cylinder_p
rm -rf error_hard_cylinder_p_meshsize_*_lobatto_${LOBATTO}_errorlv_*.txt
rm -rf hard_cylinder_p_meshsize_*_lobatto_${LOBATTO}_errorlv_*.txt | rm -rf norm_error_k*_order_*.vtk


# for batman in `seq 1 1`;
# do

  #echo $NB_PROC
  #BEGIN analytical solution
# mpirun -np 8 ./best_approximation \
#   -my_file impinging_cylinder.cub \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 1,0,0 \
#   -analytical_solution_type hard_cylinder_incident_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps \
#   -ksp_monitor \
#   -my_order 7 \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -amplitude_of_incident_wave 1
# done

# for ODISP in `seq 1 4`;
for ODISP in "1" "3" "4" "5"
do

# BEST_NB_PRC=`expr 7 + $ODISP`


# for ERRORLV in "0" "1" "2"
for ERRORLV in "0" "1" "2"
do


COUNT="0"


# echo LOG: sound hard cylinder P convergence test
echo | tee -a test_best_approximation.txt
echo | tee -a hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
echo | tee -a error_hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
echo " avg facm nze max min DOFs memory time CPUtime              "| tee -a hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
echo "${ERROR_TYPE}relative error                "| tee -a error_hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
echo "Start p convergence test for sound hard cylinder ..."

# if [ $partition = true ]; then mbpart -t -p PARTKWAY $NB_PROC  impinging_cylinder.cub  impinging_cylinder_parts$NB_PROC.h5m; fi

# if [ $partition = true ]
# then file1="impinging_cylinder_parts$NB_PROC.h5m" && file2="best_solution.h5m"
# fi

# for WAVE_NUMBER in "0.125663706143592" "0.987976724163411" "1.85028974218323" "2.71260276020305" "3.57491577822287" "4.43722879624269" "5.29954181426251" "6.16185483228233" "7.02416785030214" "7.88648086832196" "8.74879388634178" "9.61110690436160" "10.4734199223814" "11.3357329404012" "12.1980459584211" "13.0603589764409" "13.9226719944607" "14.7849850124805" "15.6472980305003" "16.5096110485202" "17.3719240665400" "18.2342370845598"	"19.0965501025796" "19.9588631205994" "20.8211761386193" "21.6834891566391" "22.5458021746589" "23.4081151926787" "24.2704282106985" "25.1327412287183"
for WAVE_NUMBER in "0.125663706143592" "0.636012227012464" "1.14636074788134" "1.65670926875021" "2.16705778961908" "2.67740631048795" "3.18775483135682" "3.69810335222569" "4.20845187309457" "4.71880039396344" "5.22914891483231" "5.73949743570118" "6.24984595657005" "6.76019447743892" "7.27054299830779"	"7.78089151917667" "8.29124004004554" "8.80158856091441" "9.31193708178328" "9.82228560265215" "10.3326341235210" "10.8429826443899" "11.3533311652588" "11.8636796861276" "12.3740282069965" "12.8843767278654" "13.3947252487343" "13.9050737696031" "14.4154222904720" "14.9257708113409" "15.4361193322097" "15.9464678530786" "16.4568163739475" "16.9671648948164" "17.4775134156852" "17.9878619365541" "18.4982104574230" "19.0085589782918" "19.5189074991607" "20.0292560200296" "20.5396045408985" "21.0499530617673" "21.5603015826362" "22.0706501035051" "22.5809986243739" "23.0913471452428" "23.6016956661117" "24.1120441869806" "24.6223927078494" "25.1327412287183"
do


echo "Start best_approximation calculation with $BEST_NB_PRC processes, mesh h$ODISP and order 8 and wavenumber $WAVE_NUMBER, error level $ERRORLV"

#BEGIN analytical solution
mpirun -np $BEST_NB_PRC ./best_approximation \
-my_file impinging_cylinder_$ODISP.cub \
-my_is_partitioned false \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-analytical_solution_type hard_cylinder_scatter_wave \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order 8 \
-my_max_post_proc_ref_level 0 \
-add_incident_wave false \
-duffy true \
-lobatto true 2>&1 |
echo $(grep --line-buffered -i "Residual norm\|Time =" | sort | uniq) | tee -a -i test_best_approximation.txt



# for ORDER in {1..7..1};
# for ORDER in `seq 1 8`;
# do
 # let NB_PROC+=${ORDER}
 # NB_PROC=`expr $ORDER + 1`
# if [ "$ORDER" -gt "$ODISP" ]
# then NB_PROC=`expr $ORDER + 1`
# else NB_PROC=`expr $ODISP + 1`
# fi
# $(echo $(( 2 * $ODISP )))
 # NB_PROC=`expr $ODISP + $(echo $(( 2 * $ERRORLV ))) + 1`


COUNT=`expr $COUNT + 1`
if [ "$COUNT" -gt "8" ]
then NB_PROC=`expr 8`
else NB_PROC=`expr 2`
fi
echo NB_PROC = $NB_PROC
MPIRUN="mpirun -np $NB_PROC"


 #To use partitioned mesh, first mbpart -t -p PARTKWAY 4  cylinder4.cub  cylinder4_4parts.h5m,
#   let NB_PROC=${ORDER}+1
#   #echo $NB_PROC
  # #BEGIN analytical solution
  # $MPIRUN $BIN_PATH/best_approximation \
  # -my_file $file1 \
  # -my_is_partitioned $partition \
  # -wave_number $WAVE_NUMBER \
  # -wave_direction 1,0,0 \
  # -analytical_solution_type hard_cylinder_incident_wave \
  # -save_postproc_mesh false \
  # -ksp_type fgmres \
  # -pc_type lu \
  # -pc_factor_mat_solver_package mumps \
  # -ksp_monitor \
  # -my_order $ORDER \
  # -my_max_post_proc_ref_level 0 \
  # -add_incident_wave false \

#
#BEGIN numerical solution
#echo "                      "| tee -a hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
#echo "${ERROR_TYPE}relative error                "| tee -a error_hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
#echo "                                   "| tee -a hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
#echo "Start numerical calculation with $NB_PROC processes, mesh h$ODISP and order $ORDER ..." | tee -a hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
echo "Start priori estimator numerical calculation with $NB_PROC processes, mesh h$ODISP and order $ORDER and wavenumber $WAVE_NUMBER, error level $ERRORLV"
mpirun -np $NB_PROC ./fe_approximation \
-my_file analytical_solution.h5m \
-my_is_partitioned false \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order 1 \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-duffy $DUFFY \
-lobatto $LOBATTO \
-adaptivity true \
-error_level $ERRORLV \
-error_type $PRIORI_TYPE \
-add_incident_wave false 2>&1 |
echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$4,$6,$8,$10,$15,$25,$32,$37}' | tee -a -i hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
echo "   "
echo "                       begin error calculation           "
echo "   "
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$4,$6,$8,$10,$15,$25,$32,$37}' | tee -a -i hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$25,$29,$30}' | tee -a -i hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
#BEGIN error calculation   awk '{print $6,$12}'  grep first 10 lines |head -10|
#grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =\|Memory usage =" | tee -a -i hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt

mpirun -np 2 ./error_norm \
-my_file ./fe_solution.h5m \
-my_is_partitioned false \
-norm_type $ERROR_TYPE \
-relative_error false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order 1 \
-my_max_post_proc_ref_level 0 \
-save_postproc_mesh true 2>&1 | echo $(grep --line-buffered -i " realtive error ") | awk '{print $6,$12}' | tee -a -i error_hard_cylinder_p_meshsize_${ODISP}_lobatto_${LOBATTO}_errorlv_${ERRORLV}.txt
#RENAME files inot their order and wave number.
mbconvert norm_error.h5m norm_error.vtk |
mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ORDER}.vtk
rm -rf norm_error_k${WAVE_NUMBER}_order_${ORDER}.vtk 
done

done

done

#SAVE the results to different directory
mkdir ${DIR_NAME}_${ERROR_TYPE}_cylinder_p
mv norm_error_k*_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_cylinder_p


# done
#foo="Hello"
#foo="$foo World"
#echo $foo
#> Hello World
