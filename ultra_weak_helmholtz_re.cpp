/** \file ultra_weak_helmholtz_re.cpp
\brief Example implementation of helmholtz problem using ultra-week formulation

\todo Should be implemented and tested problem from this article
Demkowicz, Leszek, and Jayadeep Gopalakrishnan. "Analysis of the DPG method for
the Poisson equation." SIAM Journal on Numerical Analysis 49.5 (2011):
1788-1809.

\ingroup mofem_ultra_weak_helmholtz_elem
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
#include <MoFEM.hpp>
using namespace MoFEM;

#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/symmetric.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/ptr_container/ptr_map.hpp>

#include <MethodForForceScaling.hpp>
#include <DirichletBC.hpp>
#include <PostProcOnRefMesh.hpp>

#include <Projection10NodeCoordsOnField.hpp>
#include <petsctime.h>
#include <fstream>
#include <iostream>
#include <valarray>

#include <stdexcept>
#include <cmath>
#include <boost/math/special_functions.hpp>
#include <complex>

#include <FieldApproximation.hpp>

using namespace std;
using namespace boost::math;

// #include <boost/shared_array.hpp>
// #include <kiss_fft.h>
// #include <kiss_fft.c>

#include <AnalyticalDirichletHelmholtz.hpp>
#include <AnalyticalSolutions.hpp>
// #include <HelmholtzElement.hpp>
// #include <ReynoldsStress.hpp>
// #include <SecondOrderStress.hpp>
// #include <TimeSeries.hpp>

static char help[] = "...\n\n";

struct PlaneIncidentWaveSacttrerData {

  Range tRis;

};

/**
 * Data structure to pass information between function evaluating boundary
 * values and fluxes and generic data structures for boundary conditions on
 * meshsets.
 */
// struct BcFluxData {
//   Range eNts;
//   double fLux;
// };
// typedef map<int,BcFluxData> BcFluxMap;

/** \brief Surface element data
* \ingroup mofem_ultra_weak_helmholtz_elem
*/
struct SurfaceData {

  double aDmittance_real;
  double aDmittance_imag;
  Range tRis; ///< surface triangles where hate flux is applied

};
typedef map<int,SurfaceData> surfaceIncidentWaveBcData;
// map<int,SurfaceData> surfaceIncidentWaveBc2Data;
// map<int,SurfaceData> surfaceHomogeneousBcData;
// map<int,SurfaceData> surfaceHomogeneousBc2Data;
typedef map<int,SurfaceData> sommerfeldBcData;
typedef map<int,SurfaceData> baylissTurkelBcData;

/** \brief Application of ultraweak data structure
  *
  * UltraWeakHelmholtzElement is a class collecting functions, operators and
  * data for ultra week implementation of helmholtz element. See there to
  * learn how elements are created or how operators look like.
  *
  * Some methods in UltraWeakHelmholtzElement are abstract, f.e. user need to
  * implement own source therm.

  * \ingroup mofem_ultra_weak_helmholtz_elem
  */
struct HelmholtzUltraWeak: public UltraWeakTransportElement {

  BcFluxMap &bcFluxMap;
  EntityHandle lastEnt;
  double lastFlux;

  HelmholtzUltraWeak(MoFEM::Interface &m_field,BcFluxMap &bc_flux_map):
  UltraWeakTransportElement(m_field),
  bcFluxMap(bc_flux_map),
  lastEnt(0),
  lastFlux(0) {
  }

  // /**
  //  * \brief set source term
  //  * @param  ent  handle to entity on which function is evaluated
  //  * @param  x    coord
  //  * @param  y    coord
  //  * @param  z    coord
  //  * @param  flux reference to source term set by function
  //  * @return      error code
  //  */
  // PetscErrorCode getSource(EntityHandle ent,const double x,const double y,const double z,double &flux) {
  //   PetscFunctionBegin;
  //   flux = 0;
  //   PetscFunctionReturn(0);
  // }
  //
  // /**
  //  * \brief natural (Dirihlet) boundary conditions (set values)
  //  * @param  ent   handle to finite element entity
  //  * @param  x     coord
  //  * @param  y     coord
  //  * @param  z     coord
  //  * @param  value reference to value set by function
  //  * @return       error code
  //  */
  // PetscErrorCode getBcOnValues(
  //   const EntityHandle ent,
  //   const double x,const double y,const double z,
  //   double &value) {
  //   PetscFunctionBegin;
  //   value = 0;
  //   PetscFunctionReturn(0);
  // }

  /**
   * \brief essential (Neumann) boundary condition (set fluxes)
   * @param  ent  handle to finite element entity
   * @param  x    coord
   * @param  y    coord
   * @param  z    coord
   * @param  flux reference to flux which is set by function
   * @return      [description]
   */
  PetscErrorCode getBcOnFluxes(
    const EntityHandle ent,
    const double x,const double y,const double z,
    double &flux) {
    PetscFunctionBegin;

    GlobalParameters &globalParameters;
    VectorDouble cOordinate;
    VectorDouble vAl;

    if(lastEnt==ent) {
      flux = lastFlux;
    } else {
      flux = 0;
      for(BcFluxMap::iterator mit = bcFluxMap.begin();mit!=bcFluxMap.end();mit++) {
        Range &tris = mit->second.eNts;
        if(tris.find(ent)!=tris.end()) {
          flux = mit->second.fLux;
        }
      }
      lastEnt = ent;
      lastFlux = flux;
    }
    PetscFunctionReturn(0);
  }

  /** \brief Calculate incident wave scattered on hard surface
      \ingroup mofem_helmholtz_elem

    This part shows the Neumann boundary condition of the exterior boundary
    value problem for the rigid scatterer.

    \f]
    \left. \left\{ \mathbf{n} \cdot  (ik\mathbf{d} A_{0} e^{ik \mathbf{d} \cdot \mathbf{x}})
    \right\}
    \f[

    where \f$\mathbf{n})\f$ is the normal vector point outward of the surface of scatterer.
      \f$\mathbf{x})\f$ is the cartesian coordinates and \f$d\f$ is the unit vector represents the
      direction of the incident wave. Further more


    */
  struct IncidentWaveNeumannF2 {

    GlobalParameters &globalParameters;
    IncidentWaveNeumannF2(GlobalParameters &global_parameters): globalParameters(global_parameters) {}

    VectorDouble cOordinate;
    VectorDouble vAl;

    VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

      const complex< double > i( 0.0, 1.0 );

      cOordinate.resize(3);
      cOordinate[0] = x;
      cOordinate[1] = y;
      cOordinate[2] = z;
      complex< double > wave_number = globalParameters.waveNumber.first*(1.0+i*globalParameters.lossTangent.first) + i*globalParameters.complexWaveNumber.first;
      complex< double > attenuation(0,0);
      complex<double> p_inc = 0;
      // attenuation.real(0);
      // attenuation.imag(0);
      if(globalParameters.isRayleigh.first) {
        /* speed of SAW over speed of longitudinal wave */
        complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
        attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
      }

      VectorDouble &oscilation_direction = globalParameters.waveOscilationDirection.first;
      double x1d = inner_prod(globalParameters.waveDirection.first,cOordinate);
      complex<double> amplitude = globalParameters.amplitudeOfIncidentWaveReal.first+i*globalParameters.amplitudeOfIncidentWaveImag.first;
      complex<double> angle = wave_number*(x1d);
      if(globalParameters.isRadiation.first){
        p_inc = amplitude*exp(-i*angle+wave_number*attenuation);
      } else {
        p_inc = amplitude*exp(i*angle+wave_number*attenuation);
      }

      ublas::vector<complex<double > > grad(3);
      for(int ii = 0;ii<3;ii++) {
        grad[ii] = i*wave_number*oscilation_direction[ii]*p_inc;
      }
      complex<double > grad_n = globalParameters.transmissionCoefficient.first*inner_prod(grad,normal);
      // if(globalParameters.isRadiation.first){
      //   grad_n *= -1.;
      // }
      //// check if normal pointing to ceneter;
      //double dot = -inner_prod(normal,cOordinate);
      //if(dot < 0) grad_n *= -1;

      vAl.resize(2);
      vAl[0] = std::real(grad_n);
      vAl[1] = std::imag(grad_n);

      return vAl;
    }

  };


  /**
   * \bried set-up boundary conditions
   * @param  ref_level mesh refinement level

   \note It is assumed that user would like to something non-standard with boundary
   conditions, have a own type of data structures to pass to functions calculating
   values and fluxes on boundary. For example BcFluxMap. That way this function
   is implemented here not in generic class UltraWeakTransportElement.

   * @return           error code
   */
  PetscErrorCode addBoundaryElements(BitRefLevel &ref_level) {
    MoABErrorCode rval;
    PetscErrorCode ierr;
    PetscFunctionBegin;
    Range tets;
    ierr = mField.get_entities_by_type_and_ref_level(ref_level,BitRefLevel().set(),MBTET,tets);
    Skinner skin(&mField.get_moab());
    Range skin_faces; // skin faces from 3d ents
    rval = skin.find_skin(0,tets,false,skin_faces); CHKERR_MOAB(rval);
    // note: what is essential (dirichlet) is natural (neumann) for ultra weak compared to classical FE
    // note: incident wave for UW helmholtz operator is natural Dirichlet BC applied on F
    Range natural_bc,impedance_bc,essential_bc;


    for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {

      if(it->getName().compare(0,23,"SOFT_INCIDENT_WAVE_BC") == 0) {

        surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_real = 0;
        surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_imag = 0;
        globalParameters.isIncidentWave.first = PETSC_TRUE;

        ierr = it->getMeshsetIdEntitiesByDimension(mField.get_moab(),2,surfaceIncidentWaveBcData[it->getMeshsetId()].tRis,true); CHKERRQ(ierr);
        // ierr = mField.add_ents_to_finite_element_by_TRIs(surfaceIncidentWaveBcData[it->getMeshsetId()].tRis,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);
        natural_bc.insert(surfaceIncidentWaveBcData[it->getMeshsetId()].tRis.begin(),surfaceIncidentWaveBcData[it->getMeshsetId()].tRis.end());
        // rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,surfaceIncidentWaveBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);

      }

      if(it->getName().compare(0,13,"SOMMERFELD_BC") == 0) {

        sommerfeldBcData[it->getMeshsetId()].aDmittance_real = 0;
        sommerfeldBcData[it->getMeshsetId()].aDmittance_imag = -globalParameters.waveNumber.first;

        // rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,sommerfeldBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = it->getMeshsetIdEntitiesByDimension(mField.get_moab(),2,sommerfeldBcData[it->getMeshsetId()].tRis,true); CHKERRQ(ierr);
        impedance_bc.insert(sommerfeldBcData[it->getMeshsetId()].tRis.begin(),sommerfeldBcData[it->getMeshsetId()].tRis.end());
        // ierr = mField.add_ents_to_finite_element_by_TRIs(sommerfeldBcData[it->getMeshsetId()].tRis,"ULTRAWEAK_BCIMPEDANCE"); CHKERRQ(ierr);

      }

      if(it->getName().compare(0,17,"BAYLISS_TURKEL_BC") == 0) {

        baylissTurkelBcData[it->getMeshsetId()].aDmittance_real = 0;
        baylissTurkelBcData[it->getMeshsetId()].aDmittance_imag = -globalParameters.waveNumber.first;

        // rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,baylissTurkelBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = it->getMeshsetIdEntitiesByDimension(mField.get_moab(),2,baylissTurkelBcData[it->getMeshsetId()].tRis,true); CHKERRQ(ierr);
        impedance_bc.insert(baylissTurkelBcData[it->getMeshsetId()].tRis.begin(),baylissTurkelBcData[it->getMeshsetId()].tRis.end());
        // ierr = mField.add_ents_to_finite_element_by_TRIs(baylissTurkelBcData[it->getMeshsetId()].tRis,"ULTRAWEAK_BCIMPEDANCE"); CHKERRQ(ierr);

      }

    }

    bool Dirichlet_bc_set = false;
    Range bc_dirichlet_tris,analytical_bc_tris;
    // for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_(m_field,"ANALYTICAL_BC",it)) {
    //   rval = moab.get_entities_by_type(it->getMeshset(),MBTRI,analytical_bc_tris,true); CHKERRQ_MOAB(rval);
    //   Dirichlet_bc_set = true;
    // }
    bc_dirichlet_tris.merge(analytical_bc_tris);
    AnalyticalDirichletHelmholtzBC analytical_bc_real(m_field);
    AnalyticalDirichletHelmholtzBC analytical_bc_imag(m_field);
    ierr = analytical_bc_real.initializeProblem(m_field,"BCREAL_FE","rePRES",analytical_bc_tris); CHKERRQ(ierr);
    ierr = analytical_bc_imag.initializeProblem(m_field,"BCIMAG_FE","imPRES",analytical_bc_tris); CHKERRQ(ierr);

    map<int,PlaneIncidentWaveSacttrerData> planeWaveScatterData;
    for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_(m_field,"HARD_INCIDENT_WAVE_BC",it)) {
      rval = moab.get_entities_by_type(it->getMeshset(),MBTRI,planeWaveScatterData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
      ierr = analytical_bc_real.initializeProblem(m_field,"BCREAL_FE","rePRES",planeWaveScatterData[it->getMeshsetId()].tRis); CHKERRQ(ierr);
      ierr = analytical_bc_imag.initializeProblem(m_field,"BCIMAG_FE","imPRES",planeWaveScatterData[it->getMeshsetId()].tRis); CHKERRQ(ierr);
      bc_dirichlet_tris.merge(planeWaveScatterData[it->getMeshsetId()].tRis);

      Dirichlet_bc_set = true;

    }

    // for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(mField,SIDESET|HEATFLUXSET,it)) {
    //   HeatFluxCubitBcData mydata;
    //   ierr = it->getBcDataStructure(mydata); CHKERRQ(ierr);
    //   if(mydata.data.flag1==1) {
    //     Range tris;
    //     ierr = it->getMeshsetIdEntitiesByDimension(mField.get_moab(),2,tris,true); CHKERRQ(ierr);
    //     bcFluxMap[it->getMeshsetId()].eNts = tris;
    //     bcFluxMap[it->getMeshsetId()].fLux = mydata.data.value1;
    //     // cerr << bcFluxMap[it->getMeshsetId()].eNts << endl;
    //     // cerr << bcFluxMap[it->getMeshsetId()].fLux << endl;
    //   }
    // }

    for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {

      if(it->getName().compare(0,23,"HARD_INCIDENT_WAVE_BC") == 0) {
        Range tris;
        ierr = it->getMeshsetIdEntitiesByDimension(mField.get_moab(),2,tris,true); CHKERRQ(ierr);
        bcFluxMap[it->getMeshsetId()].eNts = tris;
        essential_bc.insert(bcFluxMap[it->getMeshsetId()].eNts.begin(),bcFluxMap[it->getMeshsetId()].eNts.end());
        // bcFluxMap[it->getMeshsetId()].fLux = mydata.data.value1;
        // cerr << bcFluxMap[it->getMeshsetId()].eNts << endl;
        // cerr << bcFluxMap[it->getMeshsetId()].fLux << endl;
      }

    }

    // Range essential_bc = subtract(skin_faces,natural_bc);

    // get the intersection part of meshset shared by original BC mesh set and the bit level meshset
    Range bit_tris;
    ierr = mField.get_entities_by_type_and_ref_level(ref_level,BitRefLevel().set(),MBTRI,bit_tris);
    essential_bc = intersect(bit_tris,essential_bc);
    natural_bc = intersect(bit_tris,natural_bc);
    impedance_bc = intersect(bit_tris,impedance_bc);
    ierr = mField.add_ents_to_finite_element_by_TRIs(essential_bc,"ULTRAWEAK_BCFLUX"); CHKERRQ(ierr);
    ierr = mField.add_ents_to_finite_element_by_TRIs(natural_bc,"ULTRAWEAK_BCVALUE"); CHKERRQ(ierr);
    ierr = mField.add_ents_to_finite_element_by_TRIs(impedance_bc,"ULTRAWEAK_BCIMPEDANCE"); CHKERRQ(ierr);
    // ierr = mField.add_ents_to_finite_element_by_TRIs(skin_faces,"ULTRAWEAK_BCVALUE"); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }

  /**
   * \brief Refine mesh
   * @param  ufe       general data structure
   * @param  nb_levels number of refinement levels
   * @param  order     set order of approximation
   * @return           errpr code

   Refinement of could result in distorted mesh, for example, imagine when you
   have two levels of non-uniform refinement. Some tetrahedra on the mesh at
   first refinement instance are only refined by splitting subset of edges on
   it. Then refined child tetrahedra usually will have worse quality than
   quality of parent element. Refining such element in subsequent mesh
   refinement, potentially will deteriorate elements quality even worse. To
   prevent that adding new refinement level, recreate whole hierarchy of meshes.

   Note on subsequent improvement could include refinement of
   tetrahedra from different levels, including initial mesh. So refinement two
   could split elements created during refinement one and also split elements
   from an initial mesh.

   That adding the new refinement level creates refinement hierarchy of meshes
   from a scratch,  not adding to existing one.

   Entities from previous hierarchy are used in that process, but bit levels on
   those entities are squashed.

   */
  PetscErrorCode refineMesh(
    UltraWeakTransportElement &ufe,const int nb_levels,const int order
  ) {
    PetscErrorCode ierr;
    MoABErrorCode rval;
    MeshRefinement *refine_ptr;
    PetscFunctionBegin;
    // get refined edges having child vertex
    const RefEntity_multiIndex *ref_ents_ptr;
    ierr = mField.get_ref_ents(&ref_ents_ptr); CHKERRQ(ierr);
    typedef RefEntity_multiIndex::index<Composite_EntType_and_ParentEntType_mi_tag>::type RefEntsByComposite;
    const RefEntsByComposite &ref_ents = ref_ents_ptr->get<Composite_EntType_and_ParentEntType_mi_tag>();
    RefEntsByComposite::iterator rit,hi_rit;
    rit = ref_ents.lower_bound(boost::make_tuple(MBVERTEX,MBEDGE));
    hi_rit = ref_ents.upper_bound(boost::make_tuple(MBVERTEX,MBEDGE));
    Range refined_edges;
    // thist loop is over vertices which parent is edge
    for(;rit!=hi_rit;rit++) {
      refined_edges.insert((*rit)->getParentEnt()); // get parent edge
    }
    // get tets which has large error
    Range tets_to_refine;
    const double max_error = ufe.errorMap.rbegin()->first;
    // int size = ((double)5/6)*ufe.errorMap.size();
    for(
      map<double,EntityHandle>::iterator mit = ufe.errorMap.begin();
      mit!=ufe.errorMap.end();
      mit++
    ) {
      // cerr << mit->first << " " << mit->second << endl;
      // if((size--)>0) continue;
      if(mit->first<0.25*max_error) continue;
      tets_to_refine.insert(mit->second);
    }
    Range tets_to_refine_edges;
    rval = mField.get_moab().get_adjacencies(
      tets_to_refine,1,false,tets_to_refine_edges,moab::Interface::UNION
    ); CHKERRQ_MOAB(rval);
    refined_edges.merge(tets_to_refine_edges);
    ierr = mField.query_interface(refine_ptr); CHKERRQ(ierr);
    for(int ll = 0;ll!=nb_levels;ll++) {
      Range edges;
      ierr = mField.get_entities_by_type_and_ref_level(
        BitRefLevel().set(ll),BitRefLevel().set(),MBEDGE,edges
      ); CHKERRQ(ierr);
      edges = intersect(edges,refined_edges);
      // add edges to refine at current level edges (some of the where refined before)
      ierr = refine_ptr->add_verices_in_the_middel_of_edges(edges,BitRefLevel().set(ll+1)); CHKERRQ(ierr);
      //  get tets at current level
      Range tets;
      ierr = mField.get_entities_by_type_and_ref_level(
        BitRefLevel().set(ll),BitRefLevel().set(),MBTET,tets
      ); CHKERRQ(ierr);
      ierr = refine_ptr->refine_TET(tets,BitRefLevel().set(ll+1)); CHKERRQ(ierr);
      ierr = updateMeshsetsFieldsAndElements(ll+1); CHKERRQ(ierr);
    }

    // update fields and elements
    EntityHandle ref_meshset;
    rval = mField.get_moab().create_meshset(MESHSET_SET,ref_meshset); CHKERRQ_MOAB(rval);
    {
      // cerr << BitRefLevel().set(nb_levels) << endl;
      ierr = mField.get_entities_by_type_and_ref_level(
        BitRefLevel().set(nb_levels),BitRefLevel().set(),MBTET,ref_meshset
      ); CHKERRQ(ierr);

      Range ref_tets;
      rval = mField.get_moab().get_entities_by_type(ref_meshset,MBTET,ref_tets); CHKERRQ_MOAB(rval);

      //add entities to field
      ierr = mField.add_ents_to_field_by_TETs(ref_meshset,"FLUXES"); CHKERRQ(ierr);
      ierr = mField.add_ents_to_field_by_TETs(ref_meshset,"VALUES"); CHKERRQ(ierr);
      ierr = mField.set_field_order(0,MBTET,"FLUXES",order+1); CHKERRQ(ierr);
      ierr = mField.set_field_order(0,MBTRI,"FLUXES",order+1); CHKERRQ(ierr);
      ierr = mField.set_field_order(0,MBTET,"VALUES",order); CHKERRQ(ierr);

      // add entities to skeleton
      Range ref_tris;
      ierr = mField.get_entities_by_type_and_ref_level(
        BitRefLevel().set(nb_levels),BitRefLevel().set(),MBTRI,ref_tris
      ); CHKERRQ(ierr);
      ierr = mField.add_ents_to_finite_element_by_TRIs(
        ref_tris,"ULTRAWEAK_SKELETON"
      ); CHKERRQ(ierr);

      //add entities to finite elements
      for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(mField,BLOCKSET|MAT_THERMALSET,it)) {
        Mat_Thermal temp_data;
        ierr = it->getAttributeDataStructure(temp_data); CHKERRQ(ierr);
        setOfBlocks[it->getMeshsetId()].cOnductivity = temp_data.data.Conductivity;
        setOfBlocks[it->getMeshsetId()].cApacity = temp_data.data.HeatCapacity;
        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTET,setOfBlocks[it->getMeshsetId()].tEts,true); CHKERRQ_MOAB(rval);
        setOfBlocks[it->getMeshsetId()].tEts = intersect(ref_tets,setOfBlocks[it->getMeshsetId()].tEts);
        ierr = mField.add_ents_to_finite_element_by_TETs(setOfBlocks[it->getMeshsetId()].tEts,"ULTRAWEAK"); CHKERRQ(ierr);
      }
    }
    rval = mField.get_moab().delete_entities(&ref_meshset,1); CHKERRQ_MOAB(rval);
    PetscFunctionReturn(0);
  }

  /**
   * \brief Squash bits of entities

   Information about hierarchy of meshses is lost, but entities are not deleted
   from the mesh. After squshing entities bits, new hierarchy can be created.

   * @return error code
   */
  PetscErrorCode squashBits() {
    PetscErrorCode ierr;
    PetscFunctionBegin;
    BitRefLevel all_but_0;
    all_but_0.set(0);
    all_but_0.flip();
    BitRefLevel garbage_bit;
    garbage_bit.set(BITREFLEVEL_SIZE-1); // Garbage level
    const RefEntity_multiIndex *refined_ents_ptr;
    ierr = mField.get_ref_ents(&refined_ents_ptr); CHKERRQ(ierr);
    RefEntity_multiIndex::iterator mit = refined_ents_ptr->begin();
    for(;mit!=refined_ents_ptr->end();mit++) {
      if(mit->get()->getEntType() == MBENTITYSET) continue;
      BitRefLevel bit = mit->get()->getBitRefLevel();
      if((all_but_0&bit)==bit) {
        const_cast<RefEntity_multiIndex*>(refined_ents_ptr)->modify(
          mit,RefEntity_change_set_bit(garbage_bit)
        );
      } else {
        const_cast<RefEntity_multiIndex*>(refined_ents_ptr)->modify(
          mit,RefEntity_change_set_bit(BitRefLevel().set(0))
        );
      }
    }
    PetscFunctionReturn(0);
  }

  /**
   * \brief update meshsets with new entities after mesh refinement
   * @param  nb_levels nb_levels
   * @param  order     appropriate order
   * @return           error code
   */
  PetscErrorCode updateMeshsetsFieldsAndElements(const int nb_levels) {
    MoABErrorCode rval;
    PetscErrorCode ierr;
    BitRefLevel ref_level;
    PetscFunctionBegin;
    ref_level.set(nb_levels);
    for(_IT_CUBITMESHSETS_FOR_LOOP_(mField,it)) {
      EntityHandle meshset = it->meshset;
      ierr = mField.update_meshset_by_entities_children(meshset,ref_level,meshset,MBVERTEX,true); CHKERRQ(ierr);
      ierr = mField.update_meshset_by_entities_children(meshset,ref_level,meshset,MBEDGE,true); CHKERRQ(ierr);
      ierr = mField.update_meshset_by_entities_children(meshset,ref_level,meshset,MBTRI,true); CHKERRQ(ierr);
      ierr = mField.update_meshset_by_entities_children(meshset,ref_level,meshset,MBTET,true); CHKERRQ(ierr);
    }
    PetscFunctionReturn(0);
  }



};

int main(int argc, char *argv[]) {

  ErrorCode rval;
  PetscErrorCode ierr;

  PetscInitialize(&argc,&argv,(char *)0,help);

  moab::Core mb_instance;
  moab::Interface& moab = mb_instance;
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  // get file name form command line
  PetscBool flg = PETSC_TRUE;
  char mesh_file_name[255];
  ierr = PetscOptionsGetString(PETSC_NULL,PETSC_NULL,"-my_file",mesh_file_name,255,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"*** ERROR -my_file (MESH FILE NEEDED)");
  }

  // create MOAB communicator
  ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
  if(pcomm == NULL) pcomm =  new ParallelComm(&moab,PETSC_COMM_WORLD);

  PetscBool is_partitioned = PETSC_FALSE;
  ierr = PetscOptionsGetBool(PETSC_NULL,"-my_is_partitioned",&is_partitioned,&flg); CHKERRQ(ierr);
  if(is_partitioned) {
    //Read mesh to MOAB PARALLEL=READ_DELETE PARALLEL=READ_PART
    const char *option;
    option = "PARALLEL=BCAST_DELETE;PARALLEL_RESOLVE_SHARED_ENTS;PARTITION=PARALLEL_PARTITION;";
    // option = "PARALLEL=READ_PART;PARALLEL_RESOLVE_SHARED_ENTS;PARTITION=PARALLEL_PARTITION;";
    // rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  } else {
    const char *option;
    option = "";
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  }


  //Create mofem interface
  MoFEM::Core core(moab);
  MoFEM::Interface& m_field = core;

  /* define the type of basis function */
  PetscBool is_labatto = PETSC_TRUE;
  ierr = PetscOptionsGetBool(PETSC_NULL,"-lobatto",&is_labatto,NULL); CHKERRQ(ierr);

  // Add meshsets with material and boundary conditions
  MeshsetsManager *meshsets_manager_ptr;
  ierr = m_field.query_interface(meshsets_manager_ptr); CHKERRQ(ierr);
  ierr = meshsets_manager_ptr->setMeshsetFromFile(); CHKERRQ(ierr);

  PetscPrintf(PETSC_COMM_WORLD,"Read meshsets add added meshsets for bc.cfg\n");
  for(_IT_CUBITMESHSETS_FOR_LOOP_(m_field,it)) {
    PetscPrintf(
      PETSC_COMM_WORLD,
      "%s",static_cast<std::ostringstream&>(std::ostringstream().seekp(0) << *it << endl).str().c_str()
    );
    cerr << *it << endl;
  }

  //set entities bit level
  BitRefLevel ref_level;
  ref_level.set(0);
  ierr = m_field.seed_ref_level_3D(0,ref_level); CHKERRQ(ierr);

  //set app. order
  //see Hierarchic Finite Element Bases on Unstructured Tetrahedral Meshes (Mark Ainsworth & Joe Coyle)
  PetscInt order;
  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-my_order",&order,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    order = 0;
  }

  PetscBool wavenumber_flg;
  double wavenumber;
  // set wave number from line command, that overwrite numbre form block set
  ierr = PetscOptionsGetScalar(NULL,"-wave_number",&wavenumber,&wavenumber_flg); CHKERRQ(ierr);
  if(!wavenumber_flg) {

    SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"wave number not given, set in line command -wave_number to fix problem");

  }



  //finite elements


  BcFluxMap bc_flux_map;
  HelmholtzUltraWeak ufe(m_field,bc_flux_map);
  ierr = ufe.getGlobalParametersFromLineCommandOptions(); CHKERRQ(ierr);

  // Initially calculate problem on coarse mesh

  ierr = ufe.addFields("VALUES","FLUXES",order,is_labatto); CHKERRQ(ierr);
  ierr = ufe.addFiniteElements("FLUXES","VALUES"); CHKERRQ(ierr);

  // Set boundary conditions
  ierr = ufe.addBoundaryElements(ref_level);
  ierr = ufe.buildProblem(ref_level); CHKERRQ(ierr);
  if(m_field.check_field("reEX") && m_field.check_field("imEX")) {
    ierr = m_field.modify_finite_element_add_field_data("ULTRAWEAK_RERE","reEX"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("ULTRAWEAK_RERE","imEX"); CHKERRQ(ierr);
    // ierr = m_field.modify_finite_element_add_field_data("ULTRAWEAK_SKELETON","reVelocity"); CHKERRQ(ierr);
    // ierr = m_field.modify_finite_element_add_field_data("ULTRAWEAK_SKELETON","imVelocity"); CHKERRQ(ierr);
  }
  /* A_ij D0 D and F */
  ierr = ufe.createMatrices(); CHKERRQ(ierr);
  ierr = ufe.solveProblem(); CHKERRQ(ierr);
  ierr = ufe.calculateResidual(); CHKERRQ(ierr);
  ierr = ufe.evaluateError(); CHKERRQ(ierr);
  ierr = ufe.destroyMatrices(); CHKERRQ(ierr);
  ierr = ufe.postProc("out_0.h5m"); CHKERRQ(ierr);

  int nb_levels = 5; // default number of refinement levels
  // get number of refinement levels form command line
  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-nb_levels",&nb_levels,PETSC_NULL); CHKERRQ(ierr);

  // refine mesh, solve problem and do it again until number of refinement levels are exceeded.
  for(int ll = 1;ll!=nb_levels;ll++) {
    const int nb_levels = ll;
    ierr = ufe.squashBits(); CHKERRQ(ierr);
    ierr = ufe.refineMesh(ufe,nb_levels,order); CHKERRQ(ierr);
    ref_level = BitRefLevel().set(nb_levels);
    bc_flux_map.clear();
    ierr = ufe.addBoundaryElements(ref_level);
    ierr = ufe.buildProblem(ref_level); CHKERRQ(ierr);
    ierr = ufe.createMatrices(); CHKERRQ(ierr);
    ierr = ufe.solveProblem(); CHKERRQ(ierr);
    ierr = ufe.calculateResidual(); CHKERRQ(ierr);
    ierr = ufe.evaluateError(); CHKERRQ(ierr);
    ierr = ufe.destroyMatrices(); CHKERRQ(ierr);
    ierr = ufe.postProc(
      static_cast<std::ostringstream&>
      (std::ostringstream().seekp(0) << "out_" << nb_levels << ".h5m").str()
    ); CHKERRQ(ierr);
  }

  ierr = PetscFinalize(); CHKERRQ(ierr);

  return 0;

}
