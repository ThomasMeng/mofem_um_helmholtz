# copy dox/figures to html directory created by doxygen
add_custom_target(doxygen_copy_helmholtz_figures
    ${CMAKE_COMMAND} -E copy_directory
    ${PROJECT_SOURCE_DIR}/users_modules/helmholtz/doc/figures ${PROJECT_BINARY_DIR}/html
)
add_dependencies(doc doxygen_copy_helmholtz_figures)
