/** \file HelmholtzElement.hpp
 \ingroup mofem_helmholtz_elem

 \brief Operators and data structures for wave propagation analyse (Galerkin Element)

 Implementation of Helmholtz element for wave propagation problem

 */

/*
  This work is part of PhD thesis by on Micro-fluids: Thomas Felix Xuan Meng
 */

 /*
  * MoFEM is distributed in the hope that it will be useful, but WITHOUT
  * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  * License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
  * The header file should contains as least #include as possible for speed
  */

#ifndef __HELMHOLTZ_ELEMENT_HPP
#define __HELMHOLTZ_ELEMENT_HPP

#include<moab/Skinner.hpp>
using namespace std;
using namespace boost::math;



namespace MoFEM {

/** \brief struture grouping operators and data used for helmholtz problems
* \ingroup mofem_helmholtz_elem
*
* In order to assemble matrices and right hand side vectors, the loops over
* elements, enetities over that elememnts and finally loop over intergration
* points are executed.
*
* complex-real transformation are implemented for computational efficenicy
* See Ercegovac, Milos, and Jean-Michel Muller. "Solving Systems of Linear
* Equations in Complex Domain: Complex E-Method." (2007). for details.
*
*
* Following implementation separte those three cegories of loops and to each
* loop attach operator.
*
*/
// Three dimensional homogeneous isotropic medium and time harmonic Helmholtz operator
struct HelmholtzElement {
	/// \brief  definition of volume element
	struct MyVolumeFE: public TetElementForcesAndSourcesCore {
		MyVolumeFE(MoFEM::Interface &_mField): TetElementForcesAndSourcesCore(_mField) {}

		/** \brief it is used to calculate nb. of Gauss integartion points
		 *
		 * for more details pleas look
		 *   Reference:
		 *
		 * Albert Nijenhuis, Herbert Wilf,
		 * Combinatorial Algorithms for Computers and Calculators,
		 * Second Edition,
		 * Academic Press, 1978,
		 * ISBN: 0-12-519260-6,
		 * LC: QA164.N54.
		 *
		 * More details about algorithm
		 * http://people.sc.fsu.edu/~jburkardt/cpp_src/gm_rule/gm_rule.html
		**/
		int getRule(int order) { return order; };
	};

	MyVolumeFE feRhs; ///< cauclate right hand side for tetrahedral elements
	MyVolumeFE& getLoopFeRhs() { return feRhs; } ///< get rhs volume element
	MyVolumeFE feLhs; //< calculate left hand side for tetrahedral elements
	MyVolumeFE& getLoopFeLhs() { return feLhs; } ///< get lhs volume element

	/** \brief define surface element
	*
	*
	*/
	struct MyTriFE: public TriElementForcesAndSurcesCore {
		MyTriFE(MoFEM::Interface &_mField): TriElementForcesAndSurcesCore(_mField) {}
		int getRule(int order) { return order; };
	};

	MyTriFE feRhs_Tri; ///< cauclate right hand side for tetrahedral elements
	MyTriFE& getLoopFeRhs_Tri() { return feRhs_Tri; } ///< get rhs volume element

	MyTriFE feFlux;
  MyTriFE& getLoopFeFlux() { return feFlux; }

	MyTriFE feIncidentWave; //incident wave
  MyTriFE& getLoopfeIncidentWave() { return feIncidentWave; } //incident wave

  MyTriFE feImpedanceLhs;
  MyTriFE& getLoopFeImpedanceLhs() { return feImpedanceLhs; }



	MoFEM::Interface &mField;
    HelmholtzElement(
        MoFEM::Interface &m_field):
        feRhs(m_field),feRhs_Tri(m_field),feLhs(m_field),feFlux(m_field),feIncidentWave(m_field),feImpedanceLhs(m_field),mField(m_field) {}

	/** \brief data for calulation Angular Frequency and wave speed elements
	* \infroup mofem_helmholtz_elem
	*/
	struct BlockData {
		double aNgularfreq; // Angular frequency
		double sPeed;   // Wave Speed

		Range tRis;
		Range tEts; ///< constatins elements in block set
	};
	map<int,BlockData> setOfBlocks; ///< maps block set id with appropiate BlockData
	BlockData blockData;

	/** \brief data for calulation heat flux
	* \infroup mofem_helmholtz_elem
	*/
	struct FluxData {
		HeatfluxCubitBcData dAta; ///< for more details look to BCMultiIndices.hpp to see details of HeatfluxCubitBcData
		Range tRis; ///< suraface triangles where hate flux is applied
	};
	map<int,FluxData> setOfFluxes; ///< maps side set id with appropiate FluxData

	struct IncidentData {
		double amplitude; ///< for more details look to BCMultiIndices.hpp to see details of HeatfluxCubitBcData
		Range tRis; ///< suraface triangles where hate flux is applied
	};
	map<int,IncidentData> setOfIncident; ///< maps side set id with appropiate FluxData

	/** \brief data for Robin Boundary condition
	* \infroup mofem_helmholtz_elem
	*/
	struct ImpedanceData {
		double g; /*the zero velocity term of the Robin BC*/
		double sIgma;  //admittance of the surface, if g and sIgma both zero the surface is called sound hard.
		Range tRis; ///< those will be on body skin, except thos with contact whith other body where pressure is applied
	};
	map<int,ImpedanceData> setOfImpedance; //< maps block set id with appropiate data



	/** \brief common data used by volume elements
	* \infroup mofem_helmholtz_elem
	*/
	struct CommonData {

		VectorDouble pressureReAtGaussPts;   //Helmholtz Pressure real
		VectorDouble pressureImAtGaussPts;   //Helmholtz Pressure imag
		MatrixDouble gradReAtGaussPts;
		MatrixDouble gradImAtGaussPts;

		inline ublas::matrix_row<MatrixDouble > getGradReAtGaussPts(const int gg) {
			return ublas::matrix_row<MatrixDouble >(gradReAtGaussPts,gg);
		}

		inline ublas::matrix_row<MatrixDouble > getGradImAtGaussPts(const int gg) {
			return ublas::matrix_row<MatrixDouble >(gradImAtGaussPts,gg);
		}

		MatrixDouble Ann,Anv,Avv;
		ublas::vector<MatrixDouble > Aen,Afn,Aev,Afv;
		ublas::matrix<MatrixDouble > Aee,Aef,Aff;

	};
	CommonData commonData;

	/// \brief operator to calculate pressure gradient at Gauss points
	struct OpGetGradReAtGaussPts: public TetElementForcesAndSourcesCore::UserDataOperator {

		CommonData &commonData;
		OpGetGradReAtGaussPts(const string field_name,CommonData &common_data):
			TetElementForcesAndSourcesCore::UserDataOperator(field_name),
			commonData(common_data) {}

		/** \brief operator calculating pressure gradients
		  *
		  * pressure gradient is calculated multiplying derivatives of shape functions by degrees of freedom.
		  */
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;
			try {

				if(data.getIndices().size()==0) PetscFunctionReturn(0);
				int nb_dofs = data.getFieldData().size();
				int nb_gauss_pts = data.getN().size1();

				//initialize
				commonData.gradReAtGaussPts.resize(nb_gauss_pts,3);
				if(type == MBVERTEX) {
					fill(commonData.gradReAtGaussPts.data().begin(),commonData.gradReAtGaussPts.data().end(),0);
				}

				for(int gg = 0;gg<nb_gauss_pts;gg++) {
					ublas::noalias(commonData.getGradReAtGaussPts(gg)) += prod( trans(data.getDiffN(gg,nb_dofs)), data.getFieldData() );
				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};

	/// \brief operator to calculate pressure gradient at Gauss points
	struct OpGetGradImAtGaussPts: public TetElementForcesAndSourcesCore::UserDataOperator {

		CommonData &commonData;
		OpGetGradImAtGaussPts(const string field_name,CommonData &common_data):
			TetElementForcesAndSourcesCore::UserDataOperator(field_name),
			commonData(common_data) {}

		/** \brief operator calculating pressure gradients
		  *
		  * pressure gradient is calculated multiplying derivatives of shape functions by degrees of freedom.
		  */
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;
			try {

				if(data.getIndices().size()==0) PetscFunctionReturn(0);
				int nb_dofs = data.getFieldData().size();
				int nb_gauss_pts = data.getN().size1();

				//initialize
				commonData.gradImAtGaussPts.resize(nb_gauss_pts,3);
				if(type == MBVERTEX) {
					fill(commonData.gradImAtGaussPts.data().begin(),commonData.gradImAtGaussPts.data().end(),0);
				}

				for(int gg = 0;gg<nb_gauss_pts;gg++) {
					ublas::noalias(commonData.getGradImAtGaussPts(gg)) += prod( trans(data.getDiffN(gg,nb_dofs)), data.getFieldData() );
				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};

	/** \brief opearator to caulate pressure  and rate of pressure at Gauss points
	  * \infroup mofem_helmholtz_elem
	  */
	template<typename OP>
	struct OpGetFieldAtGaussPts: public OP::UserDataOperator {

		VectorDouble &fieldAtGaussPts;
		OpGetFieldAtGaussPts(const string field_name,VectorDouble &field_at_gauss_pts):
			OP::UserDataOperator(field_name),
			fieldAtGaussPts(field_at_gauss_pts) {}
		/** \brief operator calculating pressure and rate of pressure
		  *
		  * pressure pressure or rate of pressure is calculated multiplyingshape functions by degrees of freedom
		  */
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;
			try {

				if(data.getFieldData().size()==0) PetscFunctionReturn(0);
				int nb_dofs = data.getFieldData().size();
				int nb_gauss_pts = data.getN().size1();

				//initialize
				fieldAtGaussPts.resize(nb_gauss_pts);
				if(type == MBVERTEX) {
					//loop over shape functions on entities allways start from
					//vertices, so if nodal shape functions are processed, vector of
					//field values is zeroad at initialization
					fill(fieldAtGaussPts.begin(),fieldAtGaussPts.end(),0);
				}

				for(int gg = 0;gg<nb_gauss_pts;gg++) {
					fieldAtGaussPts[gg] += inner_prod(data.getN(gg,nb_dofs),data.getFieldData());

				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};

	/** \brief operator to calculate pressure at Gauss pts
	* \infroup mofem_helmholtz_elem
	*/
	struct OpGetTetPressureReAtGaussPts: public OpGetFieldAtGaussPts<TetElementForcesAndSourcesCore> {
		OpGetTetPressureReAtGaussPts(const string field_name,CommonData &common_data):
			OpGetFieldAtGaussPts<TetElementForcesAndSourcesCore>(field_name,common_data.pressureReAtGaussPts) {}
	};
	struct OpGetTetPressureImAtGaussPts: public OpGetFieldAtGaussPts<TetElementForcesAndSourcesCore> {
		OpGetTetPressureImAtGaussPts(const string field_name,CommonData &common_data):
			OpGetFieldAtGaussPts<TetElementForcesAndSourcesCore>(field_name,common_data.pressureImAtGaussPts) {}
	};

	/** \brief operator to calculate pressure at Gauss pts
	* \infroup mofem_helmholtz_elem
	*/
	struct OpGetTriPressureReAtGaussPts: public OpGetFieldAtGaussPts<TriElementForcesAndSurcesCore> {
		OpGetTriPressureReAtGaussPts(const string field_name,CommonData &common_data):
			OpGetFieldAtGaussPts<TriElementForcesAndSurcesCore>(field_name,common_data.pressureReAtGaussPts) {}
	};

	struct OpGetTriPressureImAtGaussPts: public OpGetFieldAtGaussPts<TriElementForcesAndSurcesCore> {
		OpGetTriPressureImAtGaussPts(const string field_name,CommonData &common_data):
			OpGetFieldAtGaussPts<TriElementForcesAndSurcesCore>(field_name,common_data.pressureImAtGaussPts) {}
	};


    MatrixDouble hoCoordsTri;

	//get the higher order approximation coordinates.
    struct OpHoCoordTri: public TriElementForcesAndSurcesCore::UserDataOperator {

		MatrixDouble &hoCoordsTri;
		OpHoCoordTri(const string field_name,MatrixDouble &ho_coords):
			TriElementForcesAndSurcesCore::UserDataOperator(field_name),
			hoCoordsTri(ho_coords) {}

		/*
		Cartesian coordinates for gaussian points inside elements
		X^coordinates = DOF dot* N
		*/
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;

			try {

				if(data.getFieldData().size()==0) PetscFunctionReturn(0);

				hoCoordsTri.resize(data.getN().size1(),3);
				if(type == MBVERTEX) {
					hoCoordsTri.clear();
				}

				int nb_dofs = data.getFieldData().size();

				for(unsigned int gg = 0;gg<data.getN().size1();gg++) {
					for(int dd = 0;dd<3;dd++) {
						hoCoordsTri(gg,dd) += cblas_ddot(nb_dofs/3,&data.getN(gg)[0],1,&data.getFieldData()[dd],3); //calculate x,y,z at each GaussPts
					}
				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

    };

	MatrixDouble hoCoordsTet;

	//get the higher order approximation coordinates.
    struct OpHoCoordTet: public TetElementForcesAndSourcesCore::UserDataOperator {

		MatrixDouble &hoCoordsTet;
		OpHoCoordTet(const string field_name,MatrixDouble &ho_coords):
			TetElementForcesAndSourcesCore::UserDataOperator(field_name),
			hoCoordsTet(ho_coords) {}

		/*
		Cartesian coordinates for gaussian points inside elements
		X^coordinates = DOF dot* N
		*/
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;

			try {

				if(data.getFieldData().size()==0) PetscFunctionReturn(0);

				hoCoordsTet.resize(data.getN().size1(),3);
				if(type == MBVERTEX) {
					hoCoordsTet.clear();
				}

				int nb_dofs = data.getFieldData().size();

				for(unsigned int gg = 0;gg<data.getN().size1();gg++) {
					for(int dd = 0;dd<3;dd++) {
						hoCoordsTet(gg,dd) += cblas_ddot(nb_dofs/3,&data.getN(gg)[0],1,&data.getFieldData()[dd],3); //calculate x,y,z at each GaussPts
					}
				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

    };



	/** \biref operator to calculate right hand side of stiffness terms
	* \infroup mofem_helmholtz_elem
	*/
	struct OpHelmholtzRhs_Re: public TetElementForcesAndSourcesCore::UserDataOperator {

		BlockData &dAta;
		CommonData &commonData;
		bool useTsF;

		OpHelmholtzRhs_Re(const string re_field_name,const string im_field_name,BlockData &data,CommonData &common_data):
			TetElementForcesAndSourcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useTsF(true) {}

		Vec F;
		OpHelmholtzRhs_Re(const string re_field_name,const string im_field_name,Vec _F,BlockData &data,CommonData &common_data):
			TetElementForcesAndSourcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useTsF(false),F(_F) { }

		/* tip:all vectors should be declared as class members instead of as local function variables. */
		VectorDouble dNdT_Re;
		VectorDouble Nf;

		/** \brief calculate helmholtz operator apply on lift.
		  *
		  * F = int diffN^T gard_T0 - N^T k T0 dOmega^2
		  *
		  */
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;

			if(dAta.tEts.find(getMoFEMFEPtr()->get_ent()) == dAta.tEts.end()) {
				PetscFunctionReturn(0);
			}

			try {

				if(data.getIndices().size()==0) PetscFunctionReturn(0);
				if(dAta.tEts.find(getMoFEMFEPtr()->get_ent())==dAta.tEts.end()) PetscFunctionReturn(0);

				PetscErrorCode ierr;

				/* moFEM tip: use "row_data.getIndices().size()" instead of "row_data.getN().size2()" for nb_row_dofs
				the former can cause problems with mix approximation orders */

				int nb_row_dofs = data.getIndices().size();
				Nf.resize(nb_row_dofs);
				bzero(&*Nf.data().begin(),data.getIndices().size()*sizeof(FieldData));

				//wave number K is the propotional to the frequency of incident wave
				//and represents number of waves per wave length 2Pi - 2Pi/K
				double wAvenumber = dAta.aNgularfreq/dAta.sPeed;
				double wAvenUmber = pow(wAvenumber,2.0);
				dNdT_Re.resize(nb_row_dofs);

				for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

					double val = getVolume()*getGaussPts()(3,gg);

					if(getHoGaussPtsDetJac().size()>0) {
						val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
					}

					dNdT_Re.clear();
					/*  k^2 N^T q */
					double const_kT_Re = wAvenUmber*commonData.pressureReAtGaussPts[gg];
					/*  diffN^T diffN q */
					ublas::noalias(dNdT_Re) = prod(data.getDiffN(gg,nb_row_dofs),commonData.getGradReAtGaussPts(gg));
					/*  diffN^T diffN q - k^2 N^T N q  lift of helmholtz operator*/

					cblas_daxpy(nb_row_dofs, -const_kT_Re, &data.getN(gg,nb_row_dofs)[0], 1, &dNdT_Re[0], 1);
					/* insufficient, FIX ME*/

					ublas::noalias(Nf) += val*(dNdT_Re);

				}

				if(useTsF) {
					ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),
										&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
				} else {
					ierr = VecSetValues(F,data.getIndices().size(),
										&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);

				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};

	/** \biref operator to calculate right hand side of stiffness terms
	* \infroup mofem_helmholtz_elem
	*/
	struct OpHelmholtzRhs_Im: public TetElementForcesAndSourcesCore::UserDataOperator {

		BlockData &dAta;
		CommonData &commonData;
		bool useTsF;

		OpHelmholtzRhs_Im(const string re_field_name,const string im_field_name,BlockData &data,CommonData &common_data):
			TetElementForcesAndSourcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useTsF(true) {}

		Vec F;
		OpHelmholtzRhs_Im(const string re_field_name,const string im_field_name,Vec _F,BlockData &data,CommonData &common_data):
			TetElementForcesAndSourcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useTsF(false),F(_F) { }

		/* tip:all vectors should be declared as class members instead of as local function variables. */
		VectorDouble dNdT_Im;
		VectorDouble Nf;

		/** \brief calculate helmholtz operator apply on lift.
		  *
		  * F = int diffN^T gard_T0 - N^T k T0 dOmega^2
		  *
		  */
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;

			if(dAta.tEts.find(getMoFEMFEPtr()->get_ent()) == dAta.tEts.end()) {
				PetscFunctionReturn(0);
			}

			try {

				if(data.getIndices().size()==0) PetscFunctionReturn(0);
				if(dAta.tEts.find(getMoFEMFEPtr()->get_ent())==dAta.tEts.end()) PetscFunctionReturn(0);

				PetscErrorCode ierr;

				/* moFEM tip: use "row_data.getIndices().size()" instead of "row_data.getN().size2()" for nb_row_dofs
				the former can cause problems with mix approximation orders */

				int nb_row_dofs = data.getIndices().size();
				Nf.resize(nb_row_dofs);
				bzero(&*Nf.data().begin(),data.getIndices().size()*sizeof(FieldData));


				double wAvenumber = dAta.aNgularfreq/dAta.sPeed;
				double wAvenUmber = pow(wAvenumber,2.0);
				dNdT_Im.resize(nb_row_dofs);

				for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

					double val = getVolume()*getGaussPts()(3,gg);

					if(getHoGaussPtsDetJac().size()>0) {
						val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
					}

					dNdT_Im.clear();
					/*  k^2 N^T q */
					double const_kT_Im = wAvenUmber*commonData.pressureImAtGaussPts[gg];
					/*  diffN^T diffN q */
					ublas::noalias(dNdT_Im) = prod(data.getDiffN(gg,nb_row_dofs),commonData.getGradImAtGaussPts(gg));
					/*  diffN^T diffN q - k^2 N^T N q  lift of helmholtz operator*/

					cblas_daxpy(nb_row_dofs, -const_kT_Im, &data.getN(gg,nb_row_dofs)[0], 1, &dNdT_Im[0], 1);
					/* insufficient, FIX ME*/
					ublas::noalias(Nf) += val*(dNdT_Im);

				}

				if(useTsF) {
					ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),
										&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
				} else {
					ierr = VecSetValues(F,data.getIndices().size(),
										&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);

				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};


	/** \biref operator to calculate right hand side of stiffness terms
	* \infroup mofem_helmholtz_elem
	*/
	struct OpHelmholtzRhs_impedance: public TriElementForcesAndSurcesCore::UserDataOperator {

		BlockData &dAta;
		CommonData &commonData;
		bool useTsF;
		bool useReal;

		OpHelmholtzRhs_impedance(const string re_field_name,const string im_field_name,BlockData &data,CommonData &common_data,bool use_real):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useReal(use_real),useTsF(true) {}

		Vec F;
		OpHelmholtzRhs_impedance(const string re_field_name,const string im_field_name,Vec _F,BlockData &data,CommonData &common_data,bool use_real):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useReal(use_real),useTsF(false),F(_F) { }

		VectorDouble Nf;

		/* tip:all vectors should be declared as class members instead of as local function variables. */


		/** \brief calculate helmholtz operator apply on lift.
		  *
		  * F = int i K T N^T dS
		  *
		  */
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;

			try {

				if(data.getIndices().size()==0) PetscFunctionReturn(0);
				if(dAta.tRis.find(getMoFEMFEPtr()->get_ent())==dAta.tRis.end()) PetscFunctionReturn(0);

				PetscErrorCode ierr;

				/* moFEM tip: use "row_data.getIndices().size()" instead of "row_data.getN().size2()" for nb_row_dofs
				the former can cause problems with mix approximation orders */

				const FENumeredDofMoFEMEntity *dof_ptr;
				ierr = getMoFEMFEPtr()->get_row_dofs_by_petsc_gloabl_dof_idx(data.getIndices()[0],&dof_ptr); CHKERRQ(ierr);
				int rank = dof_ptr->get_max_rank();
				int nb_row_dofs = data.getIndices().size()/rank;

				Nf.resize(nb_row_dofs);
				bzero(&*Nf.data().begin(),data.getIndices().size()*sizeof(FieldData));

				//wave number K is the propotional to the frequency of incident wave
				//and represents number of waves per wave length 2Pi - 2Pi/K
				double k = dAta.aNgularfreq/dAta.sPeed;
				bool ho_geometry = true; //FIXME

				for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

					double val = getGaussPts()(2,gg);

					if(ho_geometry) {
						double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
						val *= area;
					} else {
						val *= getArea();
					}


					if(useReal) {

						ublas::noalias(Nf) += val*( - k*commonData.pressureImAtGaussPts(gg)*data.getN(gg,nb_row_dofs));

					} else{

						ublas::noalias(Nf) += val*(k*commonData.pressureReAtGaussPts(gg)*data.getN(gg,nb_row_dofs));


					}


				}

				if(useTsF) {
					ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),
										&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
				} else {
					ierr = VecSetValues(F,data.getIndices().size(),
										&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);

				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};


	/** \biref operator to calculate right hand side wave source term F
	* \infroup mofem_helmholtz_elem
	*/
	struct OpHelmholtzRhs_F: public TetElementForcesAndSourcesCore::UserDataOperator {

		BlockData &dAta;
		CommonData &commonData;
		MatrixDouble &hoCoords;
		bool useTsF;
		bool useScalar;
		bool useReal;

		OpHelmholtzRhs_F(const string re_field_name,const string im_field_name,BlockData &data,CommonData &common_data,MatrixDouble &ho_coords,bool usescalar,bool use_real):
			TetElementForcesAndSourcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useTsF(false),hoCoords(ho_coords),useScalar(usescalar),useReal(use_real) {}  //here useTsF(ture) originally.

		Vec F;
		OpHelmholtzRhs_F(const string re_field_name,const string im_field_name,Vec _F,BlockData &data,CommonData &common_data,MatrixDouble &ho_coords,bool usescalar,bool use_real):
			TetElementForcesAndSourcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useTsF(false),F(_F),hoCoords(ho_coords),useScalar(usescalar),useReal(use_real) { }

		VectorDouble Nf;

		/** \brief calculate Helmholtz RHS source term.
		  *
		  * F = int diffN^T F(x,y,z) dOmega^2
		  *
		  */

		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;

			if(dAta.tEts.find(getMoFEMFEPtr()->get_ent()) == dAta.tEts.end()) {
				PetscFunctionReturn(0);
			}


			try {

				PetscErrorCode ierr;
				if(data.getIndices().size()==0) PetscFunctionReturn(0);
				int nb_row = data.getIndices().size();
				Nf.resize(nb_row);
				bzero(&Nf[0],nb_row*sizeof(double));
				double x,y,z;
				double iNcidentwave;
				double wAvenumber = dAta.aNgularfreq/dAta.sPeed;

				for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

					double val = getVolume()*getGaussPts()(3,gg);

					if(getHoGaussPtsDetJac().size()>0) {
						val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
						x = hoCoords(gg,0);
						y = hoCoords(gg,1);
						z = hoCoords(gg,2);

					} else {
						x = getCoordsAtGaussPts()(gg,0);
						y = getCoordsAtGaussPts()(gg,1);
						z = getCoordsAtGaussPts()(gg,2);
					}

					VectorDouble F_S;
				 //Scalar value source term F = a
				    if(useScalar) {
						const double f_scalar = -1; //scalar source vector F=1, wait for modification.
						ublas::scalar_vector<double> F (data.getN().size1(),f_scalar); //scalar source vector
						F_S = F;
				    }


				 //Functional value source term F =f(x)


				    else {


						/***** incident wave in x direction *****/
						/*** incident wave from Finite Element Analysis of Acoustic Scattering by Frank Ihlenburg **/

						double theta = atan2(y,x)+2*M_PI; //the arctan of radians (y/x)

						const double k = wAvenumber;  //Wave number
						const double a = 0.5;         //radius of the sphere,wait to modify by user
						const double const1 = k * a;

						const complex< double > i( 0.0, 1.0 );

						//// magnitude of incident wave
						const double phi_incident_mag = 1.0;

						const double tol = 1.0e-10;
						double max = 0.0;
						double min = 999999.0;
						complex< double > result = 0.0;
						complex< double > prev_result;
						double error = 100.0;


						/*** Cyclindrical incident wave ***/
						//double R = sqrt(pow(x,2.0)+pow(y,2.0)); //radius
						double R = 0.5;
						unsigned int n = 1; //initialized the infinite series loop

						double Jn_der_zero = ( - cyl_bessel_j( 1, const1 ));

						//n=0;
						result -= Jn_der_zero;

						while( error > tol )  //finding the acoustic potential in one single point.
						{
							prev_result = result;
							//The derivative of bessel function
							double Jn_der = (n / const1 * cyl_bessel_j( n, const1 ) - cyl_bessel_j( n + 1, const1 ));

							result -= 2.0 * pow( i, n ) * Jn_der * cos(n*theta);
							error = abs( abs( result ) - abs( prev_result ) );
							++n;
						}

						/** 	End     **/
						//result = i * k * cos( theta ) * exp( i * k * R * cos( theta ) ); //derivative of incident wave

						//cout << "\n rhs_F is running \n" << endl;

						if(useReal){
							iNcidentwave = std::real(k*result);

						} else if(!useReal) {
							iNcidentwave = std::imag(k*result);

						}

						}

						val *= iNcidentwave;
						ublas::noalias(Nf) += val*data.getN(gg,nb_row);

					}



				if(useTsF) {
					ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),
										&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
				} else {
					ierr = VecSetValues(F,data.getIndices().size(),
										&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);

				}


		} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};


	/** \biref operator to calculate left hand side of stiffness terms
	  * \infroup mofem_helmholtz_elem
	  */
	struct OpHelmholtzLhs_A: public TetElementForcesAndSourcesCore::UserDataOperator {

		BlockData &dAta;
		CommonData &commonData;
		bool cAlculate;

        bool useTsB;
		OpHelmholtzLhs_A(const string re_field_name,const string im_field_name,BlockData &data,CommonData &common_data,bool calculate):
			TetElementForcesAndSourcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useTsB(true),cAlculate(calculate) { }

		Mat A;
		OpHelmholtzLhs_A(const string re_field_name,const string im_field_name,Mat _A,BlockData &data,CommonData &common_data,bool calculate):
			TetElementForcesAndSourcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),commonData(common_data),useTsB(false),A(_A),cAlculate(calculate) {}

		MatrixDouble /*K,*/transK;

		/** \brief calculate helmholtz stiffness matrix
		  *
		  * K = int diffN^T * diffN^T - K^2 N^T N dOmega^2
		  *
		  */
		PetscErrorCode doWork(
			int row_side,int col_side,
			EntityType row_type,EntityType col_type,
			DataForcesAndSurcesCore::EntData &row_data,
			DataForcesAndSurcesCore::EntData &col_data) {
			PetscFunctionBegin;

			if(dAta.tEts.find(getMoFEMFEPtr()->get_ent()) == dAta.tEts.end()) {
				PetscFunctionReturn(0);
			}

			MatrixDouble *K_ptr;

			try {

				if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
				if(col_data.getIndices().size()==0) PetscFunctionReturn(0);


				switch(row_type) {

					case MBVERTEX:
						switch(col_type) {
							case MBVERTEX:
								K_ptr = &commonData.Ann;
								break;
							case MBTET:
								K_ptr = &commonData.Anv;
								break;
							default:
								SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"row_type = MBVERTEX, error col_type not match");
								break;
						}
						break;

					case MBEDGE:
						switch(col_type) {
							case MBVERTEX:
								commonData.Aen.resize(6);
								K_ptr = &commonData.Aen[row_side];
								break;
							case MBTET:
								commonData.Aev.resize(6);
								K_ptr = &commonData.Aev[row_side];
								break;
							case MBEDGE:
								commonData.Aee.resize(6,6);
								K_ptr = &commonData.Aee(row_side,col_side);
								break;
							case MBTRI:
								commonData.Aef.resize(6,4);
								K_ptr = &commonData.Aef(row_side,col_side);
								break;

							default:
								SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"row_type = MBEDGE, error col_type not match");
								break;
						}
						break;

					case MBTRI:
						switch(col_type) {
							case MBVERTEX:
								commonData.Afn.resize(4);
								K_ptr = &commonData.Afn[row_side];
								break;
							case MBTET:
								commonData.Afv.resize(4);
								K_ptr = &commonData.Afv[row_side];
								break;
							case MBTRI:
								commonData.Aff.resize(4,4);
								K_ptr = &commonData.Aff(row_side,col_side);
								break;
							default:
								SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"row_type = MBTRI, error col_type not match");
								break;
						}
						break;

					case MBTET:
						K_ptr = &commonData.Avv;
						break;
					default:
						SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCT,"row_type not match");
						break;
						//never should happen
				}

				MatrixDouble &K = *K_ptr;

				int nb_row = row_data.getIndices().size();
				int nb_col = col_data.getIndices().size();

				if(cAlculate) {

					K.resize(nb_row,nb_col);
					bzero(&*K.data().begin(),nb_row*nb_col*sizeof(double));

					for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {

						double wAvenumber = dAta.aNgularfreq/dAta.sPeed;
						double k_2 = pow(wAvenumber,2.0);
						/* check whether if double wAvenUmber = pow(dAta.aNgularfreq/dAta.sPeed,2.0); takes
						less time the the calculation above */

						double val = getVolume()*getGaussPts()(3,gg);
						if(getHoGaussPtsDetJac().size()>0) {
							val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
						}
						noalias(K) += val*(prod(row_data.getDiffN(gg,nb_row),trans(col_data.getDiffN(gg,nb_col)))-k_2*outer_prod( row_data.getN(gg,nb_row),col_data.getN(gg,nb_col) ));
					}

				}
				PetscErrorCode ierr;
				if(!useTsB) {
					const_cast<FEMethod*>(getFEMethod())->ts_B = A;    //FEMethod does not belong to MoFEM::Interface anymore.
				}

				ierr = MatSetValues(
						   A,  //(getFEMethod()->ts_B), instead in New thermal element. wait
						   nb_row,&row_data.getIndices()[0],
						   nb_col,&col_data.getIndices()[0],
						   &K(0,0),ADD_VALUES); CHKERRQ(ierr);

				if(row_side != col_side || row_type != col_type) {
					transK.resize(nb_col,nb_row);
					noalias(transK) = trans( K );
					ierr = MatSetValues(
							   A,
							   nb_col,&col_data.getIndices()[0],
							   nb_row,&row_data.getIndices()[0],
							   &transK(0,0),ADD_VALUES); CHKERRQ(ierr);
				}

			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};


	/** \brief operator for calculate Impedance flux and assemble to right hand side
	  * \infroup mofem_helmholtz_elem
	  */
	struct OpHelmholtzFlux:public TriElementForcesAndSurcesCore::UserDataOperator {

		BlockData &datA;
		FluxData &dAta;
		bool ho_geometry;
		bool useTsF;
		OpHelmholtzFlux(const string re_field_name,const string im_field_name,BlockData &DATA,FluxData &data,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			datA(DATA),dAta(data),ho_geometry(_ho_geometry),useTsF(true) { }

		Vec F;
		OpHelmholtzFlux(const string re_field_name,const string im_field_name,Vec _F,
				   BlockData &DATA,FluxData &data,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			datA(DATA),dAta(data),ho_geometry(_ho_geometry),useTsF(false),F(_F) { }

		VectorDouble Nf;

		/** \brief calculate Impedance flux
		  *
		  * F = int_S N^T * g dS
		  *
		  */
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;

			if(data.getIndices().size()==0) PetscFunctionReturn(0);
			if(dAta.tRis.find(getMoFEMFEPtr()->get_ent())==dAta.tRis.end()) PetscFunctionReturn(0);

			PetscErrorCode ierr;

			const FENumeredDofMoFEMEntity *dof_ptr;
			ierr = getMoFEMFEPtr()->get_row_dofs_by_petsc_gloabl_dof_idx(data.getIndices()[0],&dof_ptr); CHKERRQ(ierr);
			int rank = dof_ptr->get_max_rank();

			int nb_dofs = data.getIndices().size()/rank;

			Nf.resize(data.getIndices().size());
			fill(Nf.begin(),Nf.end(),0);

			//cerr << getNormal() << endl;
			//cerr << getNormals_at_GaussPt() << endl;

			for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

				double val = getGaussPts()(2,gg);
				double flux;
				cout << "\n flux occurs \n" << endl;

				if(ho_geometry) {
					double area = norm_2(getNormals_at_GaussPt(gg)); //cblas_dnrm2(3,&getNormals_at_GaussPt()(gg,0),1);
					flux = dAta.dAta.data.value1*area;  //FluxData.HeatfluxCubitBcData.data.value1 * area
				} else {
					flux = dAta.dAta.data.value1*getArea();
				}
				//cblas_daxpy(nb_row_dofs,val*flux,&data.getN()(gg,0),1,&*Nf.data().begin(),1);
				ublas::noalias(Nf) += val*flux*data.getN(gg,nb_dofs);

			}

			if(useTsF) {
				ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),
									&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
			} else {
				ierr = VecSetValues(F,data.getIndices().size(),
									&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
			}

			PetscFunctionReturn(0);
		}

	};


	/** \brief operator for calculate Impedance flux and assemble to right hand side
	* \infroup mofem_helmholtz_elem
	*/
	struct OpHelmholtzIncidentWave:public TriElementForcesAndSurcesCore::UserDataOperator {

		MatrixDouble &hoCoords;
		BlockData &datA;
		IncidentData &dAta;
		bool ho_geometry;
		bool useTsF;
		bool useReal;

		OpHelmholtzIncidentWave(const string re_field_name,const string im_field_name,BlockData &DATA,IncidentData &data,MatrixDouble &ho_coords,bool use_real,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			datA(DATA),dAta(data),ho_geometry(_ho_geometry),useTsF(true),hoCoords(ho_coords),useReal(use_real) { }

		Vec F;
		OpHelmholtzIncidentWave(const string re_field_name,const string im_field_name,Vec _F,
						BlockData &DATA,IncidentData &data,MatrixDouble &ho_coords,bool use_real,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			datA(DATA),dAta(data),ho_geometry(_ho_geometry),useTsF(false),F(_F),hoCoords(ho_coords),useReal(use_real) { }

		VectorDouble Nf;

		/** \brief calculate Incident wave on rigid scatterer
		  *
		  * F = int_S N^T * g gradient (exp(i*k*x dot d) ) * n dS  , F = -du_inc/dr represents sound hard wave (mere reflecting no absorption)
		  *
		  */
		PetscErrorCode doWork(
			int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
			PetscFunctionBegin;

			if(data.getIndices().size()==0) PetscFunctionReturn(0);
			if(dAta.tRis.find(getMoFEMFEPtr()->get_ent())==dAta.tRis.end()) PetscFunctionReturn(0);

			PetscErrorCode ierr;

			const FENumeredDofMoFEMEntity *dof_ptr;
			ierr = getMoFEMFEPtr()->get_row_dofs_by_petsc_gloabl_dof_idx(data.getIndices()[0],&dof_ptr); CHKERRQ(ierr);
			int rank = dof_ptr->get_max_rank();

			int nb_dofs = data.getIndices().size();

			Nf.resize(data.getIndices().size());
			fill(Nf.begin(),Nf.end(),0);

			//cerr << getNormal() << endl;
			//cerr << getNormals_at_GaussPt() << endl;
			double wAvenumber = datA.aNgularfreq/datA.sPeed;
			unsigned int nb_gauss_pts = data.getN().size1();

			for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

				double val = getGaussPts()(2,gg);

				double iNcidentwave;
				////////////////////********************************/////////////

				double x,y,z;


				if(hoCoords.size1() == nb_gauss_pts) {


					double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
					val *= area;
					x = hoCoords(gg,0);
					y = hoCoords(gg,1);
					z = hoCoords(gg,2);
				} else {
					val *= getArea();
					x = getCoordsAtGaussPts()(gg,0);
					y = getCoordsAtGaussPts()(gg,1);
					z = getCoordsAtGaussPts()(gg,2);
				}
				/***** incident wave in x direction *****/
				/*** incident wave from Finite Element Analysis of Acoustic Scattering by Frank Ihlenburg **/
				//const double pi = atan( 1.0 ) * 4.0;

				//double theta = atan2(y,x); //the arctan of radians (y/x)
				double theta = atan2(y,x)+2*M_PI;
				//cout << "\n theta = \n" << theta << "\n M_PI = \N" << M_PI << endl;
				const double k = wAvenumber;  //Wave number
				const double a = 0.5;         //radius of the sphere,wait to modify by user
				const double const1 = k * a;

				const complex< double > i( 0.0, 1.0 );

				//// magnitude of incident wave
				const double phi_incident_mag = 1.0;

				const double tol = 1.0e-10;
				double max = 0.0;
				double min = 999999.0;
				complex< double > result = 0.0;
				complex< double > prev_result;
				double error = 100.0;

				/**** Spherical incident wave ***/

				unsigned int n = 0; //initialized the infinite series loop

				while( error > tol )  //finding the acoustic potential
				{
					double jn_der = ( n / const1 * sph_bessel( n, const1 ) - sph_bessel( n + 1, const1 ) );  //The derivative of bessel function

					double Pn = legendre_p( n, cos( theta ) );  //Legendre

					prev_result = result;

					result += pow( i, n ) * ( 2.0 * n + 1.0 ) * Pn * jn_der;  //edition from Papers
					error = abs( abs( result ) - abs( prev_result ) );
					++n;
				}
				/**    End     **/

				/*** Cyclindrical incident wave ***/
				//double R = sqrt(pow(x,2.0)+pow(y,2.0)); //radius

				//unsigned int n = 1; //initialized the infinite series loop

				//double Jn_der_zero = ( - cyl_bessel_j( 1, const1 ));

				////n=0;
				//result -= Jn_der_zero;
				//
				//while( error > tol )  //finding the acoustic potential in one single point.
				//{
				//	prev_result = result;
				//	//The derivative of bessel function
				//	double Jn_der = (n / const1 * cyl_bessel_j( n, const1 ) - cyl_bessel_j( n + 1, const1 ));
				//
				//	result -= 2.0 * pow( i, n ) * Jn_der * cos(n*theta);
				//	error = abs( abs( result ) - abs( prev_result ) );
				//	++n;
				//}

				/** 	End     **/
				//result = i * k * cos( theta ) * exp( i * k * R * cos( theta ) ); //derivative of incident wave


				if(useReal){
					iNcidentwave = std::real(result);

				} else if(!useReal) {
					iNcidentwave = std::imag(result);

				}


				ublas::noalias(Nf) += val*iNcidentwave*data.getN(gg,nb_dofs);
			}

			if(useTsF) {
				ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),
									&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
			} else {
				ierr = VecSetValues(F,data.getIndices().size(),
									&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
			}

			PetscFunctionReturn(0);
		}

	};


	/// \biref operator to calculate Impedance on body surface and assemble to imaginary lhs of equations
	struct OpImpedanceLhs_reimC:public TriElementForcesAndSurcesCore::UserDataOperator {

		ImpedanceData &dAta;
		BlockData &blockData;
		MatrixDouble &hoCoords;
		bool ho_geometry;
		bool useTsB;

		OpImpedanceLhs_reimC(const string re_field_name,const string im_field_name,
						ImpedanceData &data,BlockData &block_data,MatrixDouble &ho_coords,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),ho_geometry(_ho_geometry),useTsB(true),blockData(block_data),hoCoords(ho_coords) {}

		Mat A;
		OpImpedanceLhs_reimC(const string re_field_name,const string im_field_name,Mat _A,
						ImpedanceData &data,BlockData &block_data,MatrixDouble &ho_coords,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),ho_geometry(_ho_geometry),useTsB(false),A(_A),blockData(block_data),hoCoords(ho_coords) {}

		MatrixDouble K,transK;
		/** \brief calculate helmholtz Impedance term in the imaginary lhs of equations
		 *
		 * K = intS N^T i sIgma K N dS
		 */
		PetscErrorCode doWork(
			int row_side,int col_side,
			EntityType row_type,EntityType col_type,
			DataForcesAndSurcesCore::EntData &row_data,
			DataForcesAndSurcesCore::EntData &col_data) {
			PetscFunctionBegin;

			try {

				if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
				if(col_data.getIndices().size()==0) PetscFunctionReturn(0);

				int nb_row = row_data.getIndices().size();
				int nb_col = col_data.getIndices().size();
				K.resize(nb_row,nb_col);
				bzero(&*K.data().begin(),nb_row*nb_col*sizeof(double));

				for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {  /* Integrate the shape functions in one element */
					double waveNumber = blockData.aNgularfreq/blockData.sPeed;
              		double val = getGaussPts()(2,gg);

					unsigned int nb_gauss_pts = row_data.getN().size1();
					double impedanceConst = dAta.sIgma*waveNumber;
					if(hoCoords.size1() == nb_gauss_pts) {
						double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
						val *= area;
					}   else {
						val *= getArea();
					}
					noalias(K) += val*impedanceConst*outer_prod( row_data.getN(gg,nb_row),col_data.getN(gg,nb_col) );

				}

				PetscErrorCode ierr;
				if(!useTsB) {
					const_cast<FEMethod*>(getFEMethod())->ts_B = A;  //to change the pointer getFEMethod()'s private member ts_B value with A.
				}                                                                    //getFEMethod() belong to class MoFEM::Interface::FEMethod
				ierr = MatSetValues(
						   (getFEMethod()->ts_B),
						   nb_row,&row_data.getIndices()[0],              //MatSetValues(A,i,m,j,n,value,INSERT_VALUES,ierr), i*j the size of input matrix,
						   nb_col,&col_data.getIndices()[0],              // m and n - the position of parent matrix to allowed the input matrix.
						   &K(0,0),ADD_VALUES); CHKERRQ(ierr);
				if(row_side != col_side || row_type != col_type) {
					transK.resize(nb_col,nb_row);
					noalias(transK) = trans( K );
					ierr = MatSetValues(
							   (getFEMethod()->ts_B),
							   nb_col,&col_data.getIndices()[0],
							   nb_row,&row_data.getIndices()[0],
							   &transK(0,0),ADD_VALUES); CHKERRQ(ierr);
				}


			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};


	/// \biref operator to calculate Impedance on body surface and assemble to imaginary lhs of equations
	struct OpImpedanceLhs_imreC:public TriElementForcesAndSurcesCore::UserDataOperator {

		ImpedanceData &dAta;
		BlockData &blockData;
		MatrixDouble &hoCoords;
		bool ho_geometry;
		bool useTsB;

		OpImpedanceLhs_imreC(const string re_field_name,const string im_field_name,
					   ImpedanceData &data,BlockData &block_data,MatrixDouble &ho_coords,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),ho_geometry(_ho_geometry),useTsB(true),blockData(block_data),hoCoords(ho_coords) {}

		Mat A;
		OpImpedanceLhs_imreC(const string re_field_name,const string im_field_name,Mat _A,
					   ImpedanceData &data,BlockData &block_data,MatrixDouble &ho_coords,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),ho_geometry(_ho_geometry),useTsB(false),A(_A),blockData(block_data),hoCoords(ho_coords) {}

		MatrixDouble K,transK;
		/** \brief calculate helmholtz Impedance term in the imaginary lhs of equations
		 *
		 * K = - intS N^T i sIgma K N dS
		 */
		PetscErrorCode doWork(
			int row_side,int col_side,
			EntityType row_type,EntityType col_type,
			DataForcesAndSurcesCore::EntData &row_data,
			DataForcesAndSurcesCore::EntData &col_data) {
			PetscFunctionBegin;

			try {

				if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
				if(col_data.getIndices().size()==0) PetscFunctionReturn(0);

				int nb_row = row_data.getIndices().size();
				int nb_col = col_data.getIndices().size();
				K.resize(nb_row,nb_col);
				bzero(&*K.data().begin(),nb_row*nb_col*sizeof(double));

				for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {

					double waveNumber = blockData.aNgularfreq/blockData.sPeed;
              		double val = getGaussPts()(2,gg);

					unsigned int nb_gauss_pts = row_data.getN().size1();
					double impedanceConst = -dAta.sIgma*waveNumber;
					if(hoCoords.size1() == nb_gauss_pts) {
						double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
						val *= area;
					}   else {
						val *= getArea();
					}
					noalias(K) += val*impedanceConst*outer_prod( row_data.getN(gg,nb_row),col_data.getN(gg,nb_col) );

				}

				PetscErrorCode ierr;
				if(!useTsB) {
					const_cast<FEMethod*>(getFEMethod())->ts_B = A;  //to change the pointer getFEMethod()'s private member ts_B value with A.
				}                                                                    //getFEMethod() belong to class MoFEM::Interface::FEMethod
				ierr = MatSetValues(
						   (getFEMethod()->ts_B),
						   nb_row,&row_data.getIndices()[0],              //MatSetValues(A,i,m,j,n,value,INSERT_VALUES,ierr), i*j the size of input matrix,
						   nb_col,&col_data.getIndices()[0],              // m and n - the position of parent matrix to allowed the input matrix.
						   &K(0,0),ADD_VALUES); CHKERRQ(ierr);
				if(row_side != col_side || row_type != col_type) {
					transK.resize(nb_col,nb_row);
					noalias(transK) = trans( K );
					ierr = MatSetValues(
							   (getFEMethod()->ts_B),
							   nb_col,&col_data.getIndices()[0],
							   nb_row,&row_data.getIndices()[0],
							   &transK(0,0),ADD_VALUES); CHKERRQ(ierr);
				}


			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};

	struct OpImpedanceLhs_D:public TriElementForcesAndSurcesCore::UserDataOperator {

		ImpedanceData &dAta;
		BlockData &blockData;
		MatrixDouble &hoCoords;
		bool ho_geometry;
		bool useTsB;

		OpImpedanceLhs_D(const string re_field_name,const string im_field_name,
							 ImpedanceData &data,BlockData &block_data,MatrixDouble &ho_coords,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),ho_geometry(_ho_geometry),useTsB(true),blockData(block_data),hoCoords(ho_coords) {}

		Mat A;
		OpImpedanceLhs_D(const string re_field_name,const string im_field_name,Mat _A,
							 ImpedanceData &data,BlockData &block_data,MatrixDouble &ho_coords,bool _ho_geometry = false):
			TriElementForcesAndSurcesCore::UserDataOperator(re_field_name,im_field_name),
			dAta(data),ho_geometry(_ho_geometry),useTsB(false),A(_A),blockData(block_data),hoCoords(ho_coords) {}

		MatrixDouble K,transK;
		/** \brief calculate helmholtz Impedance term in the real part of lhs equations
		 *
		 * K = intS 1/R N^T N dS
		 * see <<Plane-wave basis finite elements and boundary elements for three-dimensional wave scattering>>
		 * by E. Perrey-Debain, O. Laghrouche, P. Bettess and J. Trevelyan for details.
		 */
		PetscErrorCode doWork(
			int row_side,int col_side,
			EntityType row_type,EntityType col_type,
			DataForcesAndSurcesCore::EntData &row_data,
			DataForcesAndSurcesCore::EntData &col_data) {
			PetscFunctionBegin;

			try {

				if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
				if(col_data.getIndices().size()==0) PetscFunctionReturn(0);

				int nb_row = row_data.getIndices().size();
				int nb_col = col_data.getIndices().size();
				K.resize(nb_row,nb_col);
				bzero(&*K.data().begin(),nb_row*nb_col*sizeof(double));

				for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {
              		double val = getGaussPts()(2,gg);

					unsigned int nb_gauss_pts = row_data.getN().size1();
					double x,y,z;
					if(hoCoords.size1() == nb_gauss_pts) {
						double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
						val *= area;
						x = hoCoords(gg,0);
						y = hoCoords(gg,1);
						z = hoCoords(gg,2);
					}   else {
						val *= getArea();
						x = getCoordsAtGaussPts()(gg,0);
						y = getCoordsAtGaussPts()(gg,1);
						z = getCoordsAtGaussPts()(gg,2);
					}
					double r_inv = 1/(2*(sqrt(pow(x,2.0)+pow(y,2.0)))); //inverse radius
					noalias(K) += val*r_inv*outer_prod( row_data.getN(gg,nb_row),col_data.getN(gg,nb_col) );

				}


				PetscErrorCode ierr;
				if(!useTsB) {
					const_cast<FEMethod*>(getFEMethod())->ts_B = A;
				}
				ierr = MatSetValues(
						   (getFEMethod()->ts_B),
						   nb_row,&row_data.getIndices()[0],
						   nb_col,&col_data.getIndices()[0],
						   &K(0,0),ADD_VALUES); CHKERRQ(ierr);
				if(row_side != col_side || row_type != col_type) {
					transK.resize(nb_col,nb_row);
					noalias(transK) = trans( K );
					ierr = MatSetValues(
							   (getFEMethod()->ts_B),
							   nb_col,&col_data.getIndices()[0],
							   nb_row,&row_data.getIndices()[0],
							   &transK(0,0),ADD_VALUES); CHKERRQ(ierr);
				}


			} catch (const std::exception& ex) {
				ostringstream ss;
				ss << "throw in method: " << ex.what() << endl;
				SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
			}

			PetscFunctionReturn(0);
		}

	};


	/** \brief add helmholtz element on tets
	  * \infroup mofem_helmholtz_elem
	  *
	  * It get data from block set and define element in moab
	  *w
	  * \param problem name
	  * \param field name
	  * \param name of mesh nodal positions (if not defined nodal coordinates are used)
	  */
	PetscErrorCode addHelmholtzElements(
		const string problem_name,const string re_field_name,const string im_field_name,const string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
		PetscFunctionBegin;

		PetscErrorCode ierr;
		ErrorCode rval;

		//to do matrix A and C; add Finite Element "HELMHOLTZ_FE"
		ierr = mField.add_finite_element("HELMHOLTZ_FE",MF_ZERO); CHKERRQ(ierr );
		ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_FE",im_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_FE",im_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_FE",im_field_name); CHKERRQ(ierr);

		if(mField.check_field(mesh_nodals_positions)) {
			ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_FE",mesh_nodals_positions); CHKERRQ(ierr);
		}
		ierr = mField.modify_problem_add_finite_element(problem_name,"HELMHOLTZ_FE"); CHKERRQ(ierr);

		//takes skin of block of entities
		//Skinner skin(&mField.get_moab());
		// loop over all blocksets and get data which name is FluidPressure

		for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {

			if(it->get_Cubit_name().compare(0,13,"MAT_HELMHOLTZ") == 0) {

				//get block attributes
				vector<double> attributes;
				ierr = it->get_Cubit_attributes(attributes); CHKERRQ(ierr);
				if(attributes.size()<2) {
					SETERRQ1(PETSC_COMM_SELF,1,"not enough block attributes to deffine fluid pressure element, attributes.size() = %d ",attributes.size());
				}

				blockData.aNgularfreq = attributes[0];
				blockData.sPeed = attributes[1];
				setOfBlocks[it->get_msId()].aNgularfreq = attributes[0];
				setOfBlocks[it->get_msId()].sPeed = attributes[1];

				rval = mField.get_moab().get_entities_by_type(it->meshset,MBTET,setOfBlocks[it->get_msId()].tEts,true); CHKERR_PETSC(rval);
				ierr = mField.add_ents_to_finite_element_by_TETs(setOfBlocks[it->get_msId()].tEts,"HELMHOLTZ_FE"); CHKERRQ(ierr);
			}
		}

		PetscFunctionReturn(0);
	}

	/** \brief add helmholtz flux element
	  * \infroup mofem_helmholtz_elem
	  *
	  * It get data from helmholtz flux set and define elemenet in moab. Alternatively
	  * uses block set with name HELMHOLTZ_FLUX.
	  *
	  * \param problem name
	  * \param field name
	  * \param name of mesh nodal positions (if not defined nodal coordinates are used)
	  */
	PetscErrorCode addHelmholtzFluxElement(
		const string problem_name,const string re_field_name,const string im_field_name) {
		PetscFunctionBegin;

		PetscErrorCode ierr;
		ErrorCode rval;

		ierr = mField.add_finite_element("HELMHOLTZ_FLUX_FE",MF_ZERO); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_FLUX_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_FLUX_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_FLUX_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_FLUX_FE",im_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_FLUX_FE",im_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_FLUX_FE",im_field_name); CHKERRQ(ierr);

		if(mField.check_field("MESH_NODE_POSITIONS")) {
			ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_FLUX_FE","MESH_NODE_POSITIONS"); CHKERRQ(ierr);
		}

		ierr = mField.modify_problem_add_finite_element(problem_name,"HELMHOLTZ_FLUX_FE"); CHKERRQ(ierr);

		for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(mField,SIDESET|HEATFLUXSET,it)) {
			ierr = it->get_cubit_bc_data_structure(setOfFluxes[it->get_msId()].dAta); CHKERRQ(ierr);
			rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,setOfFluxes[it->get_msId()].tRis,true); CHKERR_PETSC(rval);
			ierr = mField.add_ents_to_finite_element_by_TRIs(setOfFluxes[it->get_msId()].tRis,"HELMHOLTZ_FLUX_FE"); CHKERRQ(ierr);
		}

		//this is alternative method for setting boundary conditions, to bypass bu in cubit file reader.
		//not elegant, but good enough
		for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {
			if(it->get_Cubit_name().compare(0,9,"HELMHOLTZ_FLUX") == 0) {
				vector<double> data;
				ierr = it->get_Cubit_attributes(data); CHKERRQ(ierr);
				if(data.size()!=1) {
					SETERRQ(PETSC_COMM_SELF,1,"Data inconsistency");
				}
				strcpy(setOfFluxes[it->get_msId()].dAta.data.name,"HelmholtzFlux");
				setOfFluxes[it->get_msId()].dAta.data.flag1 = 1;
				setOfFluxes[it->get_msId()].dAta.data.value1 = data[0];
				//cerr << setOfFluxes[it->get_msId()].dAta << endl;
				rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,setOfFluxes[it->get_msId()].tRis,true); CHKERR_PETSC(rval);
				ierr = mField.add_ents_to_finite_element_by_TRIs(setOfFluxes[it->get_msId()].tRis,"HELMHOLTZ_FLUX_FE"); CHKERRQ(ierr);

			}
		}


		PetscFunctionReturn(0);
	}



	/** \brief add helmholtz flux element
	  * \infroup mofem_helmholtz_elem
	  *
	  * It get data from helmholtz flux set and define elemenet in moab. Alternatively
	  * uses block set with name HELMHOLTZ_FLUX.
	  *
	  * \param problem name
	  * \param field name
	  * \param name of mesh nodal positions (if not defined nodal coordinates are used)
	  */
	PetscErrorCode addHelmholtzIncidentElement(
		const string problem_name,const string re_field_name,const string im_field_name) {
		PetscFunctionBegin;

		PetscErrorCode ierr;
		ErrorCode rval;

		ierr = mField.add_finite_element("HELMHOLTZ_INCIDENT_FE",MF_ZERO); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_INCIDENT_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_INCIDENT_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_INCIDENT_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_INCIDENT_FE",im_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_INCIDENT_FE",im_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_INCIDENT_FE",im_field_name); CHKERRQ(ierr);

		if(mField.check_field("MESH_NODE_POSITIONS")) {
			ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_INCIDENT_FE","MESH_NODE_POSITIONS"); CHKERRQ(ierr);
		}

		ierr = mField.modify_problem_add_finite_element(problem_name,"HELMHOLTZ_INCIDENT_FE"); CHKERRQ(ierr);

		//this is alternative method for setting boundary conditions, to bypass bu in cubit file reader.
		//not elegant, but good enough
		for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {
			if(it->get_Cubit_name().compare(0,9,"INCIDENT") == 0) {
				vector<double> data;
				ierr = it->get_Cubit_attributes(data); CHKERRQ(ierr);
				if(data.size()!=1) {
					SETERRQ(PETSC_COMM_SELF,1,"Data inconsistency");
				}

				setOfIncident[it->get_msId()].amplitude = data[0];

				//cerr << setOfIncident[it->get_msId()].dAta << endl;
				rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,setOfIncident[it->get_msId()].tRis,true); CHKERR_PETSC(rval);
				ierr = mField.add_ents_to_finite_element_by_TRIs(setOfIncident[it->get_msId()].tRis,"HELMHOLTZ_INCIDENT_FE"); CHKERRQ(ierr);

			}
		}


		PetscFunctionReturn(0);
	}


	/** \brief add Impedance element
	* \infroup mofem_helmholtz_elem
	*
	* It get data from convection set and define elemenet in moab. Alternatively
	* uses block set with name IMPEDANCE.
	*
	* \param problem name
	* \param field name
	* \param name of mesh nodal positions (if not defined nodal coordinates are used)
	*/
	PetscErrorCode addHelmholtzImpedanceElement(
		const string problem_name,const string re_field_name,const string im_field_name) {
		PetscFunctionBegin;

		PetscErrorCode ierr;
		ErrorCode rval;

		ierr = mField.add_finite_element("HELMHOLTZ_IMPEDANCE_FE",MF_ZERO); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_IMPEDANCE_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_IMPEDANCE_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_IMPEDANCE_FE",re_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_IMPEDANCE_FE",im_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_IMPEDANCE_FE",im_field_name); CHKERRQ(ierr);
		ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_IMPEDANCE_FE",im_field_name); CHKERRQ(ierr);

		ierr = mField.modify_problem_add_finite_element(problem_name,"HELMHOLTZ_IMPEDANCE_FE"); CHKERRQ(ierr);

		for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {
			if(it->get_Cubit_name().compare(0,9,"IMPEDANCE") == 0) {

				vector<double> data;
				ierr = it->get_Cubit_attributes(data); CHKERRQ(ierr);
				if(data.size()!=2) {
					SETERRQ(PETSC_COMM_SELF,1,"Data inconsistency");
				}
				setOfImpedance[it->get_msId()].g = data[0];
				setOfImpedance[it->get_msId()].sIgma = data[1];
				rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,setOfImpedance[it->get_msId()].tRis,true); CHKERR_PETSC(rval);
				ierr = mField.add_ents_to_finite_element_by_TRIs(setOfImpedance[it->get_msId()].tRis,"HELMHOLTZ_IMPEDANCE_FE"); CHKERRQ(ierr);

			}
		}


		PetscFunctionReturn(0);
	}


	/** \brief this function is used in case of stationary problem to set elements for rhs
	  * \infroup mofem_helmholtz_elem
	  */
	PetscErrorCode setHelmholtzFiniteElementRhs_FOperators(string re_field_name,string im_field_name,Vec &F,bool useScalar,const string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
		PetscFunctionBegin;
		map<int,BlockData>::iterator sit = setOfBlocks.begin();

		if(mField.check_field(mesh_nodals_positions)) {

			//feIncidentWave.get_op_to_do_Rhs().push_back(new OpHoCoordTri(mesh_nodals_positions,hoCoordsTri));

			feRhs.get_op_to_do_Rhs().push_back(new OpHoCoordTet(mesh_nodals_positions,hoCoordsTet));

		}

		for(;sit!=setOfBlocks.end();sit++) {
			//add finite element
			feRhs.get_op_to_do_Rhs().push_back(new OpHelmholtzRhs_F(re_field_name,re_field_name,F,sit->second,commonData,hoCoordsTet,useScalar,true));
			feRhs.get_op_to_do_Rhs().push_back(new OpHelmholtzRhs_F(im_field_name,im_field_name,F,sit->second,commonData,hoCoordsTet,useScalar,false));
		}
		PetscFunctionReturn(0);
	}

	PetscErrorCode setHelmholtzFiniteElementRhsOperators(string re_field_name,string im_field_name,Vec &F,bool useImpedance) {
		PetscFunctionBegin;

		map<int,BlockData>::iterator sit = setOfBlocks.begin();

		for(;sit!=setOfBlocks.end();sit++) {

			feRhs.get_op_to_do_Rhs().push_back(new OpGetGradReAtGaussPts(re_field_name,commonData));
			feRhs.get_op_to_do_Rhs().push_back(new OpGetTetPressureReAtGaussPts(re_field_name,commonData));
			feRhs.get_op_to_do_Rhs().push_back(new OpHelmholtzRhs_Re(re_field_name,re_field_name,F,sit->second,commonData));

			feRhs.get_op_to_do_Rhs().push_back(new OpGetGradImAtGaussPts(im_field_name,commonData));
			feRhs.get_op_to_do_Rhs().push_back(new OpGetTetPressureImAtGaussPts(im_field_name,commonData));
			feRhs.get_op_to_do_Rhs().push_back(new OpHelmholtzRhs_Im(im_field_name,im_field_name,F,sit->second,commonData));

			//cout << "\n useImpedance = \n" << useImpedance << std::endl;
			//FIXE ME, NOT WORKING
			//if(useImpedance) {
			//	feIncidentWave.get_op_to_do_Rhs().push_back(new OpHelmholtzRhs_impedance(re_field_name,re_field_name,F,sit->second,commonData,true));
			//	feIncidentWave.get_op_to_do_Rhs().push_back(new OpHelmholtzRhs_impedance(im_field_name,im_field_name,F,sit->second,commonData,false));
			//}
			//add finite element
		}

		PetscFunctionReturn(0);
	}



	/** \brief this fucntion is used in case of stationary Helmholtz problem for lhs stiffness term
	  * \infroup mofem_helmholtz_elem
	  */
	PetscErrorCode setHelmholtzFiniteElementLhsOperators(string re_field_name,string im_field_name,Mat A) {
		PetscFunctionBegin;
		map<int,BlockData>::iterator sit = setOfBlocks.begin();
		for(;sit!=setOfBlocks.end();sit++) {
			//add finite elemen
			feLhs.get_op_to_do_Lhs().push_back(new OpHelmholtzLhs_A(re_field_name,re_field_name,A,sit->second,commonData,true));
			feLhs.get_op_to_do_Lhs().push_back(new OpHelmholtzLhs_A(im_field_name,im_field_name,A,sit->second,commonData,false));
		}
		PetscFunctionReturn(0);
	}


	PetscErrorCode setHelmholtzFluxFiniteElementRhsOperators(string re_field_name,string im_field_name,Vec &F,const string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
		PetscFunctionBegin;
		bool ho_geometry = false;
		if(mField.check_field(mesh_nodals_positions)) {
			ho_geometry = true;
		}
		map<int,FluxData>::iterator sit = setOfFluxes.begin();
		for(;sit!=setOfFluxes.end();sit++) {
			//add finite element
			feFlux.get_op_to_do_Rhs().push_back(new OpHelmholtzFlux(re_field_name,im_field_name,F,blockData,sit->second,ho_geometry));
		}
		PetscFunctionReturn(0);
	}
	/** \brief this function is used in case of statonary for helmholtz incident wave flux terms

	*/

	PetscErrorCode setHelmholtzIncidentWaveFiniteElementRhsOperators(string re_field_name,string im_field_name,Vec &F,const string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
		PetscFunctionBegin;
		bool ho_geometry = false;
		map<int,IncidentData>::iterator sit = setOfIncident.begin();

		if(mField.check_field(mesh_nodals_positions)) {

			feIncidentWave.get_op_to_do_Rhs().push_back(new OpHoCoordTri(mesh_nodals_positions,hoCoordsTri));
		}

		for(;sit!=setOfIncident.end();sit++) {
			//add finite element
			feIncidentWave.get_op_to_do_Rhs().push_back(new OpHelmholtzIncidentWave(re_field_name,re_field_name,F,blockData,sit->second,hoCoordsTri,true,ho_geometry));
			feIncidentWave.get_op_to_do_Rhs().push_back(new OpHelmholtzIncidentWave(im_field_name,im_field_name,F,blockData,sit->second,hoCoordsTri,false,ho_geometry));

		}
		PetscFunctionReturn(0);
	}

	PetscErrorCode setHelmholtzImpedanceFiniteElementLhsOperators(string re_field_name,string im_field_name,Mat A,const string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
		PetscFunctionBegin;
		bool ho_geometry = false;
		//if(mField.check_field(mesh_nodals_positions)) {
		//	ho_geometry = true;
		//}
		map<int,ImpedanceData>::iterator sit = setOfImpedance.begin();

		if(mField.check_field(mesh_nodals_positions)) {
			//	ho_geometry = true;
			feIncidentWave.get_op_to_do_Rhs().push_back(new OpHoCoordTri(mesh_nodals_positions,hoCoordsTri));
		}

		for(;sit!=setOfImpedance.end();sit++) {
			//add finite element
			feImpedanceLhs.get_op_to_do_Lhs().push_back(new OpImpedanceLhs_D(re_field_name,re_field_name,A,sit->second,blockData,hoCoordsTri,ho_geometry));
			feImpedanceLhs.get_op_to_do_Lhs().push_back(new OpImpedanceLhs_D(im_field_name,im_field_name,A,sit->second,blockData,hoCoordsTri,ho_geometry));
			feImpedanceLhs.get_op_to_do_Lhs().push_back(new OpImpedanceLhs_reimC(re_field_name,im_field_name,A,sit->second,blockData,hoCoordsTri,ho_geometry));
			feImpedanceLhs.get_op_to_do_Lhs().push_back(new OpImpedanceLhs_imreC(im_field_name,re_field_name,A,sit->second,blockData,hoCoordsTri,ho_geometry));
		}
		PetscFunctionReturn(0);
	}

	};

	}

	#endif //__HELMHOLTZ_ELEMENT_HPP

	/***************************************************************************//**
	 * \defgroup mofem_helmholtz_elem Helmholtz element
	 * \ingroup mofem_forces_and_sources
	 ******************************************************************************/
