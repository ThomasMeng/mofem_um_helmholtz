/** \file AnalyticalSolutions.hpp
  \ingroup mofem_helmholtz_elem

  Analytical solutions

 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.

 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
*/

/** \brief Generic structure for analytical function
  \ingroup mofem_helmholtz_elem
  \bug point source example not implemented.
*/


struct GenericAnalyticalSolution {

  enum VALUE_TYPE { REAL = 0, IMAG, LAST_VAL_TYPE };

  virtual vector<VectorDouble >& operator()(double x, double y, double z) = 0;
  virtual ~GenericAnalyticalSolution() {}

};

/** List of analytical solution
  \ingroup mofem_helmholtz_elem
*/
enum AnalyticalSolutionTypes {
  HARD_SPHERE_SCATTER_WAVE,
  SOFT_SPHERE_SCATTER_WAVE,
  PLANE_WAVE,
  HARD_CYLINDER_SCATTER_WAVE,
  SOFT_CYLINDER_SCATTER_WAVE,
  SINGULAR_SCATTER_WAVE,
  INCIDENT_WAVE,
  NO_ANALYTICAL_SOLUTION
};

/** Line command list of analytical solutions
  \ingroup mofem_helmholtz_elem
*/
const char *analytical_solution_types[] = {
  "hard_sphere_incident_wave",
  "soft_sphere_incident_wave",
  "plane_wave",
  "hard_cylinder_scatter_wave",
  "soft_cylinder_scatter_wave",
  "singular_scatter_wave",
  "incident_wave",
  "no_analytical_solution"
};

// **** Analytical solutions ****

/** Incident wave
  \ingroup mofem_helmholtz_elem


  Equation from:
  Ihlenburg,Finite element analysis of acoustic scattering Springer Science & Business Media.

  Some details can be found here:
  <http://ansol.us/Products/Coustyx/Validation/MultiDomain/Scattering/PlaneWave/HardSphere/Downloads/dataset_description.pdf>

  \f[
  p_\textrm{inc} = \exp(ikd \cdot \mathbf{x})
  \f]
  FIXME
  Rayleigh with attenuation of the incident wave need to be implemented.

  */
struct IncidentWave: public GenericAnalyticalSolution {

  vector<VectorDouble > rEsult;
  double wAvenumber;
  double aTtenuation;
  double rAdius;
  double complexWaveNumber;
  double fRequency;
  double sPeed;
  VectorDouble dIrection;
  VectorDouble cOordinate;
  double transmissionCoefficient;
  double amplitudeReal; ///< The real amplitude of the incident wave
  double amplitudeImag;
  double pHase;
  bool isRayleigh;

  IncidentWave() {}
  IncidentWave(double wavenumber,VectorDouble d,bool is_rayleigh = false,double attenuation = 0,double radius = 0, double leaky_saw = 0,double frequency = 0,double velocity = 0,double transmission_coefficient = 1,double r_amplitude = 1,double i_amplitude = 0,double phase = 0):
    wAvenumber(wavenumber),
    dIrection(d),
    isRayleigh(is_rayleigh),
    aTtenuation(attenuation),
    rAdius(radius),
    complexWaveNumber(leaky_saw),
    fRequency(frequency),
    sPeed(velocity),
    transmissionCoefficient(transmission_coefficient),
    amplitudeReal(r_amplitude),
    amplitudeImag(i_amplitude),
    pHase(phase)
    {}

  ~IncidentWave() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const complex< double > i( 0.0, 1.0 );
    complex< double > result = 0.0;
    cOordinate.resize(3);
    cOordinate[0] = x;
    cOordinate[1] = y;
    cOordinate[2] = z;
    complex< double > attenuationR(0,0);
    complex< double > waveNumber =  wAvenumber + i*complexWaveNumber;
    if(isRayleigh) {
      /* speed of SAW over speed of longitudinal wave */
      complex< double > vs_over_v1 = (2.0*M_PI*fRequency/waveNumber) / sPeed;
      attenuationR = sqrt(1.0 - pow(vs_over_v1,2.0))*(y);
      /* attenuationR of Rayleigh inside fluid */

      /* double totalViscosity = (4/3) * 0.001139 + 0.0034 = 0.004900  */
      /* sec(Rayleigh angle) = sec(22 degree) = 1.0790 */
      // double oMega = 2*M_PI*fRequency;
      // double dEnsity = 998;
      // attenuationR = (0.0049*pow(oMega,2.0)) / (dEnsity*pow(sPeed,3.0));
      // attenuationR *= - 1.0790*abs(y);
    }

    if(aTtenuation != 0) {
      double xOriginal = - std::sqrt(std::pow(rAdius,2.0) - std::pow(z,2.0));
      aTtenuation *= (x - xOriginal);
    }


    result = transmissionCoefficient*(amplitudeReal+i*amplitudeImag)*exp(i*waveNumber*inner_prod(dIrection,cOordinate)+i*pHase+waveNumber*attenuationR - aTtenuation);


    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};

struct IncidentWavePointSource: public GenericAnalyticalSolution {

  vector<VectorDouble > rEsult;
  double wAvenumber;
  double aTtenuation;
  double rAdius;
  double complexWaveNumber;
  double fRequency;
  double sPeed;
  double transmissionCoefficient;
  VectorDouble pOsition;
  VectorDouble sourceCoordinate;
  double amplitudeReal; ///< The real amplitude of the incident wave
  double amplitudeImag;
  bool isRayleigh;

  IncidentWavePointSource() {}
  IncidentWavePointSource(double wavenumber,VectorDouble d,bool is_rayleigh = false,double attenuation = 0,double radius = 0,double leaky_saw = 0,double frequency = 0,double velocity = 0,double transmission_coefficient = 1,double r_amplitude = 1,double i_amplitude = 0):
    wAvenumber(wavenumber),
    aTtenuation(attenuation),
    rAdius(radius),
    complexWaveNumber(leaky_saw),
    fRequency(frequency),
    sPeed(velocity),
    transmissionCoefficient(transmission_coefficient),
    sourceCoordinate(d),
    isRayleigh(is_rayleigh),
    amplitudeReal(r_amplitude),
    amplitudeImag(i_amplitude)
    {}

  ~IncidentWavePointSource() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const complex< double > i( 0.0, 1.0 );
    complex< double > result = 0.0;
    pOsition.resize(3);

    pOsition[0] = (x-sourceCoordinate[0])*(x-sourceCoordinate[0]);
    pOsition[1] = (y-sourceCoordinate[1])*(y-sourceCoordinate[1]);
    pOsition[2] = (z-sourceCoordinate[2])*(z-sourceCoordinate[2]);
    double absPoint = sqrt(pOsition[0]+pOsition[1]+pOsition[2]);
    complex<double> amplitude = amplitudeReal+i*amplitudeImag;
    complex< double > attenuationR(0,0);
    complex< double > waveNumber =  wAvenumber + i*complexWaveNumber;
    if(isRayleigh) {
      /* speed of SAW over speed of longitudinal wave */
      // complex< double > vs_over_v1 = (2.0*M_PI*fRequency/waveNumber) / sPeed;
      // attenuationR = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
      /* attenuationR of Rayleigh inside fluid */
      double oMega = 2*M_PI*fRequency;
      /* double totalViscosity = (4/3) * 0.001139 + 0.0034 = 0.004900  */
      /* sec(Rayleigh angle) = sec(22 degree) = 1.0790 */
      double dEnsity = 998;
      attenuationR = (0.0049*pow(oMega,2.0)) / (dEnsity*pow(sPeed,3.0));
      attenuationR *= - 1.0790*abs(y);
    }

    if(aTtenuation != 0) {
      double xOriginal = - std::sqrt(std::pow(rAdius,2.0) - std::pow(z,2.0));
      aTtenuation *= (x - xOriginal);
    }

    result = transmissionCoefficient*amplitude*(exp(-i*waveNumber*absPoint+waveNumber*attenuationR - aTtenuation)) / (4*M_PI*absPoint);

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};


#ifdef KISS_FFT_H

/**

 Incident wave, i.t. is inverse Fourier transform evaluated at arbitrary
 spatial points.

 \f[
 \left. \left\{ \frac{1}{n}  (A_{0} e^{ik \mathbf{d} \cdot \mathbf{x} + i \phi})
 \right\}
 \f]
 where \f$\phi\f$ is
 \f[


 \f]

  */
struct IncidentWaveDFT: public GenericAnalyticalSolution {

  vector<VectorDouble > rEsult;
  double signalLength;
  double signalDuration;
  double aTtenuation;
  double rAdius;
  double complexWaveNumber;
  double fRequency;
  double sPeed;
  double transmissionCoefficient;
  double dEnsity;
  VectorDouble dIrection;
  boost::shared_array<kiss_fft_cpx> complexOut;
  int sizeFreq;
  int sizeTime;
  int timeStep;
  VectorDouble cOordinate;
  bool isRadiation;
  bool isRayleigh;

  IncidentWaveDFT() {}
  IncidentWaveDFT(
    double signal_length,
    double signal_duration,
    VectorDouble &d,
    double attenuation,
    double radius,
    double leaky_saw,
    double frequency,
    double velocity,
    double transmission_coefficient,
    double density,
    boost::shared_array<kiss_fft_cpx> complex_out,
    int size_frequency,
    int size_time,
    int time_step,
    bool is_radiation,
    bool is_rayleigh
    ):
    signalLength(signal_length),
    signalDuration(signal_duration),
    dIrection(d),
    aTtenuation(attenuation),
    rAdius(radius),
    complexWaveNumber(leaky_saw),
    fRequency(frequency),
    sPeed(velocity),
    transmissionCoefficient(transmission_coefficient),
    dEnsity(density),
    complexOut(complex_out),
    sizeFreq(size_frequency),
    sizeTime(size_time),
    timeStep(time_step),
    isRadiation(is_radiation),
    isRayleigh(is_rayleigh)
    {}

  ~IncidentWaveDFT() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const complex< double > i( 0.0, 1.0 );
    cOordinate.resize(3);
    cOordinate[0] = x;
    cOordinate[1] = y;
    cOordinate[2] = z;
    complex< double > attenuationR(0,0);
    if(isRayleigh) {

      /* speed of SAW over speed of longitudinal wave */
      complex< double > vs_over_v1 = (signalLength*fRequency) / sPeed;
      attenuationR = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);

      /* attenuationR of Rayleigh inside fluid */
      // double oMega = 2*M_PI*fRequency;
      /* double totalViscosity = (4/3) * 0.001139 + 0.0034; */
      /* sec(Rayleigh angle) = sec(22 degree) = 1.0790 */
      // attenuationR = (0.0049*pow(oMega,2.0)) / (dEnsity*pow(sPeed,3.0));
      // attenuationR *= - 1.0790*abs(y);

    }

    if(aTtenuation != 0) {
      double xOriginal = - std::sqrt(std::abs(std::pow(rAdius,2.0) - std::pow(z,2.0)));
      aTtenuation *= (x - xOriginal);
    }

    //FIXME can apply zero padding to signal length and sIze in this section when deal with large number of time steps.
    complex< double > result = 0.0;
    for(int f = 0;f < sizeTime && sizeFreq;f++) {
      double speed = signalLength/signalDuration;
      /* 2*M_PI/signalLength : 2*M_PI*f/signalLength */
      complex< double > wave_number = (2*M_PI*f/signalLength) + i*complexWaveNumber;
      double delta_t = signalDuration/sizeTime;
      double distance = speed*delta_t*timeStep;
      double phase= 2*M_PI*f*(distance/signalLength);

      // cerr << "\n exp(+ i*wave_number*inner_prod(dIrection,cOordinate)+i*phase) = \n " << exp(+ i*wave_number*inner_prod(dIrection,cOordinate)+i*phase) << endl;

      if(isRadiation) {
        result += (complexOut[f].r+i*complexOut[f].i)*exp(+ i*wave_number*inner_prod(dIrection,cOordinate)+i*phase+wave_number*attenuationR - aTtenuation);
      } else{
        result += (complexOut[f].r+i*complexOut[f].i)*exp(- i*wave_number*inner_prod(dIrection,cOordinate)+i*phase+wave_number*attenuationR - aTtenuation);
      }
    }

    result /= sizeTime;
    result *= transmissionCoefficient;

    // cerr << "\n result = \n " << result << endl;

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};

struct IncidentWavePointSourceDFT: public GenericAnalyticalSolution {

  vector<VectorDouble > rEsult;
  double signalLength;
  double signalDuration;
  double aTtenuation;
  double rAdius;
  double complexWaveNumber;
  double fRequency;
  double sPeed;
  double transmissionCoefficient;
  double dEnsity;
  boost::shared_array<kiss_fft_cpx> complexOut;
  int sizeFreq;
  int sizeTime;
  int timeStep;
  bool isRadiation;
  bool isRayleigh;
  VectorDouble pOsition;
  VectorDouble sourceCoordinate;

  IncidentWavePointSourceDFT() {}
  IncidentWavePointSourceDFT(
    double signal_length,
    double signal_duration,
    VectorDouble &d,
    double attenuation,
    double radius,
    double leaky_saw,
    double frequency,
    double velocity,
    double transmission_coefficient,
    double density,
    boost::shared_array<kiss_fft_cpx> complex_out,
    int size_frequency,
    int size_time,
    int time_step,
    bool is_radiation,
    bool is_rayleigh
    ):
    signalLength(signal_length),
    signalDuration(signal_duration),
    sourceCoordinate(d),
    aTtenuation(attenuation),
    rAdius(radius),
    complexWaveNumber(leaky_saw),
    fRequency(frequency),
    sPeed(velocity),
    transmissionCoefficient(transmission_coefficient),
    dEnsity(density),
    complexOut(complex_out),
    sizeFreq(size_frequency),
    sizeTime(size_time),
    timeStep(time_step),
    isRadiation(is_radiation),
    isRayleigh(is_rayleigh)
    {}

  ~IncidentWavePointSourceDFT() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const complex< double > i( 0.0, 1.0 );
    complex< double > result = 0.0;
    pOsition.resize(3);
    pOsition[0] = (x-sourceCoordinate[0])*(x-sourceCoordinate[0]);
    pOsition[1] = (y-sourceCoordinate[1])*(y-sourceCoordinate[1]);
    pOsition[2] = (z-sourceCoordinate[2])*(z-sourceCoordinate[2]);
    double absPoint = sqrt(pOsition[0]+pOsition[1]+pOsition[2]);
    complex< double > attenuationR(0,0);
    if(isRayleigh) {

      /* speed of SAW over speed of longitudinal wave */
      complex< double > vs_over_v1 = (signalLength*fRequency) / sPeed;
      attenuationR = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);

      /* attenuationR of Rayleigh inside fluid */
      // double oMega = 2*M_PI*fRequency;
      /* double totalViscosity = (4/3) * 0.001139 + 0.0034; */
      /* sec(Rayleigh angle) = sec(22 degree) = 1.0790 */
      // attenuationR = (0.0049*pow(oMega,2.0)) / (dEnsity*pow(sPeed,3.0));
      // attenuationR *= - 1.0790*abs(y);

    }

    if(aTtenuation != 0) {
      double xOriginal = - std::sqrt(std::pow(rAdius,2.0) - std::pow(z,2.0));
      aTtenuation *= (x - xOriginal);
    }

    for(int f = 0;f < sizeTime && sizeFreq;f++) {
      double speed = signalLength/signalDuration;
      complex< double > wave_number = (2*M_PI*f/signalLength) + i*complexWaveNumber;
      double delta_t = signalDuration/sizeTime;
      double distance = speed*delta_t*timeStep;
      double phase= 2*M_PI*f*(distance/signalLength);
      if(isRadiation) {
        result += (-complexOut[f].r-i*complexOut[f].i)*(exp(-i*wave_number*absPoint+i*phase+wave_number*attenuationR - aTtenuation)) / (4*M_PI*absPoint);
      } else{
        result += (complexOut[f].r+i*complexOut[f].i)*(exp(-i*wave_number*absPoint+i*phase+wave_number*attenuationR - aTtenuation)) / (4*M_PI*absPoint);
      }
    }

    result /= sizeTime;
    result *= transmissionCoefficient;

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};

/**

 Transverse Wave of SAW, i.t. is inverse Fourier transform evaluated at arbitrary
 spatial points.

 \f[
 \left. \left\{ \frac{1}{n}  (A_{0} e^{ik \mathbf{d} \cdot \mathbf{x} + i \phi})
 \right\}
 \f]
 where \f$\phi\f$ is
 \f[


 \f]

  */
// struct TransverseWaveDFT: public GenericAnalyticalSolution {
//
//   vector<VectorDouble > rEsult;
//   double signalLength;
//   double signalDuration;
//   double aTtenuation;
//   double complexWaveNumber;
//   double fRequency;
//   double sPeed;
//   VectorDouble dIrection;
//   boost::shared_array<kiss_fft_cpx> complexOut;
//   int sizeFreq;
//   int sizeTime;
//   int timeStep;
//   VectorDouble cOordinate;
//   bool isRadiation;
//   bool isRayleigh;
//   vector<complex<double> > vecAl; ///< this is to calculate constant values of series only once
//
//   double a;
//
//   TransverseWaveDFT() {}
//   TransverseWaveDFT(
//     double signal_length,
//     double signal_duration,
//     VectorDouble &d,
//     double loss_tangent,
//     double leaky_saw,
//     double frequency,
//     double velocity,
//     boost::shared_array<kiss_fft_cpx> complex_out,
//     int size_frequency,
//     int size_time,
//     int time_step,
//     bool is_radiation,
//     bool is_rayleigh,
//     double sphere_radius = 0.5
//     ):
//     signalLength(signal_length),
//     signalDuration(signal_duration),
//     dIrection(d),
//     aTtenuation(loss_tangent),
//     complexWaveNumber(leaky_saw),
//     fRequency(frequency),
//     sPeed(velocity),
//     complexOut(complex_out),
//     sizeFreq(size_frequency),
//     sizeTime(size_time),
//     timeStep(time_step),
//     isRadiation(is_radiation),
//     isRayleigh(is_rayleigh),
//     a(sphere_radius)
//     {}
//
//   ~TransverseWaveDFT() {}
//
//   virtual vector<VectorDouble >& operator()(double x, double y, double z) {
//
//     const complex< double > i( 0.0, 1.0 );
//     cOordinate.resize(3);
//     cOordinate[0] = x;
//     cOordinate[1] = y;
//     cOordinate[2] = z;
//     complex< double > attenuation(0,0);
//     double x2 = x*x,y2 = y*y;
//     double R = sqrt(x2+y2);
//     double theta = atan2(y,x)+2*M_PI;
//
//     if(isRayleigh) {
//       /* speed of SAW over speed of longitudinal wave */
//       complex< double > vs_over_v1 = (signalLength*fRequency) / sPeed;
//       attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
//     }
//
//     //FIXME can apply zero padding to signal length and sIze in this section when deal with large number of time steps.
//     complex< double > result = 0.0;
//     for(int f = 0;f < sizeTime && sizeFreq;f++) {
//
//       double speed = signalLength/signalDuration;
//       /* 2*M_PI/signalLength : 2*M_PI*f/signalLength */
//       double k0 = 2*M_PI*f/signalLength;
//       complex< double > wave_number = (2*M_PI*f/signalLength)*(1.0+i*aTtenuation) + i*complexWaveNumber;
//       double const1 = k0 * a;
//       complex< double > const2 = wave_number * R;
//       complex< double > const3 = wave_number * a;
//       double delta_t = signalDuration/sizeTime;
//       double distance = speed*delta_t*timeStep;
//       double phase= 2*M_PI*f*(distance/signalLength);
//       complex< double > Bm;
//
//       /*  */
//       const double tol = 1.0e-10;
//       complex< double > results = 0.0;
//       complex< double > prev_results;
//       double error = 100.0;
//       unsigned int n = 1; //initialized the infinite series loop
//
//       double Jn_der_zero = ( - cyl_bessel_j( 1, const1 ));
//       complex< double > Jn_der_zero_a = ( - cyl_bessel_j( 1, const3 ));
//       double Jn_zero = cyl_bessel_j( 0, const1 );
//       complex< double > Jn_zero_a = cyl_bessel_j( 0, const3 );
//       complex< double > Jn_zero_r = cyl_bessel_j( 0, const2 );
//       complex< double > Hn_der_zero = ( - cyl_hankel_1( 1, const1 ));
//       complex< double >Hn_zero = cyl_hankel_1( 0, const1 );  //S Hankel first kind function
//       complex< double > Bm0 = ( (complexOut[f].r+i*complexOut[f].i) * k0 * (Hn_zero*Jn_der_zero - Hn_der_zero*Jn_zero) ) /
//       ( wave_number*Hn_zero*Jn_der_zero_a - k0*Hn_der_zero*Jn_zero_a );
//       //n=0;
//       results += Bm0*Jn_zero_r;
//
//       while( error > tol )  //finding the acoustic potential in one single point.
//       {
//         if(vecAl.size()>n) {
//           Bm = vecAl[n-1];
//         } else {
//           // cylindrical Bessel function
//           double Jn_ka = cyl_bessel_j( n, const1 );
//           complex< double > Jn_wave_number_a = cyl_bessel_j( n, const3 );
//           double Jn_der_ka = n / const1 * cyl_bessel_j( n, const1 ) - cyl_bessel_j( n + 1, const1 );
//           complex< double > Jn_der_waver_number_a = double(n) / const3 * cyl_bessel_j( n, const3 ) - cyl_bessel_j( n + 1, const3 );
//           complex< double > Hn_ka = cyl_hankel_1( n, const1 );  //S Hankel first kind function
//           complex< double > Hn_der_ka = n / const1 * cyl_hankel_1( n, const1 ) - cyl_hankel_1( n + 1, const1 );
//
//           //Constant term
//           Bm = ( (complexOut[f].r+i*complexOut[f].i) * 2.0*pow(i,n) * k0 * (Hn_ka*Jn_der_ka - Hn_der_ka*Jn_ka) ) /
//           ( wave_number*Hn_ka*Jn_der_waver_number_a - k0*Hn_der_ka*Jn_wave_number_a );;
//           vecAl.push_back(Bm);
//         }
//
//         complex< double > Jn_wave_number_r = cyl_bessel_j( n, const2 );
//         prev_results = results;
//
//         results += Bm * Jn_wave_number_r * cos(n*theta) * (exp(i*phase+wave_number));
//         error = abs( abs( results ) - abs( prev_results ) );
//         ++n;
//
//       }
//
//
//
//       // if(isRadiation) {
//       //   result += (-complexOut[f].r-i*complexOut[f].i)*exp(i*wave_number*inner_prod(dIrection,cOordinate)+i*phase+wave_number*attenuation);
//       // } else{
//       //   result += (complexOut[f].r+i*complexOut[f].i)*exp(i*wave_number*inner_prod(dIrection,cOordinate)+i*phase+wave_number*attenuation);
//       // }
//
//     }
//
//     result /= sizeTime;
//
//
//     rEsult.resize(2);
//     rEsult[REAL].resize(1);
//     (rEsult[REAL])[0] = std::real(result);
//     rEsult[IMAG].resize(1);
//     (rEsult[IMAG])[0] = std::imag(result);
//
//     return rEsult;
//
//   }
//
// };


#endif // KISS_FFT_H

/** Calculate the analytical solution of impinging wave on sphere
  \ingroup mofem_helmholtz_elem


  equation from:
  Ihlenburg,Finite element analysis of acoustic scattering Springer Science & Business Media.

  \f[
  p_\textrm{scattered} = \sum_0^N A_l h_l(kr)P_l(\cos(\phi))
  \f]

  where \f$h_l\f$ is the Hankel function of the first kind, \f$\phi\f$ is polar
  angle and \f$A_l\f$ is a constant. Constant is  should be calculated such that
  it satisfies both the Helmholtz wave equation and the Sommerfeld radiation
  condition.

  \f[
  A_l = -(2l+1)i^l \frac{j_{l}'(ka)}{h_{l}'(ka)}
  \f]
  where a is scatter sphere radius and \f$j_l\f$ Spherical Bessel function.

  */
struct HardSphereScatterWave: public GenericAnalyticalSolution {

    vector<complex<double> > vecAl; ///< this is to calculate constant values of series only once
    vector<VectorDouble > rEsult;
    double wAvenumber;
    double sphereRadius;


    HardSphereScatterWave(double wavenumber,double sphere_radius = 0.5):

    wAvenumber(wavenumber),sphereRadius(sphere_radius) {}
    virtual ~HardSphereScatterWave() {}

    virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const double tol = 1.0e-10;

    double x2 = x*x;
    double y2 = y*y;
    double z2 = z*z;

    double R = sqrt(x2+y2+z2);
    // double cos_theta = z/R; //Incident wave in Z direction, X =>sin_theta*sin_phi, Y =>sin_theta*cos_phi
    double cos_theta = x/R; //Incident wave in X direction.


    const double k = wAvenumber;    //Wave number
    const double a = sphereRadius;      //radius of the sphere,wait to modify by user

    const complex< double > i( 0.0, 1.0 );
    complex< double > Al;

    complex< double > result = 0.0;
    complex< double > prev_result;

    double error = 100.0;
    unsigned int n = 0; //initialized the infinite series loop

    while( error > tol )  //finding the acoustic potential in one single point.
    {

      if(vecAl.size()>n) {
        Al = vecAl[n];
      } else {
        // spherical Bessel function
        double const1 = k*a;
        double jn_der = n / const1 * sph_bessel( n, const1 ) - sph_bessel( n + 1, const1 );

        // spherical Hankel function
        complex< double > hn_der = n / const1 * sph_hankel_1( n, const1 ) - sph_hankel_1( n + 1, const1 );
        //Constant term
        Al = -(2.0*n+1)*pow(i,n)*jn_der/hn_der;
        vecAl.push_back(Al);
      }

      prev_result = result;

      // Legendre function
      double Pn = legendre_p(n,cos_theta);
      result += Al*sph_hankel_1(n,k*R)*Pn;

      error = abs( abs( result ) - abs( prev_result ) );

      ++n;

    }

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};

/** Calculate the analytical solution of impinging wave on sphere
  \ingroup mofem_helmholtz_elem


  Equations from:
  <http://ansol.us/Products/Coustyx/Validation/MultiDomain/Scattering/PlaneWave/SoftSphere/Downloads/dataset_description.pdf>

  \f[
  p_\textrm{scattered} = \sum_0^N A_l h_l(kr)P_l(\cos(\phi))
  \f]

  where \f$h_l\f$ is the Hankel function of the first kind, \f$\phi\f$ is polar
  angle and \f$A_l\f$ is a constant. Constant is  should be caculated such that
  it satisfies both the Helmholtz wave equation and the Sommerfeld radiation
  condition.

  \f[
  A_l = -(2l+1)i^l \frac{j_l(ka)}{h_l(ka)}
  \f]

  where a is scatter sphere radius and \f$j_l\f$ Spherical Bessel function.


  */
struct SoftSphereScatterWave: public GenericAnalyticalSolution {

  vector<complex<double> > vecAl; ///< this is to calculate constant values of series only once
  vector<VectorDouble > rEsult;
  double wAvenumber;
  double sphereRadius;


  SoftSphereScatterWave(double wavenumber,double sphere_radius = 0.5):

    wAvenumber(wavenumber),sphereRadius(sphere_radius) {}
  virtual ~SoftSphereScatterWave() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const double tol = 1.0e-10;

    double x2 = x*x;
    double y2 = y*y;
    double z2 = z*z;

    double R = sqrt(x2+y2+z2);
    // double cos_theta = z/R; //incident wave in Z direction
    double cos_theta = x/R; //incident wave in X direction

    const double k = wAvenumber;    //Wave number
    const double a = sphereRadius;      //radius of the sphere,wait to modify by user

    const complex< double > i( 0.0, 1.0 );
    complex< double > Al;

    complex< double > result = 0.0;
    complex< double > prev_result;

    double error = 100.0;
    unsigned int n = 0; //initialized the infinite series loop

    while( error > tol )  //finding the acoustic potential in one single point.
    {

      if(vecAl.size()>n) {
	Al = vecAl[n];
      } else {
	// spherical Bessel function
	double jn = sph_bessel(n,k*a);
	// spherical Hankel function
	complex<double> hn = sph_hankel_1(n,k*a);
	//Constant term
	Al = -(2.0*n+1)*pow(i,n)*jn/hn;
	vecAl.push_back(Al);
      }

      prev_result = result;

      // Legendre function
      double Pn = legendre_p(n,cos_theta);
      result += Al*sph_hankel_1(n,k*R)*Pn;

      error = abs( abs( result ) - abs( prev_result ) );

      ++n;

    }

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};

/** \brief Calculate the analytical solution of plane wave guide propagating in direction theta
  \ingroup mofem_helmholtz_elem

  \f[
  p_\textrm{scattered} = exp^{ik\mathbf{x}\Theta}
  \f]

  where:

  \f[
  \mathbf{x} = [x,y]
  \f]

  \f[
  \Theta = k[\cos(\theta),\sin(\theta)]
  \f]

  \theta is the wave propagating direction from range [0,2\pi]


   Paper: Gillman, A., Djellouli, R., & Amara, M. (2007).
   A mixed hybrid formulation based on oscillated finite element polynomials for solving Helmholtz problems.
   Journal of computational and applied mathematics

*/
struct PlaneWave: public GenericAnalyticalSolution {

  vector<VectorDouble > rEsult;
  double wAvenumber;
  double tHeta;

  PlaneWave(double wavenumber,double theta):
    wAvenumber(wavenumber),tHeta(theta) {}
  virtual ~PlaneWave() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const double k = wAvenumber;  //Wave number

    const complex< double > i( 0.0, 1.0 );
    complex< double > result = 0.0;


    //const complex< double > inc_field = exp( i * k * R * cos( theta ) );
    //const complex< double > total_field = inc_field + result;

    result = exp(i*(k*cos(tHeta)*x+k*sin(tHeta)*y));

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};

/** \brief Calculate the analytical solution of impinging wave on cylinder
  \ingroup mofem_helmholtz_elem

   \f[
  p_\textrm{scattered} = \sum_0^N A_l H_l(kr)\cos(l\theta)
  \f]

  where \f$H_l\f$ is the cylindrical Hankel function of the first kind, \f$\theta\f$
  is the polar angle in polar coordinate and \f$A_l\f$ is a constant. Constant is
  should be caculated such that it satisfies both the Helmholtz wave equation and the
  Sommerfeld radiation condition.

  \f[
  A_l = -\epsilon_{l} i^l \frac{J_{l}'(ka)}{H_{l}'(ka)}
  \f]

  where a is scatter sphere radius and \f$J_l\f$ Cylindrical Bessel function.

  \f[
  \epsilon_{l} = 1 \textrm{when}l=0
  \f]

   \f[
  \epsilon_{l} = 2 \textrm{when}l \neq 0
  \f]

  Paper:
    Kechroud, R., Soulaimani, A., & Antoine, X. (2009).
    A performance study of plane wave finite element methods with a Padé-type artificial boundary condition in acoustic scattering.
    Advances in Engineering Software, 40(8), 738-750.

*/
struct HardCylinderScatterWave: public GenericAnalyticalSolution {

  vector<complex<double> > vecAl; ///< this is to calculate constant values of series only once
  vector<VectorDouble > rEsult;
  double wAvenumber;
  //double shereRadius;
  double a;

  HardCylinderScatterWave(double wavenumber,double sphere_radius = 0.5): wAvenumber(wavenumber),a(sphere_radius) {}
  virtual ~HardCylinderScatterWave() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const double tol = 1.0e-10;
    double x2 = x*x,y2 = y*y;
    double R = sqrt(x2+y2);
    double theta = atan2(y,x)+2*M_PI;

    const double k = wAvenumber;  //Wave number
    const double const1 = k * a;
    double const2 = k * R;

    const complex< double > i( 0.0, 1.0 );
    complex< double > Al;
    // magnitude of incident wave
    //const double phi_incident_mag = 1.0;

    complex< double > result = 0.0;
    complex< double > prev_result;

    double error = 100.0;
    unsigned int n = 1; //initialized the infinite series loop

    double Jn_der_zero = ( - cyl_bessel_j( 1, const1 ));
    complex< double > Hn_der_zero = ( - cyl_hankel_1( 1, const1 ));
    complex< double >Hn_zero = cyl_hankel_1( 0, const2 );  //S Hankel first kind function

    //n=0;
    result -= (Jn_der_zero * Hn_zero)/Hn_der_zero;

    while( error > tol )  //finding the acoustic potential in one single point.
    {
      if(vecAl.size()>n) {
        Al = vecAl[n-1];
      } else {
        // cylindrical Bessel function
        double Jn_der_ka = n / const1 * cyl_bessel_j( n, const1 ) - cyl_bessel_j( n + 1, const1 );
        // cylindrical Hankel function
        complex<double> Hn_der_ka = n / const1 * cyl_hankel_1( n, const1 ) - cyl_hankel_1( n + 1, const1 );
        //Constant term
        Al = -2.0*pow(i,n)*Jn_der_ka/Hn_der_ka;
        vecAl.push_back(Al);
      }

      prev_result = result;
      complex< double >Hn_kr = cyl_hankel_1( n, const2 );  //S Hankel first kind function

      result += Al * Hn_kr * cos(n*theta);
      error = abs( abs( result ) - abs( prev_result ) );
      ++n;

    }

    //result *= phi_incident_mag;

    //const complex< double > inc_field = exp( i * k * R * cos( theta ) );  //
    //const complex< double > total_field = inc_field + result;
    //ofs << theta << "\t" << abs( result ) << "\t" << abs( inc_field ) << "\t" << abs( total_field ) <<  "\t" << R << endl; //write the file

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};


/** \brief Calculate the analytical solution of impinging wave on cylinder
  \ingroup mofem_helmholtz_elem

   \f[
  p_\textrm{scattered} = \sum_0^N A_l H_l(kr)\cos(l\theta)
  \f]

  where \f$H_l\f$ is the cylindrical Hankel function of the first kind, \f$\theta\f$
  is the polar angle in polar coordinate and \f$A_l\f$ is a constant. Constant is
  should be caculated such that it satisfies both the Helmholtz wave equation and the
  Sommerfeld radiation condition.

  \f[
  A_l = -\epsilon_{l} i^l \frac{J_{l}(ka)}{H_{l}(ka)}
  \f]

  where a is scatter sphere radius and \f$J_l\f$ Cylindrical Bessel function.

  \f[
  \epsilon_{l} = 1 \textrm{when}l=0
  \f]

   \f[
  \epsilon_{l} = 2 \textrm{when}l \neq 0
  \f]

  Paper:
    Kechroud, R., Soulaimani, A., & Antoine, X. (2009).
    A performance study of plane wave finite element methods with a Padé-type artificial boundary condition in acoustic scattering.
    Advances in Engineering Software, 40(8), 738-750.


*/
struct SoftCylinderScatterWave: public GenericAnalyticalSolution {

  vector<complex<double> > vecAl; ///< this is to calculate constant values of series only once
  vector<VectorDouble > rEsult;
  double wAvenumber;
  //double shereRadius;
  double a;

  SoftCylinderScatterWave(double wavenumber,double sphere_radius = 0.5): wAvenumber(wavenumber),a(sphere_radius) {}
  virtual ~SoftCylinderScatterWave() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const double tol = 1.0e-10;
    double x2 = x*x,y2 = y*y;
    double R = sqrt(x2+y2);
    double theta = atan2(y,x)+2*M_PI;

    const double k = wAvenumber;  //Wave number
    const double const1 = k * a;
    double const2 = k * R;

    const complex< double > i( 0.0, 1.0 );
    complex< double > Al;
    // magnitude of incident wave
    //const double phi_incident_mag = 1.0;

    complex< double > result = 0.0;
    complex< double > prev_result;

    double error = 100.0;
    unsigned int n = 1; //initialized the infinite series loop

    double Jn_zero = cyl_bessel_j( 0, const1 );
    complex< double > Hn_zero_kr = cyl_hankel_1( 0, const2 );  //S Hankel first kind function
    complex< double > Hn_zero_ka = cyl_hankel_1( 0, const1 );  //S Hankel first kind function
    //n=0;
    result -= (Jn_zero * Hn_zero_kr)/Hn_zero_ka;

    while( error > tol )  //finding the acoustic potential in one single point.
    {


      if(vecAl.size()>n) {
        Al = vecAl[n-1];
      } else {
        // cylindrical Bessel function
        double Jn_ka = cyl_bessel_j( n, const1 );
        // cylindrical Hankel function
        complex<double> Hn_ka = cyl_hankel_1( n, const1 );
        //Constant term
        Al = -2.0*pow(i,n)*Jn_ka/Hn_ka;
        vecAl.push_back(Al);
      }


      prev_result = result;

      complex< double >Hn_kr = cyl_hankel_1( n, const2 );  //S Hankel first kind function

      result += Al * Hn_kr * cos(n*theta);
      error = abs( abs( result ) - abs( prev_result ) );
      ++n;
    }

    //result *= phi_incident_mag;

    //const complex< double > inc_field = exp( i * k * R * cos( theta ) );
    //const complex< double > total_field = inc_field + result;
    //ofs << theta << "\t" << abs( result ) << "\t" << abs( inc_field ) << "\t" << abs( total_field ) <<  "\t" << R << endl; //write the file

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};



/** Calculate the analytical solution of impinging wave on L shaped domain with singularity
  \ingroup mofem_helmholtz_elem


  Equations from:
  <The p and h-p Versions of the Finite Element Method, Basic Principles and Properties Read More: http://epubs.siam.org/doi/abs/10.1137/1036141>

  \f[
  p_\textrm{scattered} = j_{\frac{2}{3}}(kr)\sin(\frac{2}{3}\phi)
  \f]
  for 0 < \f$\phi\f$ < \f$\frac{2}{3}\pi\f$
  where r is scatter sphere radius and \f$j_l\f$ Spherical Bessel function,
  \f$\phi\f$ is polar angle

  Constant should be caculated such that
  it satisfies both the Helmholtz wave equation and the Robin boundary condition.




  */
struct SingularScatterWave: public GenericAnalyticalSolution {


  vector<VectorDouble > rEsult;
  double wAvenumber;


  SingularScatterWave(double wavenumber):

    wAvenumber(wavenumber) {}
  virtual ~SingularScatterWave() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const double tol = 1.0e-10;

    double x2 = x*x,y2 = y*y;

    double R = sqrt(x2+y2);
    double theta = atan2(y,x)+2*M_PI;
    // double cos_theta = z/R; //incident wave in Z direction
    // double cos_theta = x/R; //incident wave in X direction
    // double sin_theta = y/R;

    const double k = wAvenumber;    //Wave number

    double const1 = k * R;
    double const2 = sin(2*theta/3);
    // double const2 = sin_theta;
    double Jn_two_third = cyl_bessel_j( 0.666666666666667, const1 );

    complex< double > result = 0.0;
    result = Jn_two_third * const2;

    // result = exp(theta);

    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};

/** Calculate the incident wave of impinging wave on L shaped domain with singularity
  \ingroup mofem_helmholtz_elem


  Equations from:
  <The p and h-p Versions of the Finite Element Method, Basic Principles and Properties Read More: http://epubs.siam.org/doi/abs/10.1137/1036141>

  \f[
  p_\textrm{scattered} = j_{\frac{2}{3}}(kr)\sin(\frac{2}{3}\phi)
  \f]
  for 0 < \f$\phi\f$ < \f$\frac{2}{3}\pi\f$
  where r is scatter sphere radius and \f$j_l\f$ Spherical Bessel function,
  \f$\phi\f$ is polar angle

  Constant should be caculated such that
  it satisfies both the Helmholtz wave equation and the Robin boundary condition.




  */
struct SingularIncidentWave: public GenericAnalyticalSolution {


  vector<VectorDouble > rEsult;
  double wAvenumber;


  SingularIncidentWave(double wavenumber):

    wAvenumber(wavenumber) {}
  virtual ~SingularIncidentWave() {}

  virtual vector<VectorDouble >& operator()(double x, double y, double z) {

    const double tol = 1.0e-10;

    double x2 = x*x,y2 = y*y;

    double R = sqrt(x2+y2);
    double theta = atan2(y,x)+2*M_PI;
    // double cos_theta = z/R; //incident wave in Z direction
    // double cos_theta = x/R; //incident wave in X direction
    // double sin_theta = y/R;

    const double k = wAvenumber;    //Wave number

    // double const1 = k * R;
    // double const2 = sin(2*theta/3);
    // // double const2 = sin_theta;
    //
    // double Jn_two_third = cyl_bessel_j( 0.66667, const1 );
    // double D_Jn_two_third = (0.66667/const1) * Jn_two_third - cyl_bessel_j( 1.66667, const1 )
    //
    complex< double > result = 0.0;
    // result = D_Jn_two_third * const2;

    // result = exp(theta);

    // double const1 = pow(R,-0.5);
    // double const2 = sinh(R) * cos(theta/2)
    // result =


    rEsult.resize(2);
    rEsult[REAL].resize(1);
    (rEsult[REAL])[0] = std::real(result);
    rEsult[IMAG].resize(1);
    (rEsult[IMAG])[0] = std::imag(result);

    return rEsult;

  }

};




// **** Surface boundary conditions ( used to enforce BC on surface ) ****

// ** Dirichlet BC **

// **** Function to solve best approximation problem ****

template <typename FUNVAL>
PetscErrorCode calculate_matrix_and_vector(
  MoFEM::Interface& m_field,const string &problem_name,const string &fe_name,const string &re_field,
  Mat A,vector<Vec> vec_F,FUNVAL &fun_evaluator
) {
  PetscFunctionBegin;

  PetscErrorCode ierr;
  FieldApproximationH1 field_approximation(m_field);
  // This increase rule for numerical integration. In case of 10 node
  // elements jacobian is varying linearly across element, that way to element
  // rule is add 1.
  // field_approximation.addToRule = 1;
  // field_approximation.feVolume = HelmholtzElement::MyVolumeFE(m_field,true,1);

  ierr = field_approximation.loopMatrixAndVectorVolume(
    problem_name,fe_name,re_field,A,vec_F,fun_evaluator
  ); CHKERRQ(ierr);


  if(A) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode save_data_on_mesh(
  MoFEM::Interface& m_field,const string &problem_name,const string& re_field,const string &im_field,
  Vec D,int vt,InsertMode mode,PetscBool is_partitioned
) {
  PetscFunctionBegin;

  PetscErrorCode ierr;
  // save data on mesh
  if(vt == GenericAnalyticalSolution::REAL) {
    /* set data to field from solution vec */
    if(is_partitioned) {
      ierr = m_field.set_local_ghost_vector(problem_name,COL,D,mode,SCATTER_REVERSE); CHKERRQ(ierr);
    } else {
      ierr = m_field.set_global_ghost_vector(problem_name,COL,D,mode,SCATTER_REVERSE); CHKERRQ(ierr);
    }

  } else {

    if(is_partitioned) {
      ierr = m_field.set_other_local_ghost_vector(problem_name,re_field,im_field,COL,D,mode,SCATTER_REVERSE); CHKERRQ(ierr);
    } else {
      ierr = m_field.set_other_global_ghost_vector(problem_name,re_field,im_field,COL,D,mode,SCATTER_REVERSE); CHKERRQ(ierr);
    }

  }

  PetscFunctionReturn(0);
}

template <typename FUNEVAL>
PetscErrorCode solve_problem(MoFEM::Interface& m_field,
  const string& problem_name,const string& fe_name,
  const string& re_field,const string &im_field,
  InsertMode mode,
  FUNEVAL &fun_evaluator,PetscBool is_partitioned) {
  PetscFunctionBegin;

  PetscErrorCode ierr;

  Mat A;
  ierr = m_field.MatCreateMPIAIJWithArrays(problem_name,&A); CHKERRQ(ierr);
  ierr = MatZeroEntries(A); CHKERRQ(ierr);
  Vec D;

  vector<Vec> vec_F;
  vec_F.resize(2);

  ierr = m_field.VecCreateGhost(problem_name,ROW,&vec_F[0]); CHKERRQ(ierr); /* real */
  ierr = m_field.VecCreateGhost(problem_name,ROW,&vec_F[1]); CHKERRQ(ierr); /* imag */
  ierr = m_field.VecCreateGhost(problem_name,COL,&D); CHKERRQ(ierr);

  for(int ss = 0;ss<2;ss++) {
    ierr = VecZeroEntries(vec_F[ss]); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(vec_F[ss],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(vec_F[ss],INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
  }

  ierr = calculate_matrix_and_vector(m_field,problem_name,fe_name,re_field,A,vec_F,fun_evaluator); CHKERRQ(ierr);

  KSP solver;
  ierr = KSPCreate(PETSC_COMM_WORLD,&solver); CHKERRQ(ierr);
  ierr = KSPSetOperators(solver,A,A); CHKERRQ(ierr);
  ierr = KSPSetFromOptions(solver); CHKERRQ(ierr);
  ierr = KSPSetUp(solver); CHKERRQ(ierr);

  for(int ss = 0;ss<GenericAnalyticalSolution::LAST_VAL_TYPE;ss++) {
    // solve problem
    ierr = KSPSolve(solver,vec_F[ss],D); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);


    ierr = save_data_on_mesh(m_field,problem_name,re_field,im_field,D,ss,mode,is_partitioned); CHKERRQ(ierr);

  }

  // clean
  ierr = KSPDestroy(&solver); CHKERRQ(ierr);
  ierr = VecDestroy(&vec_F[GenericAnalyticalSolution::REAL]); CHKERRQ(ierr);
  ierr = VecDestroy(&vec_F[GenericAnalyticalSolution::IMAG]); CHKERRQ(ierr);

  ierr = VecDestroy(&D); CHKERRQ(ierr);
  ierr = MatDestroy(&A); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
