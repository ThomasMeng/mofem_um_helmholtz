/** \file HelmholtzElement.hpp
 \ingroup mofem_helmholtz_elem

 \brief Operators and data structures for wave propagation analyze (Galerkin Element)

 Implementation of Helmholtz element for wave propagation problem

 Note:
 This work is part of PhD thesis by on Micro-fluids: Thomas Felix Xuan Meng

 */

/*
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
 * The header file should contains as least #include as possible for speed
 */

#ifndef __HELMHOLTZ_ELEMENT_HPP
#define __HELMHOLTZ_ELEMENT_HPP

#include <quad.h>

/** \brief Structure grouping operators and data used for wave propagation problem
  \ingroup mofem_helmholtz_elem

  Complex-real transformation, See Ercegovac, Milos, and Jean-Michel Muller.
  "Solving Systems of Linear Equations in Complex Domain: Complex E-Method."
  (2007)"

  Three dimensional homogeneous isotropic medium and time harmonic Helmholtz operator
*/
struct HelmholtzElement {


  /// \brief  Volume element
  struct MyVolumeFE: public VolumeElementForcesAndSourcesCore {
    int addToRank; ///< default value 1, i.e. assumes that geometry is approx. by quadratic functions.
    int previousOrder;
    bool isDuffy;
    int oRder;
    double qUadrature;

    MyVolumeFE(MoFEM::Interface &m_field,bool is_duffy,int order,double quadrature,int add_to_rank):
    VolumeElementForcesAndSourcesCore(m_field),
    isDuffy(is_duffy),
    oRder(order),
    qUadrature(quadrature),
    addToRank(add_to_rank),
    previousOrder(1) {}

    int getRule(int order) {
      if(isDuffy) {
        return -1;
      } else {
        return 2*order;
      }
    };

    VectorInt getRuleDuffy(int order) {
      VectorInt vduffyOrder;
      vduffyOrder.resize(2);
      /*Due to difference in exponent order,
        higher order quadrature are selected
        for w direction. The direction with
        highest exponent is sufficient to
        evaluated with half of its exponent order */

      // order =  6;
      vduffyOrder[0] = 2*order + 1;

      vduffyOrder[1] = 2*order + 1;

      return vduffyOrder;
    };


    PetscErrorCode setGeneralizedDuffyGaussPts(int order,MatrixDouble &gaussDuffy) {
      PetscFunctionBegin;

      MatrixDouble g_u;
      MatrixDouble g_w;
      int nb_gauss_pts_u;
      int nb_gauss_pts_w;
      double detJocabian = 0;

      /* 1. get the coordinates of four vertices of tetrahedron */

      /* 2. take affine transform from physical tet to reference tet */

      /* 3. take the order to retrieve the gaussian points and weights on
      unit cube, and loop over each faces */
      /* retrieve abscissa gaussian points. */
      VectorInt vduffyOrder = getRuleDuffy(order);
      if(vduffyOrder[1]<=QUAD_1D_TABLE_SIZE) {
        //FIXME add more guass points to 21 on edge instead of 11.
        nb_gauss_pts_u = QUAD_1D_TABLE[vduffyOrder[0]]->npoints;
        nb_gauss_pts_w = QUAD_1D_TABLE[vduffyOrder[1]]->npoints;
      } else {
        SETERRQ2(
          PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"selected rule order > quadrature order %d < %d",
          vduffyOrder[1],QUAD_1D_TABLE_SIZE
        );
        vduffyOrder[0] = vduffyOrder[1] = 21;
        nb_gauss_pts_u = nb_gauss_pts_w = 11;
      }
      g_u.resize(2,nb_gauss_pts_u);
      g_w.resize(2,nb_gauss_pts_w);
      g_u.clear();
      g_w.clear();

      cblas_dcopy(
        nb_gauss_pts_u,&QUAD_1D_TABLE[vduffyOrder[0]]->points[1],2,&g_u(0,0),1
      );
      cblas_dcopy(
        nb_gauss_pts_u,QUAD_1D_TABLE[vduffyOrder[0]]->weights,1,&g_u(1,0),1
      );
      cblas_dcopy(
        nb_gauss_pts_w,&QUAD_1D_TABLE[vduffyOrder[1]]->points[1],2,&g_w(0,0),1
      );
      cblas_dcopy(
        nb_gauss_pts_w,QUAD_1D_TABLE[vduffyOrder[1]]->weights,1,&g_w(1,0),1
      );

      // ublas::matrix_row<MatrixDouble > gp_u (g_u, 0);

      /* contain x,y,z and weights */
      /* 4. apply Duffy transform on gaussian points and weights wi wj wq X jacobian. */

      gaussDuffy.resize(4,nb_gauss_pts_u*nb_gauss_pts_u*nb_gauss_pts_w);
      gaussDuffy.clear();

      int ind = 0;

      /* Duffy transformation 1 (slow) */
      for(unsigned int gg1 = 0;gg1<nb_gauss_pts_u;gg1++) {
        for(unsigned int gg2 = 0;gg2<nb_gauss_pts_u;gg2++) {
          for(unsigned int gg3 = 0;gg3<nb_gauss_pts_w;gg3++) {

            gaussDuffy(0,ind) = g_u(0,gg1);
            gaussDuffy(1,ind) = (1-g_u(0,gg1))*g_u(0,gg2);
            gaussDuffy(2,ind) = (1-g_u(0,gg1))*(1-g_u(0,gg2))*g_w(0,gg3);
            detJocabian = pow((1-g_u(0,gg1)),2.0) * (1-g_u(0,gg2));
            gaussDuffy(3,ind) = 6*g_u(1,gg1)*g_u(1,gg2)*g_w(1,gg3)*detJocabian;

            ind += 1;

          }
        }
      }


      /* Duffy transformation 2 (fast) */
      // for(unsigned int gg1 = 0;gg1<nb_gauss_pts_u;gg1++) {
      //   for(unsigned int gg2 = 0;gg2<nb_gauss_pts_u;gg2++) {
      //     for(unsigned int gg3 = 0;gg3<nb_gauss_pts_w;gg3++) {
      //
      //       // int ind = (gg1)*nb_gauss_pts_u+(gg2)*nb_gauss_pts_u+gg3;
      //       gaussDuffy(0,ind) = g_u(0,gg1)*g_u(0,gg2)*g_w(0,gg3);
      //       gaussDuffy(1,ind) = g_u(0,gg1)*g_u(0,gg2)*(1-g_w(0,gg3));
      //       gaussDuffy(2,ind) = g_u(0,gg1)*(1-g_u(0,gg2));
      //       detJocabian = pow(g_u(0,gg1),2.0) * g_u(0,gg2);
      //       gaussDuffy(3,ind) = 6*g_u(1,gg1)*g_u(1,gg2)*g_w(1,gg3)*detJocabian;
      //       ind += 1;
      //
      //     }
      //   }
      // }


      /* Generalized Duffy transformation */
      // double beta = 2.0;
      // ublas::matrix_row<MatrixDouble > gp_u (g_u, 0);
      // // std::transform(gp_u.begin(), gp_u.end(), gp_u.begin(), (double(*)(double)) pow);
      // for(unsigned int gg1 = 0;gg1<nb_gauss_pts_u;gg1++) {
      //   gp_u(gg1) = pow(gp_u(gg1),beta);
      // }
      //
      // gaussDuffy.resize(4,nb_gauss_pts_u*nb_gauss_pts_u*nb_gauss_pts_w);
      // for(unsigned int gg1 = 0;gg1<nb_gauss_pts_u;gg1++) {
      //   for(unsigned int gg2 = 0;gg2<nb_gauss_pts_u;gg2++) {
      //     for(unsigned int gg3 = 0;gg3<nb_gauss_pts_w;gg3++) {
      //
      //       gaussDuffy(0,ind) = gp_u(gg1)*g_u(0,gg2)*g_w(0,gg3);
      //       gaussDuffy(1,ind) = gp_u(gg1)*g_u(0,gg2)*(1-g_w(0,gg3));
      //       gaussDuffy(2,ind) = gp_u(gg1)*(1-g_u(0,gg2));
      //
      //       detJocabian = beta * pow(gp_u(gg1),3.0*beta - 1.0) * g_u(0,gg2);
      //       gaussDuffy(3,ind) = 16.548*g_u(1,gg1)*g_u(1,gg2)*g_w(1,gg3)*detJocabian;
      //       ind += 1;
      //
      //     }
      //   }
      // }

      /* 5. take the inverse affine transformation on transformed Gaussian points and weights, */
      PetscFunctionReturn(0);
    }

    /// !\brief It will be removed in the future use other variant
    PetscErrorCode setGaussPts(int order) {
      PetscErrorCode ierr;
      PetscFunctionBegin;
      /* FIXME order is not retrieve correctly from field seted */

      if(order != previousOrder) {
        ierr = setGeneralizedDuffyGaussPts(order,gaussPts); CHKERRQ(ierr);
        previousOrder = order;
        // cerr <<"\n occur only once \n" << endl;
      } else {
        /* Guassian quadrature rule remains samilar order */
      }
      PetscFunctionReturn(0);
    }


  };

  /// \brief Surface element
  struct MySurfaceFE: public FaceElementForcesAndSourcesCore {
    int addToRank; ///< default value 1, i.e. assumes that geometry is approx. by quadratic functions.
    bool isDuffy;
    int previousOrder;
    int oRder;
    double qUadrature;

    MySurfaceFE(MoFEM::Interface &m_field,bool is_duffy,int order,double quadrature,int add_to_rank):
    FaceElementForcesAndSourcesCore(m_field),
    isDuffy(is_duffy),
    oRder(order),
    qUadrature(quadrature),
    addToRank(add_to_rank),
    previousOrder(1) {}

    int getRule(int order) {
      if(isDuffy) {
        return -1;
      } else {
        return 2*order+addToRank;
      }
    };

    VectorInt getRuleDuffy(int order) {
      VectorInt vduffyOrder;
      vduffyOrder.resize(2);
      /*Due to difference in exponent order,
        higher order quadrature are selected
        for w direction. The direction with
        highest exponent is sufficient to
        evaluated with half of its exponent order */

      // order =  6;
      vduffyOrder[0] = 2*order + 1;

      vduffyOrder[1] = 2*order + 1;

      return vduffyOrder;
    };


    PetscErrorCode setGeneralizedDuffyGaussPts(const int order,MatrixDouble &gaussDuffy) {
      PetscFunctionBegin;

      MatrixDouble g_u;
      MatrixDouble g_v;
      int nb_gauss_pts_u;
      int nb_gauss_pts_v;
      double detJocabian = 0;


      /* retrieve abscissa gaussian points. */
      VectorInt vduffyOrder = getRuleDuffy(order);
      if(vduffyOrder[1]<=QUAD_1D_TABLE_SIZE) {
        //FIXME add more guass points to 21 on edge instead of 11.
        nb_gauss_pts_u = QUAD_1D_TABLE[vduffyOrder[0]]->npoints;
        nb_gauss_pts_v = QUAD_1D_TABLE[vduffyOrder[1]]->npoints;
      } else {
        SETERRQ2(
          PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"selected rule order > quadrature order %d < %d",
          vduffyOrder[1],QUAD_1D_TABLE_SIZE
        );
        vduffyOrder[0] = vduffyOrder[1] = 21;
        nb_gauss_pts_u = nb_gauss_pts_v = 11;
      }
      g_u.resize(2,nb_gauss_pts_u);
      g_v.resize(2,nb_gauss_pts_v);
      g_u.clear();
      g_v.clear();

      cblas_dcopy(
        nb_gauss_pts_u,&QUAD_1D_TABLE[vduffyOrder[0]]->points[1],2,&g_u(0,0),1
      );
      cblas_dcopy(
        nb_gauss_pts_u,QUAD_1D_TABLE[vduffyOrder[0]]->weights,1,&g_u(1,0),1
      );
      cblas_dcopy(
        nb_gauss_pts_v,&QUAD_1D_TABLE[vduffyOrder[1]]->points[1],2,&g_v(0,0),1
      );
      cblas_dcopy(
        nb_gauss_pts_v,QUAD_1D_TABLE[vduffyOrder[1]]->weights,1,&g_v(1,0),1
      );


      gaussDuffy.resize(3,nb_gauss_pts_u*nb_gauss_pts_v);
      gaussDuffy.clear();

      int ind = 0;

      /* Duffy transformation 1 (slow) */
      // for(unsigned int gg1 = 0;gg1<nb_gauss_pts_u;gg1++) {
      //   for(unsigned int gg2 = 0;gg2<nb_gauss_pts_v;gg2++) {
      //
      //     gaussDuffy(0,ind) = g_u(0,gg1);
      //     gaussDuffy(1,ind) = (1-g_u(0,gg1))*g_v(0,gg2);
      //
      //     detJocabian = 1 - g_u(0,gg1);
      //
      //     gaussDuffy(2,ind) = 2*g_u(1,gg1)*g_v(1,gg2)*detJocabian;

      //     ind += 1;
      //
      //   }
      // }

      /* Generalized Duffy transformation */
      double beta = 1.0;
      ublas::matrix_row<MatrixDouble > gp_u (g_u,0);
      // // std::transform(gp_u.begin(), gp_u.end(), gp_u.begin(), (double(*)(double)) pow);
      for(unsigned int gg1 = 0;gg1<nb_gauss_pts_u;gg1++) {
        gp_u(gg1) = pow(gp_u(gg1),beta);
      }
      //
      // gaussDuffy.resize(3,nb_gauss_pts_u*nb_gauss_pts_v);
      // gaussDuffy.clear();
      //
      // for(unsigned int gg1 = 0;gg1<nb_gauss_pts_u;gg1++) {
      //   for(unsigned int gg2 = 0;gg2<nb_gauss_pts_v;gg2++) {
      //
      //     gaussDuffy(0,ind) = gp_u(gg1);
      //     gaussDuffy(1,ind) = (1-gp_u(gg1))*g_v(0,gg2);
      //     detJocabian = 2*beta * pow(g_u(0,gg1),beta - 1.0) * (1 - gp_u(gg1));
      //     // detJocabian = 1;
      //     gaussDuffy(2,ind) = g_u(1,gg1)*g_v(1,gg2)*detJocabian; /* 3.7500,2.0691,3.750088423604010, */
      //     ind += 1;
      //
      //   }
      // }

      for(unsigned int gg1 = 0;gg1<nb_gauss_pts_u;gg1++) {
        for(unsigned int gg2 = 0;gg2<nb_gauss_pts_v;gg2++) {

          // gaussDuffy(0,ind) = gp_u(gg1);
          // gaussDuffy(1,ind) = gp_u(gg1)*g_v(0,gg2);
          detJocabian = beta * pow(g_u(0,gg1),2*beta - 1.0);

          gaussDuffy(0,ind) = gp_u(gg1)*g_v(0,gg2);
          gaussDuffy(1,ind) = gp_u(gg1) - gp_u(gg1)*g_v(0,gg2);

          gaussDuffy(2,ind) = 2*g_u(1,gg1)*g_v(1,gg2)*detJocabian; /* */
          ind += 1;

        }
      }

      PetscFunctionReturn(0);
    }

    /// !\brief It will be removed in the future use other variant
    PetscErrorCode setGaussPts(int order) {
      PetscErrorCode ierr;
      PetscFunctionBegin;
      /* FIXME order is not retrieve correctly from field seted */

      if(order != previousOrder) {
        ierr = setGeneralizedDuffyGaussPts(order,gaussPts); CHKERRQ(ierr);
        previousOrder = order;
      } else {
        /* Guassian quadrature rule remains samilar order */
      }
      PetscFunctionReturn(0);
    }


  };

  boost::ptr_map<string,ForcesAndSurcesCore> feRhs; // surface element for LHS
  boost::ptr_map<string,ForcesAndSurcesCore> feLhs; // surface element for RHS

  /** \brief Volume element data
  * \ingroup mofem_helmholtz_elem
  */
  struct VolumeData {
    double dEnsity;
    complex< double > waveNumber;
    Range tEts; ///< contains elements in block set
  };
  map<int,VolumeData> volumeData;

  /** \brief Surface element data
  * \ingroup mofem_helmholtz_elem
  */
  struct SurfaceData {

    double aDmittance_real;
    double aDmittance_imag;
    Range tRis; ///< surface triangles where hate flux is applied

  };
  map<int,SurfaceData> surfaceIncidentWaveBcData;
  map<int,SurfaceData> surfaceIncidentWaveBc2Data;
  map<int,SurfaceData> surfaceHomogeneousBcData;
  map<int,SurfaceData> surfaceHomogeneousBc2Data;
  map<int,SurfaceData> sommerfeldBcData;
  map<int,SurfaceData> baylissTurkelBcData;

  struct GlobalParameters {
    pair<double,PetscBool> waveNumber;
    pair<double,PetscBool> fRequency;
    pair<double,PetscBool> dEnsity;
    pair<double,PetscBool> vElocity;
    pair<double,PetscBool> aTtenuation; /*/ attenuation for radiation wave need to be implemented */
    pair<double,PetscBool> rAdius;
    pair<double,PetscBool> complexWaveNumber; /* Imaginary part of leaky SAW number */
    pair<double,PetscBool> leakySawVelocity;
    pair<double,PetscBool> transmissionCoefficient; /* Imaginary part of leaky SAW number */
    pair<double,PetscBool> surfaceAdmittance;
    pair<double,PetscBool> materialCoefficient1;
    pair<double,PetscBool> materialCoefficient2;
    pair<double,PetscBool> amplitudeOfIncidentWaveReal;
    pair<double,PetscBool> amplitudeOfIncidentWaveImag;
    pair<double,PetscBool> signalLength;
    pair<double,PetscBool> signalDuration;
    pair<int,PetscBool> nbOfPointsInTime;
    pair<int,PetscBool> bEta;
    pair<int,PetscBool> oRder;
    pair<double,PetscBool> qUadrature;
    pair<VectorDouble,PetscBool> waveDirection;
    pair<VectorDouble,PetscBool> waveOscilationDirection;
    pair<VectorDouble,PetscBool> sourceCoordinate;
    pair<PetscBool,PetscBool> isMonochromaticWave;
    pair<PetscBool,PetscBool> second_order_stress;
    pair<PetscBool,PetscBool> isHomogenous;
    pair<PetscBool,PetscBool> isIncidentWave;
    pair<PetscBool,PetscBool> isRadiation;
    pair<PetscBool,PetscBool> isDirichlet;
    pair<PetscBool,PetscBool> isRayleigh;
    pair<PetscBool,PetscBool> isDuffy;

    boost::shared_array<kiss_fft_cpx> complexOut;
    int complexOutSize;
    int timeStep;
  };
  GlobalParameters globalParameters;


  PetscErrorCode getGlobalParametersFromLineCommandOptions() {
    PetscErrorCode ierr;

    PetscFunctionBegin;
    ierr = PetscOptionsBegin(mField.get_comm(),NULL,"Helmholtz problem options","none"); CHKERRQ(ierr);

    globalParameters.waveNumber.first = 1;
    ierr = PetscOptionsReal("-wave_number","wave number","",
      globalParameters.waveNumber.first,
      &globalParameters.waveNumber.first,&globalParameters.waveNumber.second); CHKERRQ(ierr);
    if(!globalParameters.waveNumber.second) {
      SETERRQ(PETSC_COMM_SELF,1,"wave number not given, set in line command -wave_number to fix problem");
    }

    globalParameters.fRequency.first = 0;
    ierr = PetscOptionsReal("-frequency","frequency of the fluid","",
      globalParameters.fRequency.first,
      &globalParameters.fRequency.first,&globalParameters.fRequency.second); CHKERRQ(ierr);

    globalParameters.nbOfPointsInTime.first = 0;
      ierr = PetscOptionsInt("-nb_of_time_step","nb of time steps used in DFT","",
        globalParameters.nbOfPointsInTime.first,
        &globalParameters.nbOfPointsInTime.first,&globalParameters.nbOfPointsInTime.second); CHKERRQ(ierr);

    globalParameters.dEnsity.first = 0;
    ierr = PetscOptionsReal("-density","density of the fluid","",
      globalParameters.dEnsity.first,
      &globalParameters.dEnsity.first,&globalParameters.dEnsity.second); CHKERRQ(ierr);

    globalParameters.vElocity.first = 0;
    ierr = PetscOptionsReal("-velocity","velocity of the fluid","",
      globalParameters.vElocity.first,
      &globalParameters.vElocity.first,&globalParameters.vElocity.second); CHKERRQ(ierr);

    globalParameters.aTtenuation.first = 0;
    ierr = PetscOptionsReal("-attenuation","transverse attenuation","",
      globalParameters.aTtenuation.first,
      &globalParameters.aTtenuation.first,&globalParameters.aTtenuation.second); CHKERRQ(ierr);

    globalParameters.rAdius.first = 0;
    ierr = PetscOptionsReal("-droplet_radius","droplet_radius","",
      globalParameters.rAdius.first,
      &globalParameters.rAdius.first,&globalParameters.rAdius.second); CHKERRQ(ierr);


    globalParameters.complexWaveNumber.first = 0;
    ierr = PetscOptionsReal("-complex_wave_number","complex part of wave number (imaginary)","",
      globalParameters.complexWaveNumber.first,
      &globalParameters.complexWaveNumber.first,&globalParameters.complexWaveNumber.second); CHKERRQ(ierr);

    globalParameters.leakySawVelocity.first = 0;
    ierr = PetscOptionsReal("-leaky_saw_velocity","Leaky SAW wave velocity (imaginary)","",
      globalParameters.leakySawVelocity.first,
      &globalParameters.leakySawVelocity.first,&globalParameters.leakySawVelocity.second); CHKERRQ(ierr);

    globalParameters.transmissionCoefficient.first = 1.0;
    ierr = PetscOptionsReal("-transmission_coefficient","Leaky SAW wave velocity (imaginary)","",
      globalParameters.transmissionCoefficient.first,
      &globalParameters.transmissionCoefficient.first,&globalParameters.transmissionCoefficient.second); CHKERRQ(ierr);

    globalParameters.surfaceAdmittance.first = 0;
    ierr = PetscOptionsReal("-surface_admittance","surface admitance applied to all surface elements on MIX_INCIDENT_WAVE_BC","",
      globalParameters.surfaceAdmittance.first,
      &globalParameters.surfaceAdmittance.first,
      &globalParameters.surfaceAdmittance.second); CHKERRQ(ierr);

    globalParameters.materialCoefficient1.first = 0;
    ierr = PetscOptionsReal("-material_coefficient1","material property for same type of surface 1","",
      globalParameters.materialCoefficient1.first,
      &globalParameters.materialCoefficient1.first,
      &globalParameters.materialCoefficient1.second); CHKERRQ(ierr);

    globalParameters.materialCoefficient2.first = 0;
    ierr = PetscOptionsReal("-material_coefficient2","material property for same type of surface 2","",
      globalParameters.materialCoefficient2.first,
      &globalParameters.materialCoefficient2.first,
      &globalParameters.materialCoefficient2.second); CHKERRQ(ierr);

    VectorDouble amplitude;
    amplitude.resize(2);
    amplitude.clear();
    amplitude[0] = 1; // default:A_r = 1
    amplitude[1] = -1; // default:A_i = -1

    int namplitude = 2;
    ierr = PetscOptionsGetRealArray(PETSC_NULL,PETSC_NULL,"-amplitude_of_incident_wave",&amplitude[0],&namplitude,NULL); CHKERRQ(ierr);
    if(namplitude > 2 ) {
      SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"*** ERROR -amplitude_of_incident_wave [Ar Ai]");
    }

    globalParameters.amplitudeOfIncidentWaveReal.first = amplitude[0];
    globalParameters.amplitudeOfIncidentWaveImag.first = amplitude[1];

    if(namplitude == 1 ) {
      globalParameters.amplitudeOfIncidentWaveImag.first = 0;
    }

    globalParameters.isMonochromaticWave.first = PETSC_TRUE;
    ierr = PetscOptionsBool(
      "-monochromatic_wave",
      "If true analysis is for monochromatic wave","",
      PETSC_TRUE,
      &globalParameters.isMonochromaticWave.first,
      &globalParameters.isMonochromaticWave.second
    ); CHKERRQ(ierr);

    globalParameters.second_order_stress.first = PETSC_FALSE;
    ierr = PetscOptionsBool(
      "-radiation_force",
      "If true analysis is for Radiation Force","",
      globalParameters.second_order_stress.first,
      &globalParameters.second_order_stress.first,
      &globalParameters.second_order_stress.second
    ); CHKERRQ(ierr);

    globalParameters.isHomogenous.first = PETSC_FALSE;
    ierr = PetscOptionsBool(
      "-surface_condition",
      "If true the surface is defined as homogeneous Neumann (Dirichlet) BC","",
      globalParameters.isHomogenous.first,
      &globalParameters.isHomogenous.first,
      &globalParameters.isHomogenous.second
    ); CHKERRQ(ierr);
    globalParameters.isIncidentWave.first = PETSC_FALSE;

    globalParameters.isRadiation.first = PETSC_TRUE;
    ierr = PetscOptionsBool(
      "-radiation_field",
      "If true the problem is Exterior BV problem, otherwise, it is an Interior problem","",
      PETSC_TRUE,
      &globalParameters.isRadiation.first,
      &globalParameters.isRadiation.second
    ); CHKERRQ(ierr);

    globalParameters.isDirichlet.first = PETSC_FALSE;
    ierr = PetscOptionsBool(
      "-dirichlet",
      "If true the problem is with Neumann-Dirichlet BC, false for pure Neumann incident wave BC","",
      PETSC_TRUE,
      &globalParameters.isDirichlet.first,
      &globalParameters.isDirichlet.second
    ); CHKERRQ(ierr);

    globalParameters.isRayleigh.first = PETSC_FALSE;
    ierr = PetscOptionsBool(
      "-rayleigh_wave",
      "If true the incident is Rayleigh wave, otherwise, it is an non-attenuated wave","",
      PETSC_FALSE,
      &globalParameters.isRayleigh.first,
      &globalParameters.isRayleigh.second
    ); CHKERRQ(ierr);

    if(!globalParameters.isRadiation.first) {
      globalParameters.amplitudeOfIncidentWaveReal.first = - globalParameters.amplitudeOfIncidentWaveReal.first;
      globalParameters.amplitudeOfIncidentWaveImag.first = - globalParameters.amplitudeOfIncidentWaveImag.first;
    }

    globalParameters.signalLength.first = 1;
    ierr = PetscOptionsReal("-signal_length",
      "if DFT analysis this set signal length","",
      globalParameters.signalLength.first,
      &globalParameters.signalLength.first,
      &globalParameters.signalLength.second); CHKERRQ(ierr);

    globalParameters.signalDuration.first = 1;
    ierr = PetscOptionsReal("-signal_duration",
      "if DFT analysis this set signal duration","",
      globalParameters.signalDuration.first,
      &globalParameters.signalDuration.first,
      &globalParameters.signalDuration.second); CHKERRQ(ierr);


    globalParameters.waveDirection.first.resize(3);
    globalParameters.waveDirection.first.clear();
    globalParameters.waveDirection.first[2] = 1;
    int nmax = 3;
    ierr = PetscOptionsRealArray(
      "-wave_direction","direction of incident wave","",&globalParameters.waveDirection.first[0],&nmax,&globalParameters.waveDirection.second
    ); CHKERRQ(ierr);
    if(globalParameters.waveDirection.second) {
      if(nmax > 0 && nmax != 3) {

        SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"*** ERROR -wave_direction [3*1 vector] default:X direction [0,0,1]");

      }
    }

    globalParameters.waveOscilationDirection.first.resize(3);
    globalParameters.waveOscilationDirection.first.clear();
    globalParameters.waveOscilationDirection.first = globalParameters.waveDirection.first;
    int imax = 3;
    ierr = PetscOptionsRealArray(
      "-wave_oscilation_direction","direction of Longitudinal wave","",&globalParameters.waveOscilationDirection.first[0],&imax,&globalParameters.waveOscilationDirection.second
    ); CHKERRQ(ierr);
    if(globalParameters.waveOscilationDirection.second) {
      if(imax > 0 && imax != 3) {

        SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"*** ERROR -wave_oscilation_direction [3*1 vector] default:X direction [0,0,1]");

      }
    }

    globalParameters.sourceCoordinate.first.resize(3);
    globalParameters.sourceCoordinate.first.clear();
    int mmax = 3;
    ierr = PetscOptionsRealArray(
      "-source_coordinate","Coodinates of the source point","",&globalParameters.sourceCoordinate.first[0],&mmax,&globalParameters.sourceCoordinate.second
    ); CHKERRQ(ierr);
    if(globalParameters.sourceCoordinate.second) {
      if(mmax > 0 && mmax != 3) {

        SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"*** ERROR -source_coordinate [3*1 vector] default: origin location [0,0,0]");

      }
    }

    globalParameters.isDuffy.first = PETSC_FALSE;
    ierr = PetscOptionsBool(
      "-duffy",
      "If true Duffy transformation","",
      globalParameters.isDuffy.first,
      &globalParameters.isDuffy.first,
      &globalParameters.isDuffy.second
    ); CHKERRQ(ierr);

    globalParameters.bEta.first = 0;
      ierr = PetscOptionsInt("-beta","the exponent beta of Duffy transformation","",
        globalParameters.bEta.first,
        &globalParameters.bEta.first,&globalParameters.bEta.second); CHKERRQ(ierr);

    globalParameters.oRder.first = 2;
      ierr = PetscOptionsInt("-my_order","the order of polynomial (affect the quadrature rule)","",
        globalParameters.oRder.first,
        &globalParameters.oRder.first,&globalParameters.oRder.second); CHKERRQ(ierr);

    // if(globalParameters.oRder.first > 6) {
    //   SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"*** ERROR order > 6");
    // }

    globalParameters.qUadrature.first = 2;
    ierr = PetscOptionsReal("-quadrature_rule",
      "if DFT analysis this set signal duration","",
      globalParameters.qUadrature.first,
      &globalParameters.qUadrature.first,
      &globalParameters.qUadrature.second); CHKERRQ(ierr);


    ierr = PetscOptionsEnd(); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }

  /** \brief Common data used by volume and surface elements
  * \ingroup mofem_helmholtz_elem
  */
  struct CommonData {

    map<string,VectorDouble > pressureAtGaussPts;
    map<string,MatrixDouble > gradPressureAtGaussPts;
    MatrixDouble hoCoordsTri;
    MatrixDouble hoCoordsTet;

    map<EntityType, vector< VectorInt > > imIndices;

  };
  CommonData commonData;

  MoFEM::Interface &mField;
  int addToRank; ///< default value 1, i.e. assumes that geometry is approx. by quadratic functions.

  HelmholtzElement(
    MoFEM::Interface &m_field):
    mField(m_field),
    addToRank(0) {}

  struct OpGetImIndices: public ForcesAndSurcesCore::UserDataOperator  {

    CommonData &commonData;
    const string reFieldName,imFieldName;
    bool takeIndicesFromElementRowIndices;

    OpGetImIndices(
      const string re_field_name,const string im_field_name,CommonData &common_data
    ):
    ForcesAndSurcesCore::UserDataOperator(re_field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
    commonData(common_data),
    reFieldName(re_field_name),
    imFieldName(im_field_name) {

      if(reFieldName!=imFieldName) {

        takeIndicesFromElementRowIndices = false;

      } else {

        takeIndicesFromElementRowIndices = true;

      }

    }

    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;

      int nb_row_dofs = data.getIndices().size();
      if(nb_row_dofs==0) PetscFunctionReturn(0);

      if(commonData.imIndices[type].size()!=6) {
        commonData.imIndices[type].resize(6);
      }

      if(takeIndicesFromElementRowIndices) {

        if((commonData.imIndices[type]).size()<=side) {
          SETERRQ2(
            PETSC_COMM_WORLD,MOFEM_DATA_INCONSISTENCY,
            "Worng size %d <= %d",(commonData.imIndices[type]).size(),side
          );
        }
        // (commonData.imIndices[type])[side].resize(data.getIndices().size(),false);
        (commonData.imIndices[type])[side] = data.getIndices();

      } else {

      	// Get rows and cols indices of imaginary part and assemble matrix.
      	// Note: However HELMHOLTZ_IMIM_FE element is not calculated, since
      	// matrix A on real elements is equal to matrix in imaginary elements,
      	// it need be to declared. Declaration indicate that on imaginary part,
      	// assembled matrix has non-zero values;
      	PetscErrorCode ierr;
      	ierr = getPorblemRowIndices(imFieldName,type,side,(commonData.imIndices[type])[side]); CHKERRQ(ierr);

      }

      PetscFunctionReturn(0);
    }

  };

  /** \brief Calculate pressure and gradient of pressure in volume
    */
  struct OpGetValueAndGradAtGaussPts: public VolumeElementForcesAndSourcesCore::UserDataOperator {

    CommonData &commonData;
    const string fieldName;
    OpGetValueAndGradAtGaussPts(const string field_name,CommonData &common_data):
    VolumeElementForcesAndSourcesCore::UserDataOperator(field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
    commonData(common_data),fieldName(field_name) {}

    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;
      try {

        int nb_dofs = data.getFieldData().size();
        if(nb_dofs==0) PetscFunctionReturn(0);
        int nb_gauss_pts = data.getN().size1();

        VectorDouble &value = commonData.pressureAtGaussPts[fieldName];
        MatrixDouble &gradient = commonData.gradPressureAtGaussPts[fieldName];

        // initialize
        value.resize(nb_gauss_pts,false);
        gradient.resize(nb_gauss_pts,3,false);
        if(type == MBVERTEX) {
          gradient.clear();
          value.clear();
        }

        for(int gg = 0;gg<nb_gauss_pts;gg++) {

          value[gg] += inner_prod(data.getN(gg,nb_dofs),data.getFieldData());
          //ublas::noalias(ublas::matrix_row<MatrixDouble >(gradient,gg)) +=
          //prod( trans(data.getDiffN(gg,nb_dofs)), data.getFieldData() );
          cblas_dgemv(CblasRowMajor,CblasTrans,
            nb_dofs,3,1,
            &data.getDiffN()(gg,0),3,
            &data.getFieldData()[0],1,
            1,&gradient(gg,0),1
          );

        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  /** \brief Calculate pressure on surface
    */
  struct OpGetValueAtGaussPts: public FaceElementForcesAndSourcesCore::UserDataOperator {

    CommonData &commonData;
    const string fieldName;
    OpGetValueAtGaussPts(const string field_name,CommonData &common_data):
    FaceElementForcesAndSourcesCore::UserDataOperator(field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
    commonData(common_data),fieldName(field_name) {}

    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;
      try {

        int nb_dofs = data.getFieldData().size();
        if(nb_dofs==0) PetscFunctionReturn(0);
        int nb_gauss_pts = data.getN().size1();

        VectorDouble &value = commonData.pressureAtGaussPts[fieldName];

        // Initialize
        value.resize(nb_gauss_pts,false);
        if(type == MBVERTEX) {
          value.clear();
        }

        for(int gg = 0;gg<nb_gauss_pts;gg++) {
          value[gg] += inner_prod(data.getN(gg,nb_dofs),data.getFieldData());
        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };


  /** \brief Coordinates at integration points, on surface.

    This takes into account HO approximation for geometry

    */
  struct OpHoCoordTri: public FaceElementForcesAndSourcesCore::UserDataOperator {

    MatrixDouble &hoCoordsTri;
    OpHoCoordTri(const string field_name,MatrixDouble &ho_coords):
      FaceElementForcesAndSourcesCore::UserDataOperator(field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
      hoCoordsTri(ho_coords) {}

    /*
    Cartesian coordinates for integration points inside elements
    X^coordinates = DOF dot* N
    */
    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;

      try {

        int nb_dofs = data.getFieldData().size();
        if(nb_dofs==0) PetscFunctionReturn(0);

        hoCoordsTri.resize(data.getN().size1(),3,false);
        if(type == MBVERTEX) {
          hoCoordsTri.clear();
        }

        for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

	        // Calculate x,y,z at each GaussPts
          for(int dd = 0;dd<3;dd++) {
            hoCoordsTri(gg,dd) += cblas_ddot(nb_dofs/3,&data.getN(gg)[0],1,&data.getFieldData()[dd],3);
          }

        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  /** \brief Coordinates at integration points, on Volume.

    This takes into account HO approximation for geometry

    */
  struct OpHoCoordTet: public VolumeElementForcesAndSourcesCore::UserDataOperator {

    MatrixDouble &hoCoordsTet;
    OpHoCoordTet(const string field_name,MatrixDouble &ho_coords):
      VolumeElementForcesAndSourcesCore::UserDataOperator(field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
      hoCoordsTet(ho_coords) {}

    /*
    Cartesian coordinates for integration points inside elements
    X^coordinates = DOF dot* N
    */
    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;

      try {

        int nb_dofs = data.getFieldData().size();
        if(nb_dofs==0) PetscFunctionReturn(0);

        hoCoordsTet.resize(data.getN().size1(),3,false);
        if(type == MBVERTEX) {
          hoCoordsTet.clear();
        }

        for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

          // Calculate x,y,z at each GaussPts
          for(int dd = 0;dd<3;dd++) {
            hoCoordsTet(gg,dd) += cblas_ddot(nb_dofs/3,&data.getN(gg)[0],1,&data.getFieldData()[dd],3);
          }

        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  /** \brief Rhs vector for Helmholtz operator
    \ingroup mofem_helmholtz_elem

    \f[
    F_i = \int_{\Omega^e} \frac{\partial N_i}{\partial X_j} \frac{\partial p}{\partial X_j} - k^2 N_i p \textrm{d}V
    \f]

  */
  struct OpHelmholtzRhs: public VolumeElementForcesAndSourcesCore::UserDataOperator {

    VolumeData &dAta;
    CommonData &commonData;
    const string fieldName;
    Vec F;
    bool rEal;

    OpHelmholtzRhs(
      const string field_name,Vec _F,VolumeData &data,CommonData &common_data,bool real):
      VolumeElementForcesAndSourcesCore::UserDataOperator(field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
      dAta(data),
      commonData(common_data),
      fieldName(field_name),
      rEal(real),
      F(_F) { }

    VectorDouble Nf;

    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;

      int nb_row_dofs = data.getIndices().size();
      if(nb_row_dofs==0) PetscFunctionReturn(0);
      if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
        PetscFunctionReturn(0);
      }

      try {

        PetscErrorCode ierr;

        VectorDouble &pressure = commonData.pressureAtGaussPts[fieldName];
        MatrixDouble &grad_p = commonData.gradPressureAtGaussPts[fieldName];

        Nf.resize(nb_row_dofs,false);
        Nf.clear();

        // wave number "k" is the proportional to the frequency of incident wave
        // and represents number of waves per wave length 2Pi - 2Pi/K
        complex< double > k_pow2 = dAta.waveNumber*dAta.waveNumber;
        double k_pow2_dis;
        if(rEal) {
          k_pow2_dis = real(k_pow2);
        } else {
          k_pow2_dis = imag(k_pow2);
        }

        for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

          double val = getVolume()*getGaussPts()(3,gg);
          if(getHoGaussPtsDetJac().size()>0) {
            val *= getHoGaussPtsDetJac()[gg]; // higher order geometry
          }

          /// Integrate diffN^T grad_p - k^2 N^T p dV

          //const ublas::matrix_row<MatrixDouble > gard_p_at_gauss_pt(grad_p,gg);
          //ublas::noalias(Nf) += val*prod(data.getDiffN(gg,nb_row_dofs),gard_p_at_gauss_pt);
          cblas_dgemv(CblasRowMajor,CblasNoTrans,nb_row_dofs,3,val,
            &data.getDiffN()(gg,0),3,
            &grad_p(gg,0),1,
            1.,&Nf[0],1
          );
          ublas::noalias(Nf) -= val*k_pow2_dis*data.getN(gg,nb_row_dofs)*pressure[gg];

        }


        ierr = VecSetValues(F,data.getIndices().size(),
        &data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  /** \biref operator to calculate right hand side wave source term F
  * \infroup mofem_helmholtz_elem
  */
  struct OpHelmholtzRhs_F: public VolumeElementForcesAndSourcesCore::UserDataOperator {

    VolumeData &dAta;
    CommonData &commonData;
    GlobalParameters &globalParameters;
    const string fieldName;
    const double epsilon;
    Vec F;
    double x,y,z;

    OpHelmholtzRhs_F(const string field_name,Vec _F,VolumeData &data,CommonData &common_data,GlobalParameters &global_parameters):
      VolumeElementForcesAndSourcesCore::UserDataOperator(field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
      dAta(data),
      commonData(common_data),
      fieldName(field_name),
      F(_F),
      epsilon(1.0e-3),
      globalParameters(global_parameters) {}

    VectorDouble Nf;

    /** \brief calculate Helmholtz RHS source term.
      *
      * F = int diffN^T \delta(x-x_{0}) dOmega^2
      *
      */

    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;

      int nb_row_dofs = data.getIndices().size();
      if(nb_row_dofs==0) PetscFunctionReturn(0);
      if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
        PetscFunctionReturn(0);
      }

      try {

        PetscErrorCode ierr;

        Nf.resize(nb_row_dofs,false);
        Nf.clear();

        for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

          double val = getVolume()*getGaussPts()(3,gg);
          double sOurce;

          if(getHoGaussPtsDetJac().size()>0) {
            val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
          }

          if(commonData.hoCoordsTet.size1()) {
            x = commonData.hoCoordsTet(gg,0);
            y = commonData.hoCoordsTet(gg,1);
            z = commonData.hoCoordsTet(gg,2);
          } else {
            x = getCoordsAtGaussPts()(gg,0);
            y = getCoordsAtGaussPts()(gg,1);
            z = getCoordsAtGaussPts()(gg,2);
          }


          if(std::abs(globalParameters.sourceCoordinate.first[0] - x) < epsilon && std::abs(globalParameters.sourceCoordinate.first[1] - y) < epsilon && std::abs(globalParameters.sourceCoordinate.first[2] - z) < epsilon) {

            sOurce = globalParameters.amplitudeOfIncidentWaveReal.first;
          } else {
            sOurce = 0;
          }
            ublas::noalias(Nf) += val*sOurce*data.getN(gg,nb_row_dofs);

        }

        ierr = VecSetValues(F,data.getIndices().size(),
        &data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  /** \brief Lhs for helmholtz operator

    \ingroup mofem_helmholtz_elem

    \f[
    A_{ik} = \int_{\Omega^e} \frac{\partial N_i}{\partial X_j} \frac{\partial N_k}{\partial X_j} - k^2 N_i N_k \textrm{d}V
    \f]

    */
    struct OpHelmholtzLhs: public VolumeElementForcesAndSourcesCore::UserDataOperator {

    VolumeData &dAta;
    CommonData &commonData;
    const string reFieldName;
    const string imFieldName;
    Mat A;

    OpHelmholtzLhs(
      const string &re_field_name,const string &im_field_name,
      Mat _A,VolumeData &data,CommonData &common_data
    ):
    VolumeElementForcesAndSourcesCore::UserDataOperator(
      re_field_name,re_field_name,ForcesAndSurcesCore::UserDataOperator::OPROWCOL
    ),
    dAta(data),
    commonData(common_data),
    reFieldName(re_field_name),
    imFieldName(im_field_name),
    A(_A) {}

    MatrixDouble K,transK,K_im,transK_im;

    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data) {
      PetscFunctionBegin;

      int nb_rows = row_data.getIndices().size();
      if(nb_rows==0) PetscFunctionReturn(0);
      int nb_cols = col_data.getIndices().size();
      if(nb_cols==0) PetscFunctionReturn(0);
      if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
        PetscFunctionReturn(0);
      }

      try {

        K.resize(nb_rows,nb_cols,false);
        K.clear();
        // K_im.resize(nb_rows,nb_cols,false);
        // K_im.clear();

        complex< double > k_pow2 = dAta.waveNumber*dAta.waveNumber;
        // cerr << "\n V number of gauss points : = \n " << row_data.getN().size1() << endl;
        for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {

          double val = getVolume()*getGaussPts()(3,gg);
          if(getHoGaussPtsDetJac().size()>0) {
            val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
          }

          const double *diff_row_mat_ptr = &row_data.getDiffN()(gg,0);
          const double *diff_col_mat_ptr = &col_data.getDiffN()(gg,0);
          cblas_dgemm(
            CblasRowMajor,CblasNoTrans,CblasTrans,
            nb_rows,nb_cols,3,
            +val,diff_row_mat_ptr,3,
            diff_col_mat_ptr,3,
            1,&K(0,0),nb_cols
          );

          const double *row_mat_ptr = &row_data.getN()(gg,0);
          const double *col_mat_ptr = &col_data.getN()(gg,0);
          cblas_dger(CblasRowMajor,
            nb_rows,nb_cols,
            -val*real(k_pow2),
            row_mat_ptr,1,
            col_mat_ptr,1,
            &K(0,0),nb_cols
          );
          /* imaginary matrix */
          /* // cout << "\n col_mat_ptr = \n " << col_data.getN()(gg,nb_cols) << endl;
          // cout << "\n *row_mat_ptr = \n " << row_data.getN()(gg,nb_rows) << endl;
          // cblas_dger(CblasRowMajor,
          //   nb_rows,nb_cols,
          //   -val*imag(k_pow2),
          //   row_mat_ptr,1,
          //   col_mat_ptr,1,
          //   &K_im(0,0),nb_cols
          // ); */
          // noalias(K_im) -= val*imag(k_pow2)*outer_prod( row_data.getN(gg,nb_rows),col_data.getN(gg,nb_cols) );
          /*noalias(K) -= val*k_pow2*outer_prod( row_data.getN(gg,nb_rows),col_data.getN(gg,nb_cols) ); */
          /*noalias(K) += val*prod(row_data.getDiffN(gg,nb_rows),trans(col_data.getDiffN(gg,nb_cols))); */

        }

        PetscErrorCode ierr;
        // real-real
        ierr = MatSetValues(
          A,
          nb_rows,&row_data.getIndices()[0],
          nb_cols,&col_data.getIndices()[0],
          &K(0,0),ADD_VALUES
        ); CHKERRQ(ierr);
        // cout << "\n real-real nb_rows,&row_data.getIndices()[0] \n " << row_data.getIndices()[0] << endl;
        if(row_side != col_side || row_type != col_type) {
          transK.resize(nb_cols,nb_rows,false);
          noalias(transK) = trans(K);
          ierr = MatSetValues(
            A,
            nb_cols,&col_data.getIndices()[0],
            nb_rows,&row_data.getIndices()[0],
            &transK(0,0),ADD_VALUES
          ); CHKERRQ(ierr);
        }

        // imag-imag
        ierr = MatSetValues(
          A,
          nb_rows,&((commonData.imIndices[row_type])[row_side])[0],
          nb_cols,&((commonData.imIndices[col_type])[col_side])[0],
          &K(0,0),ADD_VALUES
        ); CHKERRQ(ierr);
        // cout << "\n imag-imag ((commonData.imIndices[row_type])[row_side])[0] \n " << ((commonData.imIndices[row_type])[row_side])[0] << endl;
        if(row_side != col_side || row_type != col_type) {
          transK.resize(nb_cols,nb_rows,false);
          noalias(transK) = trans(K);
          ierr = MatSetValues(
            A,
            nb_cols,&((commonData.imIndices[col_type])[col_side])[0],
            nb_rows,&((commonData.imIndices[row_type])[row_side])[0],
            &transK(0,0),ADD_VALUES
          ); CHKERRQ(ierr);
        }
        // /* imag-real */
        // ierr = MatSetValues(
        //   A,
        //   nb_rows,&((commonData.imIndices[row_type])[row_side])[0],
        //   nb_cols,&col_data.getIndices()[0],
        //   &K_im(0,0),ADD_VALUES
        // ); CHKERRQ(ierr);
        // // cout << "\n imag-real ((commonData.imIndices[row_type])[row_side])[0] \n " << ((commonData.imIndices[row_type])[row_side])[0] << endl;
        // if(row_side != col_side || row_type != col_type) {
        //   transK_im.resize(nb_cols,nb_rows,false);
        //   noalias(transK_im) = trans(K_im);
        //   ierr = MatSetValues(
        //     A,
        //     nb_cols,&col_data.getIndices()[0],
        //     nb_rows,&((commonData.imIndices[row_type])[row_side])[0],
        //     &transK_im(0,0),ADD_VALUES
        //   ); CHKERRQ(ierr);
        // }

        // /* real-imag */
        // noalias(K_im) = K_im*-1.0;
        // ierr = MatSetValues(
        //   A,
        //   nb_rows,&row_data.getIndices()[0],
        //   nb_cols,&((commonData.imIndices[col_type])[col_side])[0],
        //   &K_im(0,0),ADD_VALUES
        // ); CHKERRQ(ierr);
        //
        // if(row_side != col_side || row_type != col_type) {
        //   transK_im.resize(nb_cols,nb_rows,false);
        //   noalias(transK_im) = trans(K_im);
        //   ierr = MatSetValues(
        //     A,
        //     nb_cols,&((commonData.imIndices[col_type])[col_side])[0],
        //     nb_rows,&row_data.getIndices()[0],
        //     &transK(0,0),ADD_VALUES
        //   ); CHKERRQ(ierr);
        // }


      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  struct ZeroFunVal {

    VectorDouble vAl;

    VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {
      vAl.resize(2);
      vAl[0] = 0;
      vAl[1] = 0;
      return vAl;
    }

  };
  ZeroFunVal zeroFunVal;

  struct BaylissTurkel {

    BaylissTurkel() {}

    VectorDouble vAl;
    VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

      const complex< double > i( 0.0, 1.0 );
      double x2=x*x,y2=y*y,z2=z*z;
      double R = sqrt(x2+y2+z2);

      complex< double > result = 1.0/(2.0*R);

      vAl.resize(2);
      vAl[0] = std::real(result);
      vAl[1] = std::imag(result);

      return vAl;
    }

  };

  /** \brief Calculate incident wave scattered on hard surface
      \ingroup mofem_helmholtz_elem

    This part shows the Neumann boundary condition of the exterior boundary
    value problem for the rigid scatterer.

    \f]
    \left. \left\{ \mathbf{n} \cdot  (ik\mathbf{d} A_{0} e^{ik \mathbf{d} \cdot \mathbf{x}})
    \right\}
    \f[

    where \f$\mathbf{n})\f$ is the normal vector point outward of the surface of scatterer.
      \f$\mathbf{x})\f$ is the cartesian coordinates and \f$d\f$ is the unit vector represents the
      direction of the incident wave. Further more


    */
  struct IncidentWaveNeumannF2 {

    GlobalParameters &globalParameters;
    IncidentWaveNeumannF2(GlobalParameters &global_parameters): globalParameters(global_parameters) {}

    VectorDouble cOordinate;
    VectorDouble vAl;

    VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

      const complex< double > i( 0.0, 1.0 );

      cOordinate.resize(3);
      cOordinate[0] = x;
      cOordinate[1] = y;
      cOordinate[2] = z;
      complex< double > wave_number = globalParameters.waveNumber.first + i*globalParameters.complexWaveNumber.first;
      complex< double > attenuation(0,0);
      complex<double> p_inc = 0;
      // attenuation.real(0);
      // attenuation.imag(0);
      if(globalParameters.isRayleigh.first) {
        /* speed of SAW over speed of longitudinal wave */
        complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
        // if(y >= 0) {y = -2*y;}
        // attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
        attenuation = sqrt(1.0 - pow(vs_over_v1,2.0))*(y);
      }

      if(globalParameters.aTtenuation.first != 0) {
        double xOriginal = - std::sqrt(std::abs(std::pow(globalParameters.rAdius.first,2.0) - std::pow(z,2.0)));
        globalParameters.aTtenuation.first *= (x - xOriginal);
      }

      VectorDouble &oscilation_direction = globalParameters.waveOscilationDirection.first;
      double x1d = inner_prod(globalParameters.waveDirection.first,cOordinate);
      complex<double> amplitude = globalParameters.amplitudeOfIncidentWaveReal.first+i*globalParameters.amplitudeOfIncidentWaveImag.first;
      complex<double> angle = wave_number*(x1d);
      if(globalParameters.isRadiation.first){
        p_inc = amplitude*exp(+i*angle+wave_number*attenuation - globalParameters.aTtenuation.first);
      } else {
        p_inc = amplitude*exp(-i*angle+wave_number*attenuation - globalParameters.aTtenuation.first);
      }

      ublas::vector<complex<double > > grad(3);
      for(int ii = 0;ii<3;ii++) {
        grad[ii] = i*wave_number*oscilation_direction[ii]*p_inc;
      }
      complex<double > grad_n = globalParameters.transmissionCoefficient.first*inner_prod(grad,normal);
      // if(globalParameters.isRadiation.first){
      //   grad_n *= -1.;
      // }
      //// check if normal pointing to ceneter;
      //double dot = -inner_prod(normal,cOordinate);
      //if(dot < 0) grad_n *= -1;

      vAl.resize(2);
      vAl[0] = std::real(grad_n);
      vAl[1] = std::imag(grad_n);

      return vAl;
    }

  };

  /** \brief Calculate incident wave scattered on soft surface with admittance
      \ingroup mofem_helmholtz_elem

    This part shows the Dirichlet like boundary condition of the exterior boundary
    value problem for the rigid scatterer.

    \f]
    \left. \left\{A_{0} e^{ik \mathbf{d} \cdot \mathbf{x}})
    \right\}
    \f[

      \f$\mathbf{x})\f$ is the cartesian coordinates and \f$d\f$ is the unit vector represents the
      direction of the incident wave.



    \bug Assumes that normal sf surface pointing outward.
    */
  struct IncidentWaveDirichletF2 {

    GlobalParameters &globalParameters;
    IncidentWaveDirichletF2(GlobalParameters &global_parameters): globalParameters(global_parameters) {}

    VectorDouble cOordinate;
    VectorDouble vAl;

    VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

      const complex< double > i( 0.0, 1.0 );

      cOordinate.resize(3);
      cOordinate[0] = x;
      cOordinate[1] = y;
      cOordinate[2] = z;
      complex< double > wave_number = globalParameters.waveNumber.first + i*globalParameters.complexWaveNumber.first;
      complex< double > attenuation(0,0);
      if(globalParameters.isRayleigh.first) {
        /* speed of SAW over speed of longitudinal wave */
        complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
        attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
      }

      if(globalParameters.aTtenuation.first != 0) {
        double xOriginal = - std::sqrt(std::abs(std::pow(globalParameters.rAdius.first,2.0) - std::pow(z,2.0)));
        globalParameters.aTtenuation.first *= (x - xOriginal);
      }

      double x1d = inner_prod(globalParameters.waveDirection.first,cOordinate);
      complex<double> amplitude = globalParameters.amplitudeOfIncidentWaveReal.first+i*globalParameters.amplitudeOfIncidentWaveImag.first;
      complex<double> angle = wave_number*(x1d);
      complex<double> p_inc = globalParameters.transmissionCoefficient.first*globalParameters.surfaceAdmittance.first*amplitude*exp(i*angle+wave_number*attenuation - globalParameters.aTtenuation.first);
      // if(globalParameters.isRadiation.first){
      //   p_inc *= -i;
      // }
      vAl.resize(2);
      vAl[0] = std::real(p_inc);
      vAl[1] = std::imag(p_inc);




      return vAl;
    }

  };

  /** \brief Calculate incident wave scattered from point source
      \ingroup mofem_helmholtz_elem

    This part shows the Neumann boundary condition of the exterior boundary
    value problem for the rigid scatterer.

    \f]
    \left. \left\{ \mathbf{n} \cdot  (A_{0} \frac{e^{|x-x_{0}|}}{\4 \pi |x-x_{0}|})
    \right\}
    \f[

    where \f$\mathbf{n})\f$ is the normal vector point outward of the surface of scatterer.
      \f$\mathbf{x})\f$ is the cartesian coordinates and \f$d\f$ is the unit vector represents the
      direction of the incident wave. Further more


    */
  struct IncidentWaveDirichletPointSource {

    GlobalParameters &globalParameters;
    IncidentWaveDirichletPointSource(GlobalParameters &global_parameters): globalParameters(global_parameters) {}

    VectorDouble cOordinate;
    VectorDouble pOsition;
    VectorDouble vAl;

    VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

      const complex< double > i( 0.0, 1.0 );

      cOordinate.resize(3);
      cOordinate[0] = x;
      cOordinate[1] = y;
      cOordinate[2] = z;
      pOsition.resize(3);
      complex< double > wave_number = globalParameters.waveNumber.first + i*globalParameters.complexWaveNumber.first;
      complex< double > attenuation(0,0);
      if(globalParameters.isRayleigh.first) {
        /* speed of SAW over speed of longitudinal wave */
        complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
        attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
      }

      if(globalParameters.aTtenuation.first != 0) {
        double xOriginal = - std::sqrt(std::abs(std::pow(globalParameters.rAdius.first,2.0) - std::pow(z,2.0)));
        globalParameters.aTtenuation.first *= (x - xOriginal);
      }

      pOsition[0] = (cOordinate[0]-globalParameters.sourceCoordinate.first[0])*(cOordinate[0]-globalParameters.sourceCoordinate.first[0]);
      pOsition[1] = (cOordinate[1]-globalParameters.sourceCoordinate.first[1])*(cOordinate[1]-globalParameters.sourceCoordinate.first[1]);
      pOsition[2] = (cOordinate[2]-globalParameters.sourceCoordinate.first[2])*(cOordinate[2]-globalParameters.sourceCoordinate.first[2]);
      double absPoint = sqrt(pOsition[0]+pOsition[1]+pOsition[2]);
      complex<double> amplitude = globalParameters.amplitudeOfIncidentWaveReal.first+i*globalParameters.amplitudeOfIncidentWaveImag.first;
      complex<double> p_inc = globalParameters.transmissionCoefficient.first*amplitude*(exp(-i*wave_number*absPoint+wave_number*attenuation - globalParameters.aTtenuation.first)) / (4*M_PI*absPoint);

      // if(globalParameters.isRadiation.first){
      //   p_inc *= -i*globalParameters.surfaceAdmittance.first;
      // }

      vAl.resize(2);
      vAl[0] = std::real(p_inc);
      vAl[1] = std::imag(p_inc);

      return vAl;
    }

  };

  struct IncidentWaveNeumannPointSource {

    GlobalParameters &globalParameters;
    IncidentWaveNeumannPointSource(GlobalParameters &global_parameters): globalParameters(global_parameters) {}

    VectorDouble cOordinate;
    VectorDouble pOsition;
    VectorDouble vAl;

    VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

      const complex< double > i( 0.0, 1.0 );

      cOordinate.resize(3);
      cOordinate[0] = x;
      cOordinate[1] = y;
      cOordinate[2] = z;
      pOsition.resize(3);
      complex< double > wave_number = globalParameters.waveNumber.first + i*globalParameters.complexWaveNumber.first;
      complex< double > attenuation(0,0);
      if(globalParameters.isRayleigh.first) {
        /* speed of SAW over speed of longitudinal wave */
        complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
        attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
      }

      if(globalParameters.aTtenuation.first != 0) {
        double xOriginal = - std::sqrt(std::abs(std::pow(globalParameters.rAdius.first,2.0) - std::pow(z,2.0)));
        globalParameters.aTtenuation.first *= (x - xOriginal);
      }

      pOsition[0] = (cOordinate[0]-globalParameters.sourceCoordinate.first[0]);
      pOsition[1] = (cOordinate[1]-globalParameters.sourceCoordinate.first[1]);
      pOsition[2] = (cOordinate[2]-globalParameters.sourceCoordinate.first[2]);
      double absPoint = sqrt(pOsition[0]*pOsition[0]+pOsition[1]*pOsition[1]+pOsition[2]*pOsition[2]);
      complex<double> amplitude = globalParameters.amplitudeOfIncidentWaveReal.first+i*globalParameters.amplitudeOfIncidentWaveImag.first;

      complex<double> p_inc = 2.0 * amplitude*(exp(i*wave_number*absPoint+wave_number*attenuation - globalParameters.aTtenuation.first)) / (4*M_PI*absPoint);
      complex<double> n_dev1 = (1.0+i*wave_number*absPoint) / absPoint;
      complex<double> n_dev2 = inner_prod(normal,pOsition) / absPoint;

      complex<double > grad_n = - globalParameters.transmissionCoefficient.first*p_inc*n_dev1*n_dev2;
      //// check if normal pointing to ceneter;
      //double dot = -inner_prod(normal,cOordinate);
      //if(dot < 0) grad_n *= -1;

      vAl.resize(2);
      vAl[0] = std::real(p_inc);
      vAl[1] = std::imag(p_inc);

      return vAl;
    }

  };

  #ifdef KISS_FFT_H

  /**

    It is inverse Fourier transform evaluated at arbitrary Gauss points.

    This part shows the Neumann boundary condition of the exterior boundary
    value problem for the rigid scatterer.

    The following parameters are used in the calculation of plane incident wave
    \f[
    &f = \frac{1}{T} frequency in hertz of s^{-1} \\
    &T = \frac{1}{f} period or duration complete one cycle
     of signal in s (second) \\
    &\lambda = \frac{2 \pi}{k} = \frac{c}{f} wavelength in meter \\

    &c = \lambda f = \frac{\lambda}{T} = \frac{\omega}{k} = wavespeed (phase velocity) in m\s \\

    &k = \frac{\omeag}{c} wave number in rad \cdot m^{-1} \\

    &\omega = 2 \pi f = \frac{2 \pi}{T} angular frequency in rad/s \\

    &\delta t = T/n is the stride of the signal length between each time steps \\
    &\delta n is the stride of time step for there is n time steps. \\
    &\phi = 2 \pi f ( (c \delta t \delta n)/lambda ) is the phase \\

    \f]

    for input signal \f$ x(n) \f$ which n is the number of the data in time domain,
    through the fast forward fourier transformation (thanks to the package KISS), we can transfer
    any arbitrary signal from time (or spatial) domain into frequency domain. Since
    the Euler formulation, the combination of sinusoid functions can be expressed in
    complex exponentialform. (this related to the Euler Identity) the reuslts in frequency are
    put as boundary condition of the helmholtz problem.


    \f[
    \left. \left\{ \frac{1}{n} \mathbf{n} \cdot  (ik\mathbf{d} A_{0} e^{ik \mathbf{d} \cdot \mathbf{x} + i \phi})
    \right\}
    \f]

    where \f$\mathbf{n})\f$ is the normal vector point outward of the surface of scatterer.
      \f$\mathbf{x})\f$ is the cartesian coordinates and \f$d\f$ is the unit vector represents the
      direction of the incident wave.


    \bug Assumes that normal sf surface pointing outward.

    */
  struct IncidentWaveNeumannDFT_F2 {

    GlobalParameters &globalParameters;
    IncidentWaveNeumannDFT_F2(GlobalParameters &global_parameters):
    globalParameters(global_parameters) {}

    VectorDouble cOordinate;
    VectorDouble vAl;

    VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

      const complex< double > i( 0.0, 1.0 );

      cOordinate.resize(3);
      cOordinate[0] = x;
      cOordinate[1] = y;
      cOordinate[2] = z;

      double signal_length = globalParameters.signalLength.first;
      double signal_duration = globalParameters.signalDuration.first;
      VectorDouble &direction = globalParameters.waveDirection.first;
      VectorDouble &oscilation_direction = globalParameters.waveOscilationDirection.first;

      double time_step = globalParameters.timeStep;
      int sizeFreq = globalParameters.complexOutSize;
      int sizeTime = globalParameters.nbOfPointsInTime.first;
      boost::shared_array<kiss_fft_cpx> &complex_out = globalParameters.complexOut;

      complex<double> p_inc_frequency = 0;
      ublas::vector<complex<double > > grad(3);
      grad.clear();

      complex< double > attenuation(0,0);
      if(globalParameters.isRayleigh.first) {
        /* speed of SAW over speed of longitudinal wave */
        complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
        attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
      }

      if(globalParameters.aTtenuation.first != 0) {
        double xOriginal = - std::sqrt(std::abs(std::pow(globalParameters.rAdius.first,2.0) - std::pow(z,2.0)));
        globalParameters.aTtenuation.first *= (x - xOriginal);
      }

      //manually apply the IFFT.
      for(int f = 0;f < sizeTime && sizeFreq;f++) {

        double speed = signal_length/signal_duration;
        /* 2*M_PI/signal_length : 2*M_PI*f/signal_length*/
        complex< double > wave_number = (2*M_PI*f/signal_length) + i*globalParameters.complexWaveNumber.first; //2pi f / lambda
        double delta_t = signal_duration/sizeTime; //time step
        double distance = speed*delta_t*time_step;
        double phase= 2*M_PI*f*(distance/signal_length);
        if(globalParameters.isRadiation.first){
          p_inc_frequency = (complex_out[f].r+i*complex_out[f].i)*exp(+ i*wave_number*inner_prod(direction,cOordinate)+i*phase+wave_number*attenuation - globalParameters.aTtenuation.first);
        } else {
          p_inc_frequency = (complex_out[f].r+i*complex_out[f].i)*exp(- i*wave_number*inner_prod(direction,cOordinate)+i*phase+wave_number*attenuation - globalParameters.aTtenuation.first);
        }

        for(int ii = 0;ii<3;ii++) {
          grad[ii] += i*wave_number*oscilation_direction[ii]*p_inc_frequency;
        }

      }

      grad /= sizeTime;
      complex<double > grad_n = inner_prod(grad,normal);
      grad_n *= globalParameters.transmissionCoefficient.first;
      // cerr << "\n grad_n = \n" << grad_n << endl;
      vAl.resize(2);
      vAl[0] = std::real(grad_n);
      vAl[1] = std::imag(grad_n);

      return vAl;
    }

  };

  /** \brief Calculate incident wave scattered on soft surface with admittance
      \ingroup mofem_helmholtz_elem

    It is inverse Fourier transform evaluated at arbitrary Gauss points.

    This part shows the Dirichlet like boundary condition of the exterior boundary
    value problem for the rigid scatterer.

    \f]
    \left. \left\{ A_{0} e^{ik \mathbf{d} \cdot \mathbf{x}})
    \right\}
    \f[

      \f$\mathbf{x})\f$ is the cartesian coordinates and \f$d\f$ is the unit vector represents the
      direction of the incident wave. Further more
    */

    struct IncidentWaveDirichletDFT_F2 {

      GlobalParameters &globalParameters;
      IncidentWaveDirichletDFT_F2(GlobalParameters &global_parameters):
      globalParameters(global_parameters) {}

      VectorDouble cOordinate;
      VectorDouble vAl;

      VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

        const complex< double > i( 0.0, 1.0 );

        cOordinate.resize(3);
        cOordinate[0] = x;
        cOordinate[1] = y;
        cOordinate[2] = z;

        double signal_length = globalParameters.signalLength.first;
        double signal_duration = globalParameters.signalDuration.first;
        VectorDouble &direction = globalParameters.waveDirection.first;
        double time_step = globalParameters.timeStep;
        int sizeFreq = globalParameters.complexOutSize;
        int sizeTime = globalParameters.nbOfPointsInTime.first;
        boost::shared_array<kiss_fft_cpx> &complex_out = globalParameters.complexOut;
        complex<double> p_inc_frequency = 0;

        complex< double > attenuation(0,0);
        if(globalParameters.isRayleigh.first) {
          /* speed of SAW over speed of longitudinal wave */
          complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
          attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
        }

        if(globalParameters.aTtenuation.first != 0) {
          double xOriginal = - std::sqrt(std::abs(std::pow(globalParameters.rAdius.first,2.0) - std::pow(z,2.0)));
          globalParameters.aTtenuation.first *= (x - xOriginal);
        }

        //manually apply the IFFT.
        for(int f = 0;f < sizeTime && sizeFreq;f++) {

          double speed = signal_length/signal_duration;
          complex< double > wave_number = (2*M_PI*f/signal_length) + i*globalParameters.complexWaveNumber.first; //2pi f / lambda
          double delta_t = signal_duration/sizeTime; //time step
          double distance = speed*delta_t*time_step;
          double phase= 2*M_PI*f*(distance/signal_length);
          if(globalParameters.isRadiation.first){
            p_inc_frequency += (complex_out[f].r+i*complex_out[f].i)*exp(+ i*wave_number*inner_prod(direction,cOordinate)+i*phase+wave_number*attenuation - globalParameters.aTtenuation.first);
          } else {
            p_inc_frequency += (complex_out[f].r+i*complex_out[f].i)*exp(- i*wave_number*inner_prod(direction,cOordinate)+i*phase+wave_number*attenuation - globalParameters.aTtenuation.first);
          }

        }
        // if(!globalParameters.isRadiation.first){
        //   p_inc_frequency *= -i*globalParameters.surfaceAdmittance.first;
        // }
        p_inc_frequency /= sizeTime;
        p_inc_frequency *= globalParameters.transmissionCoefficient.first;

        vAl.resize(2);
        vAl[0] = std::real(p_inc_frequency);
        vAl[1] = std::imag(p_inc_frequency);

        return vAl;
      }

    };


    struct IncidentWaveNeumannPointSourceDFT_F2 {

      GlobalParameters &globalParameters;
      IncidentWaveNeumannPointSourceDFT_F2(GlobalParameters &global_parameters):
      globalParameters(global_parameters) {}

      VectorDouble cOordinate;
      VectorDouble pOsition;
      VectorDouble sOurce;
      VectorDouble vAl;

      VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

        const complex< double > i( 0.0, 1.0 );

        cOordinate.resize(3);
        pOsition.resize(3);
        cOordinate[0] = x;
        cOordinate[1] = y;
        cOordinate[2] = z;
        sOurce = globalParameters.sourceCoordinate.first;

        pOsition[0] = (cOordinate[0]-sOurce[0]);
        pOsition[1] = (cOordinate[1]-sOurce[1]);
        pOsition[2] = (cOordinate[2]-sOurce[2]);
        double absPoint = sqrt(pOsition[0]*pOsition[0]+pOsition[1]*pOsition[1]+pOsition[2]*pOsition[2]);
        // double normMulti = norm_2(normal) * norm_2(pOsition);

        double signal_length = globalParameters.signalLength.first;
        double signal_duration = globalParameters.signalDuration.first;
        double time_step = globalParameters.timeStep;
        int sizeFreq = globalParameters.complexOutSize;
        int sizeTime = globalParameters.nbOfPointsInTime.first;
        boost::shared_array<kiss_fft_cpx> &complex_out = globalParameters.complexOut;

        complex<double> p_inc_frequency = 0;
        complex<double > grad_n = 0;

        complex< double > attenuation(0,0);
        if(globalParameters.isRayleigh.first) {
          /* speed of SAW over speed of longitudinal wave */
          complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
          attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
        }

        if(globalParameters.aTtenuation.first != 0) {
          double xOriginal = - std::sqrt(std::abs(std::pow(globalParameters.rAdius.first,2.0) - std::pow(z,2.0)));
          globalParameters.aTtenuation.first *= (x - xOriginal);
        }

        //manually apply the IFFT.
        for(int f = 0;f < sizeTime && sizeFreq;f++) {
          double speed = signal_length/signal_duration;
          complex< double > wave_number = (2*M_PI*f/signal_length) - i*globalParameters.complexWaveNumber.first; //2pi f / lambda
          double delta_t = signal_duration/sizeTime; //time step
          double distance = speed*delta_t*time_step;
          double phase= 2*M_PI*f*(distance/signal_length);
          if(globalParameters.isRadiation.first){
            p_inc_frequency = (complex_out[f].r+i*complex_out[f].i)*(exp(+i*wave_number*absPoint+i*phase+wave_number*attenuation - globalParameters.aTtenuation.first)) / (4*M_PI*absPoint);
          } else {
            p_inc_frequency = (complex_out[f].r+i*complex_out[f].i)*(exp(-i*wave_number*absPoint+i*phase+wave_number*attenuation - globalParameters.aTtenuation.first)) / (4*M_PI*absPoint);
          }
          complex<double> n_dev1 = (1.0+i*wave_number*absPoint) / absPoint;
          complex<double> cos_angle = inner_prod(normal,pOsition) / absPoint;
          grad_n += - p_inc_frequency*n_dev1*cos_angle;
        }

        grad_n /= sizeTime;
        grad_n *= globalParameters.transmissionCoefficient.first;
        vAl.resize(2);
        vAl[0] = std::real(grad_n);
        vAl[1] = std::imag(grad_n);
        // cerr << "\n vAl = \n " << vAl << endl;
        return vAl;
      }

    };

    struct IncidentWaveDirichletPointSourceDFT_F2 {

      GlobalParameters &globalParameters;
      IncidentWaveDirichletPointSourceDFT_F2(GlobalParameters &global_parameters):
      globalParameters(global_parameters) {}

      VectorDouble cOordinate;
      VectorDouble pOsition;
      VectorDouble vAl;

      VectorDouble& operator()(double x,double y,double z,VectorDouble &normal) {

        const complex< double > i( 0.0, 1.0 );

        cOordinate.resize(3);
        pOsition.resize(3);
        cOordinate[0] = x;
        cOordinate[1] = y;
        cOordinate[2] = z;

        pOsition[0] = (cOordinate[0]-globalParameters.sourceCoordinate.first[0])*(cOordinate[0]-globalParameters.sourceCoordinate.first[0]);
        pOsition[1] = (cOordinate[1]-globalParameters.sourceCoordinate.first[1])*(cOordinate[1]-globalParameters.sourceCoordinate.first[1]);
        pOsition[2] = (cOordinate[2]-globalParameters.sourceCoordinate.first[2])*(cOordinate[2]-globalParameters.sourceCoordinate.first[2]);
        double absPoint = sqrt(pOsition[0]+pOsition[1]+pOsition[2]);

        double signal_length = globalParameters.signalLength.first;
        double signal_duration = globalParameters.signalDuration.first;
        double time_step = globalParameters.timeStep;
        int sizeFreq = globalParameters.complexOutSize;
        int sizeTime = globalParameters.nbOfPointsInTime.first;
        boost::shared_array<kiss_fft_cpx> &complex_out = globalParameters.complexOut;
        complex<double> p_inc_frequency = 0;

        complex< double > attenuation(0,0);
        if(globalParameters.isRayleigh.first) {
          /* speed of SAW over speed of longitudinal wave */
          complex< double > vs_over_v1 = (globalParameters.signalLength.first*globalParameters.fRequency.first) / globalParameters.vElocity.first;
          attenuation = -sqrt(1.0 - pow(vs_over_v1,2.0))*abs(y);
        }

        if(globalParameters.aTtenuation.first != 0) {
          double xOriginal = - std::sqrt(std::abs(std::pow(globalParameters.rAdius.first,2.0) - std::pow(z,2.0)));
          globalParameters.aTtenuation.first *= (x - xOriginal);
        }

        //manually apply the IFFT.
        for(int f = 0;f < sizeTime && sizeFreq;f++) {
          double speed = signal_length/signal_duration;
          complex< double > wave_number = (2*M_PI*f/signal_length) + i*globalParameters.complexWaveNumber.first; //2pi f / lambda
          double delta_t = signal_duration/sizeTime; //time step
          double distance = speed*delta_t*time_step;
          double phase= 2*M_PI*f*(distance/signal_length);
          if(globalParameters.isRadiation.first){
            p_inc_frequency += (complex_out[f].r+i*complex_out[f].i)*(exp(+ i*wave_number*absPoint+i*phase+wave_number*attenuation - globalParameters.aTtenuation.first)) / (4*M_PI*absPoint);
          } else {
            p_inc_frequency += (complex_out[f].r+i*complex_out[f].i)*(exp(- i*wave_number*absPoint+i*phase+wave_number*attenuation - globalParameters.aTtenuation.first)) / (4*M_PI*absPoint);
          }

        }
        // if(!globalParameters.isRadiation.first){
        //   p_inc_frequency *= -i*globalParameters.surfaceAdmittance.first;
        // }

        p_inc_frequency /= sizeTime;
        p_inc_frequency *= globalParameters.transmissionCoefficient.first;

        vAl.resize(2);
        vAl[0] = std::real(p_inc_frequency);
        vAl[1] = std::imag(p_inc_frequency);

        return vAl;
      }

    };


  #endif // KISS_FFT_H

  /** \brief Rhs vector for Helmholtz operator
    \ingroup mofem_helmholtz_elem

    Operator is build using two template functions, see equations below.
    Depending on returning values of those functions user can apply, Nuemman, Mix or
    any variant of above conditions.

    \f[
    \left.\left\{ \frac{\partial p}{\partial n} +
      (\textrm{re}[\sigma]+i*\textrm{im}[\sigma] + \textrm{re}[F1]+i\textrm{im}[F1]) p
      + \textrm{re}[F2]+i\textrm{im}[F2] \right\}\right|_\Gamma = 0
    \f]
    where \f$F1(x,y,z,\mathbf{n})\f$ and \f$F2(x,y,z,\mathbf{n})\f$ are template function evaluators given by user.

    \f[
    F_i =
      \int_{\Omega^e} \frac{\partial N_i}{\partial x_j}\frac{\partial p}{\partial x_j} \textrm{d}V +
      \int_{\Gamma^e} N_i \left\{
	(\textrm{re}[\sigma]+i*\textrm{im}[\sigma] + \textrm{re}[F1]+i\textrm{im}[F1]) p + \textrm{re}[F2]+i\textrm{im}[F2]
      \right\} \textrm{d}V
    \f]

    Note: that first term is volume interval and is taken into account in
    volume operator. Expanding surface integral and taking into account that
    admittance and shape function are complex numbers, we get:
    \f[
    \textrm{re}[F^\Gamma_i] = \int_{\Gamma^e} N_i
      \left\{
      (\textrm{re}[\sigma]+\textrm{re}[F1])\textrm{re}[p] - (\textrm{im}[\sigma]+\textrm{im}[F1])\textrm{im}[p] +  \textrm{re}[F2])
      \right\}
    \textrm{d}V
    \f]
    and
    \f[
    \textrm{im}[F^\Gamma_i] = \int_{\Gamma^e} N_i
      \left\{
      (\textrm{re}[\sigma]+\textrm{re}[F1])\textrm{im}[p] + (\textrm{im}[\sigma]+\textrm{im}[F1])\textrm{re}[p] +  \textrm{im}[F2])
      \right\}
    \textrm{d}V
    \f]


  */
  template<typename FUNEVAL1,typename FUNEVAL2>
  struct OpHelmholtzMixBCRhs: public FaceElementForcesAndSourcesCore::UserDataOperator {

    SurfaceData &dAta;
    CommonData &commonData;
    Vec F;

    const string rePressure;
    const string imPressure;

    boost::shared_ptr<FUNEVAL1> functionEvaluator1;
    boost::shared_ptr<FUNEVAL2> functionEvaluator2;

    OpHelmholtzMixBCRhs(
      const string re_field_name,const string im_field_name,
      Vec _F,SurfaceData &data,CommonData &common_data,
      boost::shared_ptr<FUNEVAL1> function_evaluator1,boost::shared_ptr<FUNEVAL2> function_evaluator2
    ):
    FaceElementForcesAndSourcesCore::UserDataOperator(re_field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
    dAta(data),commonData(common_data),F(_F),
    rePressure(re_field_name),
    imPressure(im_field_name),
    functionEvaluator1(function_evaluator1),
    functionEvaluator2(function_evaluator2) {}

    VectorDouble reNf,imNf;

    double reResidual,imResidual;
    virtual PetscErrorCode calculateResidualRe(int gg,VectorDouble &f1,VectorDouble &f2) {
      PetscFunctionBegin;

      double re_pressure = commonData.pressureAtGaussPts[rePressure](gg);
      double im_pressure = commonData.pressureAtGaussPts[imPressure](gg);

      reResidual = (dAta.aDmittance_real+f1[0])*re_pressure - (dAta.aDmittance_imag+f1[1])*im_pressure + f2[0];
      imResidual = (dAta.aDmittance_real+f1[0])*im_pressure + (dAta.aDmittance_imag+f1[1])*re_pressure + f2[1];

      PetscFunctionReturn(0);
    }

    VectorDouble nOrmal;

    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;

      try {

        PetscErrorCode ierr;

        int nb_row_dofs = data.getIndices().size();
        if(!nb_row_dofs) {
          PetscFunctionReturn(0);
        }

        if(dAta.tRis.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tRis.end()) {
          PetscFunctionReturn(0);
        }

        reNf.resize(nb_row_dofs,false);
        reNf.clear();
        imNf.resize(nb_row_dofs,false);
        imNf.clear();
        nOrmal.resize(3);

        for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

          double area = getArea();
          if(getNormalsAtGaussPt().size1()) {

            noalias(nOrmal) = getNormalsAtGaussPt(gg);
            area = ublas::norm_2(nOrmal)*0.5;
            nOrmal /= 2*area;

          }
          double val = area*getGaussPts()(2,gg);

          double x,y,z;
          if(commonData.hoCoordsTri.size1()) {
            x = commonData.hoCoordsTri(gg,0);
            y = commonData.hoCoordsTri(gg,1);
            z = commonData.hoCoordsTri(gg,2);
          } else {
            x = getCoordsAtGaussPts()(gg,0);
            y = getCoordsAtGaussPts()(gg,1);
            z = getCoordsAtGaussPts()(gg,2);
            if(gg == 0) {
              noalias(nOrmal) = getNormal();
              nOrmal /= 2*area;
            }
          }

          try {
            VectorDouble& f1 = (*functionEvaluator1)(x,y,z,nOrmal);
            VectorDouble& f2 = (*functionEvaluator2)(x,y,z,nOrmal);

            ierr = calculateResidualRe(gg,f1,f2); CHKERRQ(ierr);

            noalias(reNf) += val*(reResidual*data.getN(gg,nb_row_dofs));
            noalias(imNf) += val*(imResidual*data.getN(gg,nb_row_dofs));

          } catch (const std::exception& ex) {
            ostringstream ss;
            ss << "throw in method: " << ex.what() << endl;
            SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
          }

        }

        ierr = VecSetValues(F,
          data.getIndices().size(),&
          data.getIndices()[0],
          &reNf[0],ADD_VALUES
        ); CHKERRQ(ierr);
        ierr = VecSetValues(F,
          (commonData.imIndices[type][side]).size(),
          &(commonData.imIndices[type][side])[0],
          &imNf[0],ADD_VALUES
        ); CHKERRQ(ierr);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  /** \brief Mix boundary conditions
    * \ingroup mofem_helmholtz_elem

  \f[
  C_{ij}= \int_{\Gamma^e} (\sigma+F1) N_i N_j \textrm{d}S
  \f]

  Note that off diagonal imaginary part is assembled on the fly. C matrix is
  transposed and assembled with negative sign.

    */
  template<typename FUNEVAL1>
  struct OpHelmholtzMixBCLhs:public FaceElementForcesAndSourcesCore::UserDataOperator {

    SurfaceData &dAta;
    CommonData &commonData;
    const string rePressure;
    const string imPressure;

    boost::shared_ptr<FUNEVAL1> functionEvaluator1;

    Mat A;
    OpHelmholtzMixBCLhs(
      const string re_field_name,const string im_field_name,
      Mat _A,SurfaceData &data,CommonData &common_data,
      boost::shared_ptr<FUNEVAL1> function_evaluator1
    ):
    FaceElementForcesAndSourcesCore::UserDataOperator(
      re_field_name,re_field_name,ForcesAndSurcesCore::UserDataOperator::OPROWCOL
    ), // Note: operator is real-real
    dAta(data),
    commonData(common_data),
    rePressure(re_field_name),
    imPressure(im_field_name),
    functionEvaluator1(function_evaluator1),A(_A) {

      sYmm = false; /// This operator is not symmetric

    }

    MatrixDouble K,K0,K1,reF1K,imF1K;
    VectorDouble nOrmal;

    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data) {
      PetscFunctionBegin;

      try {

        int nb_rows = row_data.getIndices().size();
        int nb_cols = col_data.getIndices().size();
        if(nb_rows==0) PetscFunctionReturn(0);
        if(nb_cols==0) PetscFunctionReturn(0);

        if(dAta.tRis.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tRis.end()) {
          PetscFunctionReturn(0);
        }

        K.resize(nb_rows,nb_cols,false);
        K.clear();
        reF1K.resize(nb_rows,nb_cols,false);
        reF1K.clear();
        imF1K.resize(nb_rows,nb_cols,false);
        imF1K.clear();

        K0.resize(nb_rows,nb_cols,false);
        nOrmal.resize(3);

        // cerr << "\n T number of gauss points : = \n " << row_data.getN().size1() << endl;
        for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {

          double area = getArea();
          if(getNormalsAtGaussPt().size1()) {
            noalias(nOrmal) = getNormalsAtGaussPt(gg);
            area = ublas::norm_2(nOrmal)*0.5;
          }
          double val = area*getGaussPts()(2,gg);

          double x,y,z;
          if(commonData.hoCoordsTri.size1()) {
            x = commonData.hoCoordsTri(gg,0);
            y = commonData.hoCoordsTri(gg,1);
            z = commonData.hoCoordsTri(gg,2);
          } else {
            x = getCoordsAtGaussPts()(gg,0);
            y = getCoordsAtGaussPts()(gg,1);
            z = getCoordsAtGaussPts()(gg,2);
            if(gg == 0) {
              noalias(nOrmal) = getNormal();
            }
          }

          noalias(K0) = outer_prod(row_data.getN(gg,nb_rows),col_data.getN(gg,nb_cols));
          noalias(K) += val*K0;

          VectorDouble& f1 = (*functionEvaluator1)(x,y,z,nOrmal);
          if(f1[0]!=0) {
            noalias(reF1K) += val*f1[0]*K0;
          }
          if(f1[1]!=0) {
            noalias(imF1K) += val*f1[1]*K0;
          }

        }

        if(row_data.getIndices().size()!=(commonData.imIndices[row_type][row_side]).size()) {
          SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data inconsistency");
        }

        if(col_data.getIndices().size()!=(commonData.imIndices[col_type][col_side]).size()) {
          SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data inconsistency");
        }

        K1.resize(nb_rows,nb_cols,false);

        PetscErrorCode ierr;

        // real-real
        noalias(K1) = dAta.aDmittance_real*K+reF1K;
        ierr = MatSetValues(
          A,
          nb_rows,&row_data.getIndices()[0],
          nb_cols,&col_data.getIndices()[0],
          &K1(0,0),ADD_VALUES); CHKERRQ(ierr);

        // imag-imag
        noalias(K1) = dAta.aDmittance_real*K+reF1K;
        ierr = MatSetValues(
          A,
          nb_rows,&(commonData.imIndices[row_type][row_side])[0],
          nb_cols,&(commonData.imIndices[col_type][col_side])[0],
          &K1(0,0),ADD_VALUES); CHKERRQ(ierr);

        // real-imag
        noalias(K1) = -(dAta.aDmittance_imag*K+imF1K);
        ierr = MatSetValues(
          A,
          nb_rows,&row_data.getIndices()[0],
          nb_cols,&(commonData.imIndices[col_type][col_side])[0],
          &K1(0,0),ADD_VALUES); CHKERRQ(ierr);

        // imag-real
        noalias(K1) = dAta.aDmittance_imag*K+imF1K;
        ierr = MatSetValues(
          A,
          nb_rows,&(commonData.imIndices[row_type][row_side])[0],
          nb_cols,&col_data.getIndices()[0],
          &K1(0,0),ADD_VALUES); CHKERRQ(ierr);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  /* \brief Infinite Helmholtz Element

  Implementation based on Demkowicz Book:
  Computing with Adaptive HP-Elements Volume 2
  Page: 225

  Element is integrated on outer mesh surface \f$S_a\f$ up to the infinity where
  Sommerfeld radiation condition is applied. Integration is done which-in
  element.

  Moreover approximations functions are scaled in such a way, that despite they
  are different outside discretized volume, on outer mesh surface \f$S_a\f$,
  those functions looks as a shape function which we use to approximate filed in
  the interior.

  The trick is here that this is Bubnov-Galerkin formulation, so we using the
  same functions to approximate pressures on surface \f$S_a\f$ like we use in
  standard FE formulation.

  The Cartesian position in space is parametrised by surface \f$S_a\f$
  parametrisation \f$(\xi_1,\xi_2\f$), which in practice is local approximation
  on triangle on surface and radial parametrisation, as follows
  \f[
  x(\xi_1,\xi_2,\xi_3) = \xi_3^{-1}x_a(\xi_1,\xi_2),\quad a/R < \xi_3 < 1
  \f]

  Functions in the infinite element domain are constructed by tensor product of
  one-divisional radial shape functions of coordinate \f$\xi_3\f$ and standard FE
  hierarchical shape functions.
  \f[
  P(\xi_1,\xi_2,\xi_3) = \sum_{kl} P_{kl} \psi_k(\xi_3) e_l(\xi_1,\xi_2)
  \f]
  where \f$e(\xi_1,\xi_2)\f$ are standard shape functions on element face and
  \f$\psi(\xi_3)\f$ is a radial shape function. The radial shape functions are
  generated as follows
  \f[
  \psi_j =
  \left\{
    \begin{array}{cc}
      1, & j = 0 \\
      P_j(2\xi_3-1), & j > 0
    \end{array}
  \right.
  \f]
  Note that function for \f$j>0\f$ are bubble functions so there are not
  assembled into global coordinate system but are condensed in the finite element
  procedure and then element is assembled. This makes a trick mentioned above,
  that to global system are assembled additional terms but size remains
  unchanged, moreover is symmetric.

  To make all above true, pleas note that
  \f[
  p=\xi_3 e^{-ik(\xi_3^{-1}-1)}P
  \f]
  where for \f$\xi = 1\f$, \f$p(\xi_1,\xi_2) = P(\xi_1,\xi_2,1)\f$

  Infinite part,
  \f[
  b_\infty(q,p) =
    \frac{1}{a} \int_{S_a}
      \left\{
      \int_0^1
	\frac{\partial P}{\partial \xi_3}
	\left(
	\xi_3^2\frac{\partial \overline{Q}}{\partial \xi_3} - i2ka\overline{Q}
	\right)
      \textrm{d}\xi_3
      +(1+ika)P\overline{Q}|_{\xi_3=1}
    \right\}
  \textrm{d}S_a
  +
  a\int_{S_a}
    \int_0^1
      \nabla_{S_a} P\cdot
      \nabla_{S_a} \overline{Q}
    \textrm{d}xi_3
  \textrm{d}S_a
  \f]


  */

  struct InfiniteHelmholtz {

    /*MatrixDouble gaussPointsInRadialDirection;
    MatrixDouble radialShapeFunctions;
    MatrixDouble direvativesOfRadialShapeFunctions;

    MatrixDouble elementShapeFunctions;
    ublas::matrxi<double> direvativeOfElementShapeFunctions;*/

    /** \brief Generate approximation space for infinite element

    It is ordered in such a way, that first indices are loop over surface
    degrees of freedom then radial degrees of freedom. So that
    n_surface_dofs*n_radial_dofs, wher first n_surface_dofs are for surface dofs
    and constant radial shape function. Remaining dofs then could be statically
    condensed.

    */
    //PetscErrorCode generateBase(
      //VectorAdaptor &N,MatrixAdaptor &diffN) {
      //PetscFunctionBegin;

      /*int nb_gauss_points_on_surface = N.size1();
      int nb_gauss_pts_radial = radialShapeFunctions.size1();
      int nb_surface_dofs = N.size2();
      int nb_radial_dofs = radialShapeFunctions.size2();

      elementShapeFunctions.resize(
	nb_gauss_points_on_surface*nb_gauss_pts_radial,
	nb_surface_dofs*nb_radial_dofs);

      for(int ii = 0;ii<nb_radial_dofs;ii++) {
	for(int jj = 0;jj<nb_surface_dofs;jj++) {

	  for(int gg_s = 0;gg_s<nb_gauss_points_on_surface;gg++) {
	    for(int gg_r = 0;gg_r<nb_gauss_pts_radial;gg_r++) {

	    double direvative_shf_in_radial_direction =
	      direvativesOfRadialShapeFunctions(gg_s,ii)*N(gg_s,jj);

	      elementShapeFunctions(
		gg_r*nb_gauss_points_on_surface+gg_s,
		ii*nb_surface_dofs+jj) =
		radialShapeFunctions(gg_s,ii)*N(gg_r,jj);

	      direvativeOfElementShapeFunctions(
		gg_r*nb_gauss_points_on_surface+gg_s,
		3*ii*nb_surface_dofs+3*jj+0) =
		radialShapeFunctions(gg_s,ii)*diffN(gg_r,3*jj+0);
	      direvativeOfElementShapeFunctions(
		gg_r*nb_gauss_points_on_surface+gg_s,
		3*ii*nb_surface_dofs+3*jj+1) =
		radialShapeFunctions(gg_s,ii)*diffN(gg_r,3*jj+1);
	      direvativeOfElementShapeFunctions(
		gg_r*nb_gauss_points_on_surface+gg_s,
		3*ii*nb_surface_dofs+3*jj+3) =
		direvativesOfRadialShapeFunctions(gg_s,ii)*N(gg_r,jj);

	    }
	  }

	}
      }*/

      //PetscFunctionReturn(0);
    //}

  };

  /** \brief Add Helmholtz elements to problem
    * \ingroup mofem_helmholtz_elem
    *
    * It get data from block set and define element in moab
    *w
    * \param problem name
    * \param field name
    * \param name of mesh nodal positions (if not defined nodal coordinates are used)
    */
  PetscErrorCode addHelmholtzElements(
    const string re_field_name,const string im_field_name,
    const string mesh_nodals_positions = "MESH_NODE_POSITIONS",
    const string pressure_field = "P") {
    PetscFunctionBegin;

    PetscErrorCode ierr;
    ErrorCode rval;

    if(mField.check_field(pressure_field)) {

      ierr = mField.add_finite_element("PRESSURE_FE",MF_ZERO); CHKERRQ(ierr );
      ierr = mField.modify_finite_element_add_field_row("PRESSURE_FE",pressure_field); CHKERRQ(ierr);
      ierr = mField.modify_finite_element_add_field_col("PRESSURE_FE",pressure_field); CHKERRQ(ierr);
      ierr = mField.modify_finite_element_add_field_data("PRESSURE_FE",pressure_field); CHKERRQ(ierr);

    }

    ierr = mField.add_finite_element("HELMHOLTZ_RERE_FE",MF_ZERO); CHKERRQ(ierr );
    ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_RERE_FE",re_field_name); CHKERRQ(ierr);
    // ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_RERE_FE",im_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_RERE_FE",re_field_name); CHKERRQ(ierr);
    // ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_RERE_FE",im_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_RERE_FE",re_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_RERE_FE",im_field_name); CHKERRQ(ierr);

    ierr = mField.add_finite_element("HELMHOLTZ_IMIM_FE",MF_ZERO); CHKERRQ(ierr );
    ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_IMIM_FE",im_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_IMIM_FE",im_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_IMIM_FE",im_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_IMIM_FE",re_field_name); CHKERRQ(ierr);

    ierr = mField.add_finite_element("HELMHOLTZ_REIM_FE",MF_ZERO); CHKERRQ(ierr );
    ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_REIM_FE",re_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_row("HELMHOLTZ_REIM_FE",im_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_REIM_FE",re_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_col("HELMHOLTZ_REIM_FE",im_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_REIM_FE",re_field_name); CHKERRQ(ierr);
    ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_REIM_FE",im_field_name); CHKERRQ(ierr);

    if(mField.check_field(mesh_nodals_positions)) {

      ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_RERE_FE",mesh_nodals_positions); CHKERRQ(ierr);
      ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_IMIM_FE",mesh_nodals_positions); CHKERRQ(ierr);
      ierr = mField.modify_finite_element_add_field_data("HELMHOLTZ_REIM_FE",mesh_nodals_positions); CHKERRQ(ierr);

      if(mField.check_field(pressure_field)) {

        ierr = mField.modify_finite_element_add_field_data("PRESSURE_FE",mesh_nodals_positions); CHKERRQ(ierr);

      }

    }

    for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {

      if(it->getName().compare(0,13,"MAT_HELMHOLTZ") == 0) {

        const complex< double > i( 0.0, 1.0 );
        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTET,volumeData[it->getMeshsetId()].tEts,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(volumeData[it->getMeshsetId()].tEts,MBTET,"HELMHOLTZ_RERE_FE"); CHKERRQ(ierr);
        ierr = mField.add_ents_to_finite_element_by_type(volumeData[it->getMeshsetId()].tEts,MBTET,"HELMHOLTZ_IMIM_FE"); CHKERRQ(ierr);
        /* FIXME Replace 0 by attenuation coefficient. */
        volumeData[it->getMeshsetId()].waveNumber = globalParameters.waveNumber.first;

        if(mField.check_field(pressure_field)) {
          ierr = mField.add_ents_to_finite_element_by_type(volumeData[it->getMeshsetId()].tEts,MBTET,"PRESSURE_FE"); CHKERRQ(ierr);
        }

      }

    }

    for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {

      if(it->getName().compare(0,23,"HARD_INCIDENT_WAVE_BC") == 0) {

        surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_real = 0;
        surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_imag = 0;
        globalParameters.isIncidentWave.first = PETSC_TRUE;

        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,surfaceIncidentWaveBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(surfaceIncidentWaveBcData[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

      if(it->getName().compare(0,25,"HARD_INCIDENT_WAVE_2BC") == 0) {

        surfaceIncidentWaveBc2Data[it->getMeshsetId()].aDmittance_real = 0;
        surfaceIncidentWaveBc2Data[it->getMeshsetId()].aDmittance_imag = 0;
        globalParameters.isIncidentWave.first = PETSC_TRUE;

        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,surfaceIncidentWaveBc2Data[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(surfaceIncidentWaveBc2Data[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

      cerr << it->getName() << endl;

      if(it->getName().compare(0,22,"MIX_INCIDENT_WAVE_BC") == 0) {

        // Range tris;
        // rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,tris,true); CHKERRQ_MOAB(rval);
        // if(tris.size()==0) continue;
        cerr << "\n MIX_INCIDENT_WAVE_BC \n " << endl;
        //get block attributes
        vector<double> attributes;
        ierr = it->getAttributes(attributes); CHKERRQ(ierr);

        /* FIXME problem with paratitioned mesh parallel computation. */
        if(attributes.size()<1) {
          //SETERRQ1(PETSC_COMM_SELF,1,"first block attribute should define surface admittance (atribute size = %d)",attributes.size());
          // attributes.resize(2,0);
          surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_real = 0;
          surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_imag = globalParameters.materialCoefficient1.first;
          // cerr << "\n surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_imag = \n" << surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_imag << endl;
        } else {
          surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_real = attributes[0];
          surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_imag = attributes[1];
        }

        globalParameters.isIncidentWave.first = PETSC_TRUE;

        if(surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_imag == 0 && surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_real == 0) {

          surfaceIncidentWaveBcData[it->getMeshsetId()].aDmittance_imag = globalParameters.materialCoefficient1.first;

        }

        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,surfaceIncidentWaveBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(surfaceIncidentWaveBcData[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

      if(it->getName().compare(0,22,"MIX_INCIDENT_WAVE_2BC") == 0) {

        vector<double> attributes;
        ierr = it->getAttributes(attributes); CHKERRQ(ierr);

        if(attributes.size()<1) {
          surfaceIncidentWaveBc2Data[it->getMeshsetId()].aDmittance_real = 0;
          surfaceIncidentWaveBc2Data[it->getMeshsetId()].aDmittance_imag = globalParameters.materialCoefficient2.first;
        } else {
          surfaceIncidentWaveBc2Data[it->getMeshsetId()].aDmittance_real = attributes[0];
          surfaceIncidentWaveBc2Data[it->getMeshsetId()].aDmittance_imag = attributes[1];
        }

        globalParameters.isIncidentWave.first = PETSC_TRUE;
        // cerr << "\n surfaceIncidentWaveBc2Data[it->getMeshsetId()].aDmittance_imag = \n" << surfaceIncidentWaveBc2Data[it->getMeshsetId()].aDmittance_imag << endl;
        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,surfaceIncidentWaveBc2Data[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(surfaceIncidentWaveBc2Data[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

      if(it->getName().compare(0,9,"HARD_BC") == 0) {

        surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_real = 0;
        surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_imag = 0;
        globalParameters.isHomogenous.first = PETSC_TRUE;

        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,surfaceHomogeneousBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(surfaceHomogeneousBcData[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

      if(it->getName().compare(0,9,"ABSORB_BC") == 0) {

        //get block attributes
        vector<double> attributes;
        ierr = it->getAttributes(attributes); CHKERRQ(ierr);
        // if(attributes.size()<1) {
        //   SETERRQ1(PETSC_COMM_SELF,1,"first block attribute should define surface admittance",attributes.size());
        // }
        if(attributes.size()<1) {
          surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_real = 0;
          surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_imag = globalParameters.materialCoefficient1.first;
        } else {
          surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_real = attributes[0];
          /* wave reflected based on -\sigma */
          surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_imag = attributes[1];
        }

        globalParameters.isHomogenous.first = PETSC_TRUE;

        if(surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_imag == 0 && surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_real == 0) {
          surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_real = 0;
          surfaceHomogeneousBcData[it->getMeshsetId()].aDmittance_imag = globalParameters.materialCoefficient1.first;
        }

        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,surfaceHomogeneousBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(surfaceHomogeneousBcData[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

      if(it->getName().compare(0,10,"ABSORB_2BC") == 0) {

        //get block attributes
        vector<double> attributes;
        ierr = it->getAttributes(attributes); CHKERRQ(ierr);

        if(attributes.size()<1) {
          surfaceHomogeneousBc2Data[it->getMeshsetId()].aDmittance_real = 0;
          surfaceHomogeneousBc2Data[it->getMeshsetId()].aDmittance_imag = globalParameters.materialCoefficient2.first;
        } else {
          surfaceHomogeneousBc2Data[it->getMeshsetId()].aDmittance_real = attributes[0];
          surfaceHomogeneousBc2Data[it->getMeshsetId()].aDmittance_imag = attributes[1];
        }

        globalParameters.isHomogenous.first = PETSC_TRUE;

        if(surfaceHomogeneousBc2Data[it->getMeshsetId()].aDmittance_imag == 0 && surfaceHomogeneousBc2Data[it->getMeshsetId()].aDmittance_real == 0) {
          surfaceHomogeneousBc2Data[it->getMeshsetId()].aDmittance_real = 0;
          surfaceHomogeneousBc2Data[it->getMeshsetId()].aDmittance_imag = globalParameters.materialCoefficient2.first;
        }

        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,surfaceHomogeneousBc2Data[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(surfaceHomogeneousBc2Data[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

      if(it->getName().compare(0,13,"SOMMERFELD_BC") == 0) {

        sommerfeldBcData[it->getMeshsetId()].aDmittance_real = 0;

        sommerfeldBcData[it->getMeshsetId()].aDmittance_imag = -globalParameters.waveNumber.first;


        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,sommerfeldBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(sommerfeldBcData[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

      if(it->getName().compare(0,17,"BAYLISS_TURKEL_BC") == 0) {

        baylissTurkelBcData[it->getMeshsetId()].aDmittance_real = 0;
        baylissTurkelBcData[it->getMeshsetId()].aDmittance_imag = -globalParameters.waveNumber.first;

        rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,baylissTurkelBcData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
        ierr = mField.add_ents_to_finite_element_by_type(baylissTurkelBcData[it->getMeshsetId()].tRis,MBTRI,"HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

      }

    }

    PetscFunctionReturn(0);
  }

  PetscErrorCode setBCForPolyChromatic(
    Mat A,Vec F,
    const string re_field_name,
    const string im_field_name,
    const string mesh_nodals_positions
  ) {

    PetscFunctionBegin;
    PetscErrorCode ierr;

    map<int,SurfaceData>::iterator miit = surfaceHomogeneousBcData.begin();

    // why put miit here works
    if(globalParameters.isHomogenous.first) {
      // but here does not work.
      for(;miit!=surfaceHomogeneousBcData.end();miit++) {

        ierr = setBCForHomoneneous(A,F,re_field_name,im_field_name,mesh_nodals_positions,miit); CHKERRQ(ierr);

      }
      miit = surfaceHomogeneousBc2Data.begin();
      for(;miit!=surfaceHomogeneousBc2Data.end();miit++) {

        ierr = setBCForHomoneneous(A,F,re_field_name,im_field_name,mesh_nodals_positions,miit); CHKERRQ(ierr);

      }
      // Poly homo mixed boudnary conditions
    }

    if(globalParameters.isIncidentWave.first) {

      miit = surfaceIncidentWaveBcData.begin();

      for(;miit!=surfaceIncidentWaveBcData.end();miit++) {
        ierr = setBCForPolyIncidentWave(A,F,re_field_name,im_field_name,mesh_nodals_positions,miit); CHKERRQ(ierr);
        // Poly inhomo mixed boudnary conditions
      }

      miit = surfaceIncidentWaveBc2Data.begin();
      globalParameters.isRadiation.first = PETSC_TRUE;
      for(;miit!=surfaceIncidentWaveBc2Data.end();miit++) {
        ierr = setBCForPolyIncidentWave(A,F,re_field_name,im_field_name,mesh_nodals_positions,miit); CHKERRQ(ierr);
      }

    }

  }

  PetscErrorCode setBCForMonoChromatic(
    Mat A,Vec F,
    const string re_field_name,
    const string im_field_name,
    const string mesh_nodals_positions
  ) {

    PetscFunctionBegin;
    PetscErrorCode ierr;

    map<int,SurfaceData>::iterator miit = surfaceHomogeneousBcData.begin();

    if(globalParameters.isHomogenous.first) {

      for(;miit!=surfaceHomogeneousBcData.end();miit++) {
        ierr = setBCForHomoneneous(A,F,re_field_name,im_field_name,mesh_nodals_positions,miit); CHKERRQ(ierr);
      }

      miit = surfaceHomogeneousBc2Data.begin();
      for(;miit!=surfaceHomogeneousBc2Data.end();miit++) {
        ierr = setBCForHomoneneous(A,F,re_field_name,im_field_name,mesh_nodals_positions,miit); CHKERRQ(ierr);
      }

    }

    if(globalParameters.isIncidentWave.first) {

      miit = surfaceIncidentWaveBcData.begin();
      for(;miit!=surfaceIncidentWaveBcData.end();miit++) {
        ierr = setBCForMonoIncidentWave(A,F,re_field_name,im_field_name,mesh_nodals_positions,miit); CHKERRQ(ierr);
      }

      miit = surfaceIncidentWaveBc2Data.begin();
      globalParameters.isRadiation.first = PETSC_TRUE;
      for(;miit!=surfaceIncidentWaveBc2Data.end();miit++) {
        ierr = setBCForMonoIncidentWave(A,F,re_field_name,im_field_name,mesh_nodals_positions,miit); CHKERRQ(ierr);
      }

    }



    PetscFunctionReturn(0);

  }


  PetscErrorCode setBCForHomoneneous(
    Mat A,Vec F,
    const string re_field_name,
    const string im_field_name,
    const string mesh_nodals_positions,
    map<int,SurfaceData>::iterator miit
  ) {

    PetscFunctionBegin;
    boost::shared_ptr<ZeroFunVal> zero_function = boost::shared_ptr<ZeroFunVal>(new ZeroFunVal());

    if(miit->second.aDmittance_real!=0 || miit->second.aDmittance_imag!=0) {

      feLhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHelmholtzMixBCLhs<ZeroFunVal>(re_field_name,im_field_name,A,miit->second,commonData, zero_function)
      );

      feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHelmholtzMixBCRhs<ZeroFunVal,ZeroFunVal>(
          re_field_name,im_field_name,F,miit->second,commonData,
          zero_function,zero_function
        )
      );

    } else {

      feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHelmholtzMixBCRhs<ZeroFunVal,ZeroFunVal>(
          re_field_name,im_field_name,F,miit->second,commonData,
          zero_function,zero_function
        )
      );

    }

    PetscFunctionReturn(0);

  }

  PetscErrorCode setBCForMonoIncidentWave(
    Mat A,Vec F,
    const string re_field_name,
    const string im_field_name,
    const string mesh_nodals_positions,
    map<int,SurfaceData>::iterator miit
  ) {

    PetscFunctionBegin;

    boost::shared_ptr<ZeroFunVal> zero_function = boost::shared_ptr<ZeroFunVal>(new ZeroFunVal());

    if(miit->second.aDmittance_imag!=0 || miit->second.aDmittance_real!=0) {
        feLhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
          new OpHelmholtzMixBCLhs<ZeroFunVal>(re_field_name,im_field_name,A,miit->second,commonData, zero_function)
        );

        if(globalParameters.isDirichlet.first) {

          if(globalParameters.sourceCoordinate.second) {
            boost::shared_ptr<IncidentWaveDirichletPointSource> incident_wave_dirichlet_bc;
            incident_wave_dirichlet_bc = boost::shared_ptr<IncidentWaveDirichletPointSource>(new IncidentWaveDirichletPointSource(globalParameters));
            // assembled to the right hand vector
            feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
              new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveDirichletPointSource>(
                re_field_name,im_field_name,F,miit->second,commonData,
                zero_function,incident_wave_dirichlet_bc
              )
            );
          } else {
            boost::shared_ptr<IncidentWaveDirichletF2> incident_wave_dirichlet_bc;
            incident_wave_dirichlet_bc = boost::shared_ptr<IncidentWaveDirichletF2>(new IncidentWaveDirichletF2(globalParameters));
            // assembled to the right hand vector
            feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
              new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveDirichletF2>(
                re_field_name,im_field_name,F,miit->second,commonData,
                zero_function,incident_wave_dirichlet_bc
              )
            );
          }
        } else {

          if(globalParameters.sourceCoordinate.second) {
            boost::shared_ptr<IncidentWaveNeumannPointSource> incident_wave_dirichlet_bc;
            incident_wave_dirichlet_bc = boost::shared_ptr<IncidentWaveNeumannPointSource>(new IncidentWaveNeumannPointSource(globalParameters));
            // assembled to the right hand vector
            feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
              new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveNeumannPointSource>(
                re_field_name,im_field_name,F,miit->second,commonData,
                zero_function,incident_wave_dirichlet_bc
              )
            );
          } else {
            boost::shared_ptr<IncidentWaveNeumannF2> incident_wave_dirichlet_bc;
            incident_wave_dirichlet_bc = boost::shared_ptr<IncidentWaveNeumannF2>(new IncidentWaveNeumannF2(globalParameters));
            // assembled to the right hand vector
            feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
              new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveNeumannF2>(
                re_field_name,im_field_name,F,miit->second,commonData,
                zero_function,incident_wave_dirichlet_bc
              )
            );
          }

        }

    } else {

      if(globalParameters.sourceCoordinate.second) {
        boost::shared_ptr<IncidentWaveNeumannPointSource> incident_wave_neumann_bc;
        incident_wave_neumann_bc = boost::shared_ptr<IncidentWaveNeumannPointSource>(new IncidentWaveNeumannPointSource(globalParameters));
        // assembled to the right hand vector
        feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
          new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveNeumannPointSource>(
            re_field_name,im_field_name,F,miit->second,commonData,
            zero_function,incident_wave_neumann_bc
          )
        );
      } else {
        boost::shared_ptr<IncidentWaveNeumannF2> incident_wave_neumann_bc;
        incident_wave_neumann_bc = boost::shared_ptr<IncidentWaveNeumannF2>(new IncidentWaveNeumannF2(globalParameters));
        // assembled to the right hand vector
        feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
          new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveNeumannF2>(
            re_field_name,im_field_name,F,miit->second,commonData,
            zero_function,incident_wave_neumann_bc
          )
        );
      }

    }

    PetscFunctionReturn(0);

  }

  PetscErrorCode setBCForPolyIncidentWave(
    Mat A,Vec F,
    const string re_field_name,
    const string im_field_name,
    const string mesh_nodals_positions,
    map<int,SurfaceData>::iterator miit
  ) {

    PetscFunctionBegin;
    boost::shared_ptr<ZeroFunVal> zero_function = boost::shared_ptr<ZeroFunVal>(new ZeroFunVal());

    if(miit->second.aDmittance_imag!=0 || miit->second.aDmittance_real!=0) {
      feLhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHelmholtzMixBCLhs<ZeroFunVal>(re_field_name,im_field_name,A,miit->second,commonData,zero_function)
      );

      if(!globalParameters.isDirichlet.first) {
        if(globalParameters.sourceCoordinate.second) {
          boost::shared_ptr<IncidentWaveNeumannPointSourceDFT_F2> incident_wave_dirichlet_bc;
          incident_wave_dirichlet_bc = boost::shared_ptr<IncidentWaveNeumannPointSourceDFT_F2>(new IncidentWaveNeumannPointSourceDFT_F2(globalParameters));
          // assembled to the right hand vector
          feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
            new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveNeumannPointSourceDFT_F2>(
              re_field_name,im_field_name,F,miit->second,commonData,
              zero_function,incident_wave_dirichlet_bc
            )
          );
        } else {

          boost::shared_ptr<IncidentWaveNeumannDFT_F2> incident_wave_dirichlet_bc;
          incident_wave_dirichlet_bc = boost::shared_ptr<IncidentWaveNeumannDFT_F2>(new IncidentWaveNeumannDFT_F2(globalParameters));
          // assembled to the right hand vector
          feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
            new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveNeumannDFT_F2>(
              re_field_name,im_field_name,F,miit->second,commonData,
              zero_function,incident_wave_dirichlet_bc
            )
          );
        }

      } else {

        if(globalParameters.sourceCoordinate.second) {
          boost::shared_ptr<IncidentWaveDirichletPointSourceDFT_F2> incident_wave_dirichlet_bc;
          incident_wave_dirichlet_bc = boost::shared_ptr<IncidentWaveDirichletPointSourceDFT_F2>(new IncidentWaveDirichletPointSourceDFT_F2(globalParameters));
          // assembled to the right hand vector
          feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
            new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveDirichletPointSourceDFT_F2>(
              re_field_name,im_field_name,F,miit->second,commonData,
              zero_function,incident_wave_dirichlet_bc
            )
          );
        } else {
          boost::shared_ptr<IncidentWaveDirichletDFT_F2> incident_wave_dirichlet_bc;
          incident_wave_dirichlet_bc = boost::shared_ptr<IncidentWaveDirichletDFT_F2>(new IncidentWaveDirichletDFT_F2(globalParameters));
          // assembled to the right hand vector
          feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
            new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveDirichletDFT_F2>(
              re_field_name,im_field_name,F,miit->second,commonData,
              zero_function,incident_wave_dirichlet_bc
            )
          );
        }

      }

    } else {

      if(globalParameters.sourceCoordinate.second) {
        boost::shared_ptr<IncidentWaveNeumannPointSourceDFT_F2> incident_wave_neumann_bc;
        incident_wave_neumann_bc = boost::shared_ptr<IncidentWaveNeumannPointSourceDFT_F2>(new IncidentWaveNeumannPointSourceDFT_F2(globalParameters));
        // assembled to the right hand vector
        feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
          new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveNeumannPointSourceDFT_F2>(
            re_field_name,im_field_name,F,miit->second,commonData,
            zero_function,incident_wave_neumann_bc
          )
        );
      } else {
        boost::shared_ptr<IncidentWaveNeumannDFT_F2> incident_wave_neumann_bc;
        incident_wave_neumann_bc = boost::shared_ptr<IncidentWaveNeumannDFT_F2>(new IncidentWaveNeumannDFT_F2(globalParameters));
        // assembled to the right hand vector
        feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
          new OpHelmholtzMixBCRhs<ZeroFunVal,IncidentWaveNeumannDFT_F2>(
            re_field_name,im_field_name,F,miit->second,commonData,
            zero_function,incident_wave_neumann_bc
          )
        );
      }

    }

    PetscFunctionReturn(0);

  }


  PetscErrorCode setOperators(
    Mat A,Vec F,
    const string re_field_name,const string im_field_name,
    const string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
    PetscFunctionBegin;
    PetscErrorCode ierr;

    string fe_name;

    fe_name = "HELMHOLTZ_RERE_FE"; feLhs.insert(fe_name,new MyVolumeFE(mField,true,globalParameters.oRder.first,globalParameters.qUadrature.first,addToRank));
    fe_name = "HELMHOLTZ_RERE_FE"; feRhs.insert(fe_name,new MyVolumeFE(mField,true,globalParameters.oRder.first,globalParameters.qUadrature.first,addToRank));
    fe_name = "HELMHOLTZ_IMIM_FE"; feRhs.insert(fe_name,new MyVolumeFE(mField,true,globalParameters.oRder.first,globalParameters.qUadrature.first,addToRank));

    /* FIXME The different between two commands */
    // feLhs.at("HELMHOLTZ_RERE_FE").getOpPtrVector().push_back(
    //   new OpGetImIndices(im_field_name,im_field_name,commonData));
    feLhs.at("HELMHOLTZ_RERE_FE").getOpPtrVector().push_back(
      new OpGetImIndices(re_field_name,im_field_name,commonData));
    /* real field and imag field */
    feRhs.at("HELMHOLTZ_RERE_FE").getOpPtrVector().push_back(
      new OpGetValueAndGradAtGaussPts(re_field_name,commonData));
    feRhs.at("HELMHOLTZ_IMIM_FE").getOpPtrVector().push_back(
      new OpGetValueAndGradAtGaussPts(im_field_name,commonData));

    map<int,VolumeData>::iterator mit = volumeData.begin();
    for(;mit!=volumeData.end();mit++) {

      feLhs.at("HELMHOLTZ_RERE_FE").getOpPtrVector().push_back(
         new OpHelmholtzLhs(re_field_name,im_field_name,A,mit->second,commonData));

      feRhs.at("HELMHOLTZ_RERE_FE").getOpPtrVector().push_back(
         new OpHelmholtzRhs(re_field_name,F,mit->second,commonData,true));
      feRhs.at("HELMHOLTZ_IMIM_FE").getOpPtrVector().push_back(
         new OpHelmholtzRhs(im_field_name,F,mit->second,commonData,false));

    }


    fe_name = "HELMHOLTZ_REIM_FE";
    feLhs.insert(fe_name,new MySurfaceFE(mField,globalParameters.isDuffy.first,globalParameters.oRder.first,globalParameters.qUadrature.first,addToRank));
    feRhs.insert(fe_name,new MySurfaceFE(mField,globalParameters.isDuffy.first,globalParameters.oRder.first,globalParameters.qUadrature.first,addToRank));

    if(mField.check_field(mesh_nodals_positions)) {

      feLhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHoCoordTri(mesh_nodals_positions,commonData.hoCoordsTri)
      );

      feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHoCoordTri(mesh_nodals_positions,commonData.hoCoordsTri)
      );

    }

    // Get im indices
    feLhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(new OpGetImIndices(im_field_name,im_field_name,commonData));
    feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(new OpGetImIndices(im_field_name,im_field_name,commonData));

    // Get field values
    feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(new OpGetValueAtGaussPts(re_field_name,commonData));
    feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(new OpGetValueAtGaussPts(im_field_name,commonData));

    boost::shared_ptr<ZeroFunVal> zero_function = boost::shared_ptr<ZeroFunVal>(new ZeroFunVal());

    if(globalParameters.isMonochromaticWave.first) {
      setBCForMonoChromatic(A,F,re_field_name,im_field_name,mesh_nodals_positions);
    } else {
      setBCForPolyChromatic(A,F,re_field_name,im_field_name,mesh_nodals_positions);
    }


    map<int,SurfaceData>::iterator miit = sommerfeldBcData.begin();
    for(;miit!=sommerfeldBcData.end();miit++) {

      feLhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHelmholtzMixBCLhs<ZeroFunVal>(
          re_field_name,im_field_name,A,miit->second,commonData,
          zero_function
        )
      );

      // FIXME: need to add second functions so that residual is calculated properly
      feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHelmholtzMixBCRhs<ZeroFunVal,ZeroFunVal>(
          re_field_name,im_field_name,F,miit->second,commonData,
          zero_function,zero_function
        )
      );

    }

    boost::shared_ptr<BaylissTurkel> bayliss_turkel_bc =
    boost::shared_ptr<BaylissTurkel>(new BaylissTurkel());

    miit = baylissTurkelBcData.begin();
    for(;miit!=baylissTurkelBcData.end();miit++) {

      feLhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHelmholtzMixBCLhs<BaylissTurkel>(
          re_field_name,im_field_name,A,miit->second,commonData,
          bayliss_turkel_bc
        )
      );

      // FIXME: need to add second functions so that residual is calculated properly
      feRhs.at("HELMHOLTZ_REIM_FE").getOpPtrVector().push_back(
        new OpHelmholtzMixBCRhs<BaylissTurkel,ZeroFunVal>(
          re_field_name,im_field_name,F,miit->second,commonData,
          bayliss_turkel_bc,zero_function
        )
      );

    }

    PetscFunctionReturn(0);
  }

  PetscErrorCode calculateA(const string problem_name) {
    PetscFunctionBegin;

    PetscErrorCode ierr;
    boost::ptr_map<string,ForcesAndSurcesCore>::iterator mit = feLhs.begin();
    for(;mit!=feLhs.end();mit++) {

      ierr = mField.loop_finite_elements(problem_name,mit->first,*(mit->second)); CHKERRQ(ierr); CHKERRQ(ierr);

    }

    PetscFunctionReturn(0);
  }

  PetscErrorCode calculateF(const string problem_name) {
    PetscFunctionBegin;

    PetscErrorCode ierr;
    boost::ptr_map<string,ForcesAndSurcesCore>::iterator mit = feRhs.begin();
    for(;mit!=feRhs.end();mit++) {

      ierr = mField.loop_finite_elements(problem_name,mit->first,*(mit->second)); CHKERRQ(ierr); CHKERRQ(ierr);

    }

    PetscFunctionReturn(0);
  }

};

#endif //__HELMHOLTZ_ELEMENT_HPP

/***************************************************************************//**
 * \defgroup mofem_helmholtz_elem Helmholtz element
 * \ingroup user_modules
 ******************************************************************************/
