/** \file ReynoldsStress.hpp
 \ingroup mofem_helmholtz_elem

 \brief Operators and data structures for the calculation of
  momentum flux and Reynolds Stress of the complex scalar field
  with Homogeneous material.

 */

/*
  This work is part of PhD thesis by on Micro-fluids: Thomas Felix Xuan Meng
 */

/*
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
 * The header file should contains as least #include as possible for speed
 */


// #ifndef WITH_ADOL_C
//   #error "MoFEM need to be compiled with ADOL-C"
// #endif
/* \bug 1. Export the velocity data into vectors or .txt file,
   2. work for scalar; vector and tensor field. 3. Reynolds Stress */
struct ReynoldsStress: public VolumeElementForcesAndSourcesCore::UserDataOperator {

  MoFEM::Interface& mField;
  HelmholtzElement& helmholtzElement;
  moab::Interface &postProcMesh;
  vector<EntityHandle> &mapGaussPts;
  Vec V;

  PostProcVolumeOnRefinedMesh::CommonData &commonData;
  string reFieldName;
  string tagName;

  ReynoldsStress(
    MoFEM::Interface& m_field,
    HelmholtzElement& helmholtz_element,
    moab::Interface &post_proc_mesh,
    vector<EntityHandle> &map_gauss_pts,
    string re_field_name,
    string tag_name,
    PostProcVolumeOnRefinedMesh::CommonData &common_data,
    Vec v = PETSC_NULL):
    VolumeElementForcesAndSourcesCore::UserDataOperator(re_field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
    mField(m_field),
    helmholtzElement(helmholtz_element),
    postProcMesh(post_proc_mesh),mapGaussPts(map_gauss_pts),
    reFieldName(re_field_name),
    tagName(tag_name),
    commonData(common_data),
    V(v) {}


  PetscErrorCode doWork(
    int side,
    EntityType type,
    DataForcesAndSurcesCore::EntData &data) {
    PetscFunctionBegin;

    if(type != MBVERTEX) PetscFunctionReturn(0);
    if(data.getFieldData().size()==0) PetscFunctionReturn(0);
    // int nb_row_dofs = data.getIndices().size();
    // if(nb_row_dofs==0) PetscFunctionReturn(0);

    PetscErrorCode ierr;
    ErrorCode rval;
    int nb_gauss_pts = data.getN().size1();

    // const MoFEM::FEDofMoFEMEntity *dof_ptr = data.getFieldDofs()[0];
    /* scalar field in 3D has rank = 1, rank vector = 3, rank tensorial = 9 */
    double dEnsity = 1.0;
    if(helmholtzElement.globalParameters.dEnsity.second) {
      dEnsity = helmholtzElement.globalParameters.dEnsity.first;
    }

    double fRequency = helmholtzElement.globalParameters.fRequency.first;
    double signal_duration = helmholtzElement.globalParameters.signalDuration.first;

    double pRessure;
    double wEight;
    MatrixDouble MomentumFlux;
    double t;
    VectorDouble velocity;
    MatrixDouble Stress(3,3,0);
    double vonMises = 0;
    double sTress = 0;
    VectorDouble AverageVelocity;
    const complex< double > i( 0.0, 1.0 );
    /* save the trace of stress into vector.*/
    // MatrixDouble tRace;
    // tRace.resize(nb_gauss_pts,3,false);
    // if(type == MBVERTEX) {
    //   tRace.clear();
    // }

    /* The Momentum Flux Tensor or Fluctuating Reynolds Stress */
    const char* name;
    if(!helmholtzElement.globalParameters.isMonochromaticWave.first) {
      name = (tagName+"STRESS").c_str();
    } else {
      name = (tagName+"MOMENTUM").c_str();
    }

    Tag th_momentum;
    Tag th_von_mises;
    int tag_length = 9;
    if(helmholtzElement.globalParameters.second_order_stress.first) {
      tag_length = 1;
    }
    double def_VAL[tag_length];
    bzero(def_VAL,tag_length*sizeof(double));
    rval = postProcMesh.tag_get_handle(
      name,tag_length,MB_TYPE_DOUBLE,th_momentum,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL); CHKERRQ_MOAB(rval);

    rval = postProcMesh.tag_get_handle(
      (tagName+"VON_MISES").c_str(),1,MB_TYPE_DOUBLE,th_von_mises,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL); CHKERRQ_MOAB(rval);


    if(!helmholtzElement.globalParameters.isMonochromaticWave.first) {


      t = helmholtzElement.globalParameters.timeStep;
      velocity.resize(3);

      /* Apply Trapezoidal Rule */
      wEight = signal_duration / (2 * helmholtzElement.globalParameters.nbOfPointsInTime.first);
      if(t != 0 && t != helmholtzElement.globalParameters.nbOfPointsInTime.first - 1) {
        wEight *= 2;
      } else {

      }
      // wEight = 1.0 / (2 * helmholtzElement.globalParameters.nbOfPointsInTime.first);
      // if(t != 0 && t != helmholtzElement.globalParameters.nbOfPointsInTime.first - 1) {
      //   wEight *= 2;
      // } else {
      //
      // }

      if(!helmholtzElement.globalParameters.second_order_stress.first) {

        for(int gg = 0;gg<nb_gauss_pts;gg++) {

          velocity[0] = ( (commonData.gradMap[rowFieldName][gg])(0,0) );

          velocity[1] = ( (commonData.gradMap[rowFieldName][gg])(0,1) );

          velocity[2] = ( (commonData.gradMap[rowFieldName][gg])(0,2) );

          if(t != 0) {
            rval = postProcMesh.tag_get_data(th_momentum,&mapGaussPts[gg],1,&Stress(0,0)); CHKERRQ_MOAB(rval);
            rval = postProcMesh.tag_get_data(th_von_mises,&mapGaussPts[gg],1,&vonMises); CHKERRQ_MOAB(rval);
          }

          Stress += outer_prod(velocity,velocity)*dEnsity*wEight;
          /* Calculate Von Mises Stress Magnitude that will distort the shape of fluids */
          double von_mises = 0.5*(pow(Stress(0,0)-Stress(1,1),2.0)+pow(Stress(1,1)-Stress(2,2),2.0)+pow(Stress(0,0)-Stress(2,2),2.0)+6*(pow(Stress(1,2),2.0)+pow(Stress(2,0),2.0)+pow(Stress(0,1),2.0)));
          vonMises += sqrt(von_mises);
          // if(t+1 == helmholtzElement.globalParameters.nbOfPointsInTime.first) {
          //   tRace(gg,0) = Stress[0,0];
          //   tRace(gg,1) = Stress[1,1];
          //   tRace(gg,3) = Stress[2,2];
          // }

          rval = postProcMesh.tag_set_data(
            th_momentum,&mapGaussPts[gg],1,&Stress(0,0)
          ); CHKERRQ_MOAB(rval);
          rval = postProcMesh.tag_set_data(
            th_von_mises,&mapGaussPts[gg],1,&vonMises
          ); CHKERRQ_MOAB(rval);

        }
      } else {

        double vElocity = 1;
        if(helmholtzElement.globalParameters.vElocity.second) {
          double vElocity = helmholtzElement.globalParameters.vElocity.first;
        }
        AverageVelocity.resize(1);

        double const1 = 1/(2*dEnsity*vElocity*vElocity);
        double const2 = 0.5 * dEnsity;

        for(int gg = 0;gg<nb_gauss_pts;gg++) {
          pRessure = commonData.fieldMap[rowFieldName][gg][0];

          velocity[0] = ( (commonData.gradMap[rowFieldName][gg])(0,0) );

          velocity[1] = ( (commonData.gradMap[rowFieldName][gg])(0,1) );

          velocity[2] = ( (commonData.gradMap[rowFieldName][gg])(0,2) );
          AverageVelocity[0]  = inner_prod(velocity,velocity)*const2;
          rval = postProcMesh.tag_get_data(th_momentum,&mapGaussPts[gg],1,&sTress); CHKERRQ_MOAB(rval);

          sTress += wEight*(- const1 * pRessure * pRessure + AverageVelocity[0]);
          rval = postProcMesh.tag_set_data(th_momentum,&mapGaussPts[gg],1,&sTress); CHKERRQ_MOAB(rval);

        }

      }

    } else {

      //Combine eigenvalues and vectors to create principal stress vector
      VectorDouble prin_stress_vect1(3);
      VectorDouble prin_stress_vect2(3);
      VectorDouble prin_stress_vect3(3);
      VectorDouble prin_vals_vect(3);

      Tag th_prin_stress_vect1,th_prin_stress_vect2,th_prin_stress_vect3;
      name = (tagName+"S1").c_str();
      rval = postProcMesh.tag_get_handle(
        name,3,MB_TYPE_DOUBLE,th_prin_stress_vect1,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL); CHKERRQ_MOAB(rval);
      name = (tagName+"S2").c_str();
      rval = postProcMesh.tag_get_handle(
        name,3,MB_TYPE_DOUBLE,th_prin_stress_vect2,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL); CHKERRQ_MOAB(rval);
      name = (tagName+"S3").c_str();
      rval = postProcMesh.tag_get_handle(
        name,3,MB_TYPE_DOUBLE,th_prin_stress_vect3,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL); CHKERRQ_MOAB(rval);

      Tag th_prin_stress_vals;
      name = (tagName+"PRINCIPAL_STRESS").c_str();
      rval = postProcMesh.tag_get_handle(
        name,3,MB_TYPE_DOUBLE,th_prin_stress_vals,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL); CHKERRQ_MOAB(rval);

      if(mapGaussPts.size()!=(unsigned int)nb_gauss_pts) {
        SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data inconsistency");
      }

      for(int gg = 0;gg<nb_gauss_pts;gg++) {

        velocity.resize(3);
        MomentumFlux.resize(3,3);
        velocity[0] = (commonData.gradMap[rowFieldName][gg])(0,0);
        velocity[1] = (commonData.gradMap[rowFieldName][gg])(0,1);
        velocity[2] = (commonData.gradMap[rowFieldName][gg])(0,2);
        MomentumFlux = outer_prod(velocity,velocity);
        MomentumFlux *= dEnsity;

        rval = postProcMesh.tag_set_data(th_momentum,&mapGaussPts[gg],1,&MomentumFlux(0,0)); CHKERRQ_MOAB(rval);

        /* calculate Principal Stress and its
        corresponding Principal Direction */
        // ublas::matrix< FieldData > eigen_vectors = MomentumFlux;
        // VectorDouble eigen_values(3);

        // LAPACK - eigenvalues and vectors. Applied twice for initial creates memory space
        // int n = 3, lda = 3, info, lwork = -1;
        // double wkopt;
        // info = lapack_dsyev('V','U',n,&(eigen_vectors.data()[0]),lda,&(eigen_values.data()[0]),&wkopt,lwork);
        // if(info != 0) SETERRQ1(PETSC_COMM_SELF,1,"is something wrong with lapack_dsyev info = %d",info);
        // lwork = (int)wkopt;
        // double work[lwork];
        // info = lapack_dsyev('V','U',n,&(eigen_vectors.data()[0]),lda,&(eigen_values.data()[0]),work,lwork);
        // if(info != 0) SETERRQ1(PETSC_COMM_SELF,1,"is something wrong with lapack_dsyev info = %d",info);

        //eigen_vectors = trans(eigen_vectors);

        // prin_vals_vect[0] = eigen_values[0];
        // prin_vals_vect[1] = eigen_values[1];
        // prin_vals_vect[2] = eigen_values[2];
        // for (int ii=0; ii < 3; ii++) {
        //   prin_stress_vect1[ii] = eigen_vectors.data()[ii+3*0];
        //   prin_stress_vect2[ii] = eigen_vectors.data()[ii+3*1];
        //   prin_stress_vect3[ii] = eigen_vectors.data()[ii+3*2];
        // }
        //
        // //Tag principle stress vectors 1, 2, 3
        // rval = postProcMesh.tag_set_data(th_prin_stress_vect1,&mapGaussPts[gg],1,&prin_stress_vect1[0]); CHKERRQ_MOAB(rval);
        // rval = postProcMesh.tag_set_data(th_prin_stress_vect2,&mapGaussPts[gg],1,&prin_stress_vect2[0]); CHKERRQ_MOAB(rval);
        // rval = postProcMesh.tag_set_data(th_prin_stress_vect3,&mapGaussPts[gg],1,&prin_stress_vect3[0]); CHKERRQ_MOAB(rval);
        // rval = postProcMesh.tag_set_data(th_prin_stress_vals,&mapGaussPts[gg],1,&prin_vals_vect[0]); CHKERRQ_MOAB(rval);

      }

    }
    PetscFunctionReturn(0);
  }

};
