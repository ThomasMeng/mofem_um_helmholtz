/** \file fe_approximation.cpp

  Calculates finite element (Galerkin) approximation for incident wave problem.

  Note:

  In this implementation, first acoustic potential field is approximated on
  boundary and then finite element problem is solved.  For more rigorous
  convergence study, trace of best approximations on boundary can be calculated
  and then finite element for domain and Neumann/mix boundary.  That will give
  exact pollution error.

 */

/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <MoFEM.hpp>
using namespace MoFEM;

#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/symmetric.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/ptr_container/ptr_map.hpp>

#include <MethodForForceScaling.hpp>
#include <DirichletBC.hpp>
#include <PostProcOnRefMesh.hpp>

#include <Projection10NodeCoordsOnField.hpp>
#include <petsctime.h>
#include <fstream>
#include <iostream>
#include <valarray>

#include <stdexcept>
#include <cmath>
#include <boost/math/special_functions.hpp>
#include <complex>

#include <FieldApproximation.hpp>

using namespace std;
using namespace boost::math;

#include <boost/shared_array.hpp>
#include <kiss_fft.h>
#include <kiss_fft.c>

#include <AnalyticalDirichletHelmholtz.hpp>
#include <AnalyticalSolutions.hpp>
#include <HelmholtzElement.hpp>
#include <ReynoldsStress.hpp>
#include <SecondOrderStress.hpp>
#include <TimeSeries.hpp>

// struct PlaneIncidentWaveSacttrerData {
//
//   Range tRis;
//
// };
static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  ErrorCode rval;
  PetscErrorCode ierr;

  PetscInitialize(&argc,&argv,(char *)0,help);

  //Core mb_instance;
  moab::Core mb_instance;
  moab::Interface& moab = mb_instance;
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  PetscBool flg = PETSC_TRUE;
  char mesh_file_name[255];
  ierr = PetscOptionsGetString(PETSC_NULL,PETSC_NULL,"-my_file",mesh_file_name,255,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"*** ERROR -my_file (MESH FILE NEEDED)");
  }

  // Create moab parallel communicator
  ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
  if(pcomm == NULL) pcomm =  new ParallelComm(&moab,PETSC_COMM_WORLD);

  PetscBool is_partitioned = PETSC_FALSE;
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-my_is_partitioned",&is_partitioned,&flg); CHKERRQ(ierr);
  if(is_partitioned) {
    //Read mesh to MOAB PARALLEL=READ_DELETE PARALLEL=READ_PART
    const char *option;
    option = "PARALLEL=BCAST_DELETE;PARALLEL_RESOLVE_SHARED_ENTS;PARTITION=PARALLEL_PARTITION;";
    // option = "PARALLEL=READ_PART;PARALLEL_RESOLVE_SHARED_ENTS;PARTITION=PARALLEL_PARTITION;";
    // rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  } else {
    const char *option;
    option = "";
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  }


  PetscBool is_labatto = PETSC_TRUE;
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-lobatto",&is_labatto,NULL); CHKERRQ(ierr);


  // Create MoFEM (cephas) database
  MoFEM::Core core(moab);
  MoFEM::Interface& m_field = core;


  //set entitities bit level
  BitRefLevel bit_level0;
  bit_level0.set(0);
  EntityHandle meshset_level0;
  rval = moab.create_meshset(MESHSET_SET,meshset_level0); CHKERRQ_MOAB(rval);
  ierr = m_field.seed_ref_level_3D(0,bit_level0); CHKERRQ(ierr);

  //Fields
  if(is_labatto) {
    ierr = m_field.add_field("rePRES",H1,AINSWORTH_LOBBATO_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_field("imPRES",H1,AINSWORTH_LOBBATO_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_field("P",H1,AINSWORTH_LOBBATO_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);  // in time domain
  } else {
    ierr = m_field.add_field("rePRES",H1,AINSWORTH_LEGENDRE_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_field("imPRES",H1,AINSWORTH_LEGENDRE_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_field("P",H1,AINSWORTH_LEGENDRE_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);  // in time domain
  }

  //meshset consisting all entities in mesh
  EntityHandle root_set = moab.get_root_set();
  //add entities to field
  ierr = m_field.add_ents_to_field_by_TETs(root_set,"rePRES"); CHKERRQ(ierr);
  ierr = m_field.add_ents_to_field_by_TETs(root_set,"imPRES"); CHKERRQ(ierr);
  ierr = m_field.add_ents_to_field_by_TETs(root_set,"P"); CHKERRQ(ierr);
  /* FIXME add wave number A priori error estimator in here as a field, which
     store the error on the elements of computational domain. */

  //set app. order
  //see Hierarchic Finite Element Bases on Unstructured Tetrahedral Meshes (Mark Ainsworth & Joe Coyle)
  PetscInt order;
  ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-my_order",&order,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    order = 2;
  }

  PetscBool wavenumber_flg;
  double wavenumber;
  // set wave number from line command, that overwrite numbre form block set
  ierr = PetscOptionsGetScalar(PETSC_NULL,NULL,"-wave_number",&wavenumber,&wavenumber_flg); CHKERRQ(ierr);
  if(!wavenumber_flg) {

    SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"wave number not given, set in line command -wave_number to fix problem");

  }


  PetscBool aDaptivity = PETSC_FALSE;
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-adaptivity",&aDaptivity,NULL); CHKERRQ(ierr);


  if(aDaptivity) {
    /* A priori error indicator */
    /* Introduced by Bériot from Efficient implementation of high-order finite
       elements for Helmholtz problems */
    PetscInt threeErrorlv = 1;
    ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-error_level",&threeErrorlv,NULL); CHKERRQ(ierr);
    if(threeErrorlv > 2) {
      threeErrorlv = 2;
    }

    /* 1 => l2, 2 => h1, 3 => anisworth */
    PetscInt l2 = 1;
    ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-error_type",&l2,NULL); CHKERRQ(ierr);
    if(l2 > 3) {
      l2 = 3;
    }

    /* 1 => maximum edge length, 2 => average edge length, 3 => minimum edge length */
    pair<PetscInt,PetscBool> isCriterion;
    isCriterion.first = 1;
    ierr = PetscOptionsGetInt(PETSC_NULL,PETSC_NULL,"-error_criterion",&isCriterion.first,&isCriterion.second); CHKERRQ(ierr);
    if(isCriterion.first > 3) {
      isCriterion.first = 3;
    }

    Range tets;
    map<int,Range> orders_map;

    rval = m_field.get_moab().get_entities_by_type(0,MBTET,tets,false); CHKERRQ_MOAB(rval);
    // for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,"MAT_HELMHOLTZ",it)) {
    //   rval = mField.get_moab().get_entities_by_type(it->meshset,MBTET,tets,true); CHKERRQ_MOAB(rval);
    // }

    for(Range::iterator tit = tets.begin();tit!=tets.end();tit++) {

      double sum = 0;
      double eDges [6];
      // std::vector<double> valarryEdge(6);
      for(int ee = 0;ee<6;ee++) {

        EntityHandle edge;
        rval = m_field.get_moab().side_element(*tit,1,ee,edge); CHKERRQ_MOAB(rval);
        const EntityHandle* conn;
        int num_nodes;
        rval = m_field.get_moab().get_connectivity(edge,conn,num_nodes,true); CHKERRQ_MOAB(rval);

        double coords[num_nodes*3];
        rval = m_field.get_moab().get_coords(conn,num_nodes,coords); CHKERRQ_MOAB(rval);
        cblas_daxpy(3,-1,&coords[0],1,&coords[3],1);
        // for(int ii = 0;ii<6;ii++) {
          // cout << "\n number " << ii << endl;
          // cout << " coords after cblas_daxpy = \n" << coords[ii] << endl;
        // }
        eDges[ee] = cblas_dnrm2(3,&coords[3],1);
        sum += eDges[ee];
        // valarryEdge[ee] = cblas_dnrm2(3,&coords[3],1);

      }

      /* more robust to select average length of edges of elements than
        max or min */

      double averageEdgeK = (sum/6.) * wavenumber;
      double maxEdge = *std::max_element(eDges,eDges+6);
      double minEdge = *std::min_element(eDges,eDges+6);

      /* assign element size definition based on mesh quality */
      // double qualityIndicator = 3.*minEdge/maxEdge;

      // if(qualityIndicator <= 1.0) {
      //   cerr << "\n qualityIndicator = \n" << qualityIndicator << endl;
      // }
      // if( (qualityIndicator < 1.5) || (qualityIndicator > 0.) ) {
      //   averageEdgeK = maxEdge * wavenumber;
      // }

      if(isCriterion.first == 1) {
        averageEdgeK = maxEdge * wavenumber;
      } else if(isCriterion.first == 2) {
        averageEdgeK = minEdge * wavenumber;;
      }

      // cerr << "\n averageEdgeK = \n" << averageEdgeK << endl;


      // Caulate order from table of 1D error test.
      /* FIXME the order of p needs to be extend to more than 10 in future implementation */

      std::vector<double> errorLevel (10);

      double error_level [10];
      if(threeErrorlv == 0) {
        if(l2 == 1) {
          /* target l2 error 15% */
          /* error estimator in paper */
          //  memcpy(error_level, (double[]) {1.5,2.9,4.6,6.4,8.1,10.1,11.8,13.7,15.5,17.4}, sizeof error_level);
          /* error estimator 1 */
          double errorLevel [10] = {0.5,2.900,4.8,7.5,7.97,12.33,12.54,14.6,15.2,19.43};
          /* try the following codes, delete [] error_level; Free memory after use */
          // memcpy(error_level, new double[] {0.5,2.900,4.8,7.5,7.97,12.33,12.54,14.6,15.2,19.43}, sizeof error_level);
          memcpy(error_level, &errorLevel, sizeof error_level);
          /* error estimator 3 */
          //  memcpy(error_level, (double[]){1.62,2.900,4.52,6.52,8.275,9.995,11.885,13.645,15.515,17.6125}, sizeof error_level);
        } else if(l2 == 2) {
          /* error estimator h1 1 */
          double errorLevel [10] = {0.2,2.3,3.88,6.8,7.1,11,11.17,14.54,15.28,19.023};
          memcpy(error_level, &errorLevel, sizeof error_level);
          /* error estimator h1 3 */
          //  memcpy(error_level, (double[]) {0.675,1.956,4.255,6.49,8.315,9.085,11.445,13.650,15.765,17.9125}, sizeof error_level);
        } else {
          /* Anisworth error estimator */
          //  error_level [ ] = { };
        }
        errorLevel.insert (errorLevel.begin(),error_level,error_level+10);
        // for(int ii = 0;ii<9;ii++) {
        //   cerr << " \n error_level[ii] = \n " << error_level[ii] << endl;
        // }
      } else if(threeErrorlv == 1) {

        if(l2 == 1) {
          /* target l2 error 5% */
          //  memcpy(error_level, (double[]) {0.8,2.0,3.4,5.0,6.6,8.4,10.1,11.9,13.7,15.4}, sizeof error_level);
          double errorLevel [10] = {0.1,1.85,3.67,6.6,6.76,8.96,10.3,13.43,13.7,17};
          memcpy(error_level, &errorLevel, sizeof error_level);
          //  memcpy(error_level, (double[]) {0.776,1.98,3.755,5.25,6.585,8.385,10.395,12.170,13.765,15.3125}, sizeof error_level);
        }  else if(l2 == 2) {
          double errorLevel [10] = {0.05,1.85,3.25,5.5,5.6,8.4,9.67,12.74,12.75,15.04};
          memcpy(error_level, &errorLevel, sizeof error_level);
          //  memcpy(error_level, (double[]) {0.049535,1.31,3.455,3.78,5.585,7.685,10.039,12.230,12.6965,14.6325}, sizeof error_level);
        } else {
          //  memcpy(error_level, (double[]) { };
        }
        errorLevel.insert (errorLevel.begin(),error_level,error_level+10);

      } else if(threeErrorlv == 2) {

        if(l2 == 1) {
          /* target l2 error 0.5% */
          // memcpy(error_level, (double[]) {0.2,0.9,1.9,3.1,4.5,5.9,7.4,8.9,10.6,12.2}, sizeof error_level);
          double errorLevel [10] = {0.05,1,1.33,3.6,4.6,7.05,7.35,9.4,10.76,13.625};
          memcpy(error_level, &errorLevel, sizeof error_level);
          // memcpy(error_level, (double[]) {0.249,1.085,2.175,3.11,4.625,6.745,7.439,8.815,10.7965,13.0825}, sizeof error_level);
        }  else if(l2 == 2) {
          double errorLevel [10] = {0.02,0.41,0.83,2.35,3.64,6.25,6.28,8.46,9.83,12.62};
          memcpy(error_level, &errorLevel, sizeof error_level);
          // memcpy(error_level, (double[]) {0.120,0.685,1.155,2.175,3.905,6.145,6.149,7.915,10.1865,12.4965}, sizeof error_level);
        } else {
          // memcpy(error_level, (double[]) { };
        }
        errorLevel.insert (errorLevel.begin(),error_level,error_level+10);

      }

      int orderP = 1;
      for(int ii = 1;ii<8;ii++) {
        if(errorLevel[ii] < averageEdgeK) {
          orderP = ii + 1;
        } else {
          break;
        }

      }

      // input a prior error indicator by ascending order with Range.
      orders_map[orderP].insert(*tit);

    }

    map<int,Range>::iterator mit = orders_map.begin();
    int max_value = mit->first;
    int min_value = 10;
    int avg_value = 0;

    for(;mit!=orders_map.end();mit++) {

      if(mit->first > max_value) {
        max_value = mit->first;
      }
      if(min_value > mit->first) {
        min_value = mit->first;
      }
      avg_value += mit->first;


      Range tris;
      rval = moab.get_adjacencies(mit->second,2,false,tris,moab::Interface::UNION);
      Range edges;
      rval = moab.get_adjacencies(mit->second,1,false,edges,moab::Interface::UNION);

      /* WARNING : the order of triangle and edges are consistent with tets in current implementation */
      // ierr = m_field.set_field_order(mit->second,"rePRES",mit->first,2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(mit->second,"rePRES",mit->first); CHKERRQ(ierr);
      ierr = m_field.set_field_order(tris,"rePRES",mit->first); CHKERRQ(ierr);
      ierr = m_field.set_field_order(edges,"rePRES",mit->first); CHKERRQ(ierr);
      //
      ierr = m_field.set_field_order(mit->second,"imPRES",mit->first); CHKERRQ(ierr);
      ierr = m_field.set_field_order(tris,"imPRES",mit->first); CHKERRQ(ierr);
      ierr = m_field.set_field_order(edges,"imPRES",mit->first); CHKERRQ(ierr);

      ierr = m_field.set_field_order(mit->second,"P",mit->first); CHKERRQ(ierr);
      ierr = m_field.set_field_order(tris,"P",mit->first); CHKERRQ(ierr);
      ierr = m_field.set_field_order(edges,"P",mit->first); CHKERRQ(ierr);

    }

    if(!pcomm->rank()) {
      printf("max_value_order_p %u\n",max_value);
      printf("min_value_order_p %u\n",min_value);
      printf("avg_value_order_p %u\n",int (avg_value/orders_map.size()));
    }

  } else {

    ierr = m_field.set_field_order(root_set,MBTET,"rePRES",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(root_set,MBTRI,"rePRES",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(root_set,MBEDGE,"rePRES",order); CHKERRQ(ierr);

    ierr = m_field.set_field_order(root_set,MBTET,"imPRES",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(root_set,MBTRI,"imPRES",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(root_set,MBEDGE,"imPRES",order); CHKERRQ(ierr);

    ierr = m_field.set_field_order(root_set,MBTET,"P",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(root_set,MBTRI,"P",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(root_set,MBEDGE,"P",order); CHKERRQ(ierr);

  }


  ierr = m_field.set_field_order(root_set,MBVERTEX,"rePRES",1); CHKERRQ(ierr);
  ierr = m_field.set_field_order(root_set,MBVERTEX,"imPRES",1); CHKERRQ(ierr);
  ierr = m_field.set_field_order(root_set,MBVERTEX,"P",1); CHKERRQ(ierr);

  if(!m_field.check_field("MESH_NODE_POSITIONS")) {

    ierr = m_field.add_field("MESH_NODE_POSITIONS",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_ents_to_field_by_TETs(root_set,"MESH_NODE_POSITIONS"); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBTET,"MESH_NODE_POSITIONS",2); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBTRI,"MESH_NODE_POSITIONS",2); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBEDGE,"MESH_NODE_POSITIONS",2); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBVERTEX,"MESH_NODE_POSITIONS",1); CHKERRQ(ierr);

    ierr = m_field.build_fields(); CHKERRQ(ierr);
    Projection10NodeCoordsOnField ent_method_material(m_field,"MESH_NODE_POSITIONS");
    ierr = m_field.loop_dofs("MESH_NODE_POSITIONS",ent_method_material); CHKERRQ(ierr);

  } else {

    ierr = m_field.build_fields(); CHKERRQ(ierr);

  }

  // Finite Elements

  HelmholtzElement helmholtz_element(m_field);
  ierr = helmholtz_element.getGlobalParametersFromLineCommandOptions(); CHKERRQ(ierr);
  ierr = helmholtz_element.addHelmholtzElements("rePRES","imPRES"); CHKERRQ(ierr);
  if(m_field.check_field("reEX") && m_field.check_field("imEX")) {
    ierr = m_field.modify_finite_element_add_field_data("HELMHOLTZ_RERE_FE","reEX"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("HELMHOLTZ_RERE_FE","imEX"); CHKERRQ(ierr);
  }

  bool Dirichlet_bc_set = false;
  Range bc_dirichlet_tris,analytical_bc_tris;
  for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_(m_field,"ANALYTICAL_BC",it)) {
    rval = moab.get_entities_by_type(it->getMeshset(),MBTRI,analytical_bc_tris,true); CHKERRQ_MOAB(rval);
    Dirichlet_bc_set = true;
  }
  bc_dirichlet_tris.merge(analytical_bc_tris);
  AnalyticalDirichletHelmholtzBC analytical_bc_real(m_field);
  AnalyticalDirichletHelmholtzBC analytical_bc_imag(m_field);
  ierr = analytical_bc_real.initializeProblem(m_field,"BCREAL_FE","rePRES",analytical_bc_tris); CHKERRQ(ierr);
  ierr = analytical_bc_imag.initializeProblem(m_field,"BCIMAG_FE","imPRES",analytical_bc_tris); CHKERRQ(ierr);


  double aTtenuation = 0;
  // set loss tangent from line command, the complex wave number to include attenuation.
  ierr = PetscOptionsGetScalar(PETSC_NULL,NULL,"-attenuation",&aTtenuation,NULL); CHKERRQ(ierr);

  ierr = PetscOptionsBegin(m_field.get_comm(),NULL,"Helmholtz problem options","none"); CHKERRQ(ierr);
  PetscBool isRayleigh = PETSC_FALSE;
  ierr = PetscOptionsBool(
    "-rayleigh_wave",
    "If true the incident is Rayleigh wave, otherwise, it is an non-attenuated wave","",
    PETSC_FALSE,
    &isRayleigh,
    NULL
  ); CHKERRQ(ierr);

  double power_of_incident_wave = 1;
  ierr = PetscOptionsGetScalar(PETSC_NULL,NULL,"-amplitude_of_incident_wave",&power_of_incident_wave,NULL); CHKERRQ(ierr);


  // This is added for a case than on some surface, defined by the user a
  // incident plane wave is scattered.
  map<int,PlaneIncidentWaveSacttrerData> planeWaveScatterData;
  for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_(m_field,"SOFT_INCIDENT_WAVE_BC",it)) {
    rval = moab.get_entities_by_type(it->getMeshset(),MBTRI,planeWaveScatterData[it->getMeshsetId()].tRis,true); CHKERRQ_MOAB(rval);
    ierr = analytical_bc_real.initializeProblem(m_field,"BCREAL_FE","rePRES",planeWaveScatterData[it->getMeshsetId()].tRis); CHKERRQ(ierr);
    ierr = analytical_bc_imag.initializeProblem(m_field,"BCIMAG_FE","imPRES",planeWaveScatterData[it->getMeshsetId()].tRis); CHKERRQ(ierr);
    bc_dirichlet_tris.merge(planeWaveScatterData[it->getMeshsetId()].tRis);

    Dirichlet_bc_set = true;

  }

  //ierr = m_field.build_finite_elements(); CHKERRQ(ierr);
  //// Build adjacencies
  //ierr = m_field.build_adjacencies(bit_level0); CHKERRQ(ierr);

  // Problem
  ierr = m_field.add_problem("ACOUSTIC_PROBLEM"); CHKERRQ(ierr);
  ierr = m_field.add_problem("BCREAL_PROBLEM"); CHKERRQ(ierr); //analytical Dirichlet for real field
  ierr = m_field.add_problem("BCIMAG_PROBLEM"); CHKERRQ(ierr); //analytical Dirichlet for imag field

  // Set refinement level for problem
  ierr = m_field.modify_problem_ref_level_add_bit("ACOUSTIC_PROBLEM",bit_level0); CHKERRQ(ierr);
  ierr = m_field.modify_problem_ref_level_add_bit("BCREAL_PROBLEM",bit_level0); CHKERRQ(ierr);  //analytical Dirichlet
  ierr = m_field.modify_problem_ref_level_add_bit("BCIMAG_PROBLEM",bit_level0); CHKERRQ(ierr);  //analytical Dirichlet

  // Add elements to problems
  ierr = m_field.modify_problem_add_finite_element("ACOUSTIC_PROBLEM","HELMHOLTZ_RERE_FE"); CHKERRQ(ierr);
  ierr = m_field.modify_problem_add_finite_element("ACOUSTIC_PROBLEM","HELMHOLTZ_IMIM_FE"); CHKERRQ(ierr);
  ierr = m_field.modify_problem_add_finite_element("ACOUSTIC_PROBLEM","HELMHOLTZ_REIM_FE"); CHKERRQ(ierr);

  ierr = m_field.modify_problem_add_finite_element("BCREAL_PROBLEM","BCREAL_FE"); CHKERRQ(ierr);
  ierr = m_field.modify_problem_add_finite_element("BCIMAG_PROBLEM","BCIMAG_FE"); CHKERRQ(ierr);

  // Get start time for analyse
  PetscLogDouble t1,t2;
  PetscLogDouble v1,v2;
  ierr = PetscTime(&v1); CHKERRQ(ierr);
  ierr = PetscGetCPUTime(&t1); CHKERRQ(ierr);
  PetscLogDouble m1;
  PetscLogDouble m2;
  ierr = PetscMemoryGetCurrentUsage(&m1); CHKERRQ(ierr);

  // Build problems
  ierr = m_field.build_finite_elements(); CHKERRQ(ierr);
  // Build adjacencies
  ierr = m_field.build_adjacencies(bit_level0); CHKERRQ(ierr);

  // build porblems
  if(is_partitioned) {
    // if mesh is partitioned

    ierr = m_field.build_problem_on_distributed_mesh("ACOUSTIC_PROBLEM",true); CHKERRQ(ierr);
    ierr = m_field.partition_finite_elements("ACOUSTIC_PROBLEM",true); CHKERRQ(ierr);

    if(Dirichlet_bc_set) {

      ierr = m_field.build_problem_on_distributed_mesh("BCREAL_PROBLEM",true); CHKERRQ(ierr);
      ierr = m_field.partition_finite_elements("BCREAL_PROBLEM",true); CHKERRQ(ierr);

      ierr = m_field.build_problem_on_distributed_mesh("BCIMAG_PROBLEM",true); CHKERRQ(ierr);
      ierr = m_field.partition_finite_elements("BCIMAG_PROBLEM",true); CHKERRQ(ierr);

    }


  } else {
    // if not partitioned mesh is load to all processes

    ierr = m_field.build_problem("ACOUSTIC_PROBLEM",true); CHKERRQ(ierr);
    ierr = m_field.partition_problem("ACOUSTIC_PROBLEM"); CHKERRQ(ierr);
    ierr = m_field.partition_finite_elements("ACOUSTIC_PROBLEM"); CHKERRQ(ierr);

    if(Dirichlet_bc_set) {
      ierr = m_field.build_problem("BCREAL_PROBLEM",true); CHKERRQ(ierr);
      ierr = m_field.partition_problem("BCREAL_PROBLEM"); CHKERRQ(ierr);
      ierr = m_field.partition_finite_elements("BCREAL_PROBLEM"); CHKERRQ(ierr);



      ierr = m_field.build_problem("BCIMAG_PROBLEM",true); CHKERRQ(ierr);
      ierr = m_field.partition_problem("BCIMAG_PROBLEM"); CHKERRQ(ierr);
      ierr = m_field.partition_finite_elements("BCIMAG_PROBLEM"); CHKERRQ(ierr);
    }
  }

  ierr = m_field.partition_ghost_dofs("ACOUSTIC_PROBLEM"); CHKERRQ(ierr);
  if(Dirichlet_bc_set) {
    ierr = m_field.partition_ghost_dofs("BCREAL_PROBLEM"); CHKERRQ(ierr);
    ierr = m_field.partition_ghost_dofs("BCIMAG_PROBLEM"); CHKERRQ(ierr);
  }

  // Get problem matrices and vectors
  Vec F;  //Right hand side vector
  ierr = m_field.VecCreateGhost("ACOUSTIC_PROBLEM",ROW,&F); CHKERRQ(ierr);
  Vec T; //Solution vector
  ierr = VecDuplicate(F,&T); CHKERRQ(ierr);
  Mat A; //Left hand side matrix
  ierr = m_field.MatCreateMPIAIJWithArrays("ACOUSTIC_PROBLEM",&A); CHKERRQ(ierr);
  /* change matrix type to suit ilu */
  // Mat B;
  // ierr = MatConvert(B,MATBAIJ,MAT_INITIAL_MATRIX,&A);
  // ierr = MatDestroy(&B); CHKERRQ(ierr);
  // ierr = MatSetType(A,MATBAIJ);
  // ierr = m_field.MatCreateMPIBAIJWithArrays("ACOUSTIC_PROBLEM",&A); CHKERRQ(ierr);
  /* change matrix type to suit ilu end */
  ierr = helmholtz_element.setOperators(A,F,"rePRES","imPRES"); CHKERRQ(ierr);


  //wave direction unit vector=[x,y,z]^T
  VectorDouble wave_direction;
  wave_direction.resize(3);
  wave_direction.clear();
  wave_direction[2] = 1; // default:X direction [0,0,1]

  int nmax = 3;
  ierr = PetscOptionsGetRealArray(PETSC_NULL,PETSC_NULL,"-wave_direction",&wave_direction[0],&nmax,NULL); CHKERRQ(ierr);
  if(nmax > 0 && nmax != 3) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_INVALID_DATA,"*** ERROR -wave_direction [3*1 vector] default:X direction [0,0,1]");
  }

  PetscInt choise_value = NO_ANALYTICAL_SOLUTION;
  // set type of analytical solution
  ierr = PetscOptionsGetEList(PETSC_NULL,NULL,"-analytical_solution_type",analytical_solution_types,6,&choise_value,NULL); CHKERRQ(ierr);

  switch((AnalyticalSolutionTypes)choise_value) {

    case HARD_SPHERE_SCATTER_WAVE:

    {
      double scattering_sphere_radius = 0.5;
      ierr = PetscOptionsGetScalar(PETSC_NULL,NULL,"-scattering_sphere_radius",&scattering_sphere_radius,NULL); CHKERRQ(ierr);



      boost::shared_ptr<HardSphereScatterWave> function_evaluator = boost::shared_ptr<HardSphereScatterWave>(
        new HardSphereScatterWave(wavenumber,scattering_sphere_radius)
      );
      ierr = analytical_bc_real.setApproxOps(
        m_field,"rePRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::REAL
      ); CHKERRQ(ierr);
      ierr = analytical_bc_imag.setApproxOps(
        m_field,"imPRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::IMAG
      ); CHKERRQ(ierr);
      Dirichlet_bc_set = true;

    }

    break;

    case SOFT_SPHERE_SCATTER_WAVE:

      {

      double scattering_sphere_radius = 0.5;
      ierr = PetscOptionsGetScalar(PETSC_NULL,NULL,"-scattering_sphere_radius",&scattering_sphere_radius,NULL); CHKERRQ(ierr);


      boost::shared_ptr<SoftSphereScatterWave> function_evaluator = boost::shared_ptr<SoftSphereScatterWave>(new SoftSphereScatterWave(wavenumber,scattering_sphere_radius));
      ierr = analytical_bc_real.setApproxOps(m_field,"rePRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::REAL); CHKERRQ(ierr);
      ierr = analytical_bc_imag.setApproxOps(m_field,"imPRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::IMAG); CHKERRQ(ierr);
      Dirichlet_bc_set = true;

    }

    break;

    case PLANE_WAVE:

    {

      double angle = 0.25;
      // set wave number from line command, that overwrite numbre form block set
      ierr = PetscOptionsGetScalar(PETSC_NULL,NULL,"-wave_guide_angle",&angle,NULL); CHKERRQ(ierr);


      boost::shared_ptr<PlaneWave> function_evaluator = boost::shared_ptr<PlaneWave>(new PlaneWave(wavenumber,angle*M_PI));
      ierr = analytical_bc_real.setApproxOps(m_field,"rePRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::REAL); CHKERRQ(ierr);
      ierr = analytical_bc_imag.setApproxOps(m_field,"imPRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::IMAG); CHKERRQ(ierr);
      Dirichlet_bc_set = true;

    }

    break;

    case HARD_CYLINDER_SCATTER_WAVE:

    {

      boost::shared_ptr<HardCylinderScatterWave> function_evaluator = boost::shared_ptr<HardCylinderScatterWave>(new HardCylinderScatterWave(wavenumber));
      ierr = analytical_bc_real.setApproxOps(m_field,"rePRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::REAL); CHKERRQ(ierr);
      ierr = analytical_bc_imag.setApproxOps(m_field,"imPRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::IMAG); CHKERRQ(ierr);
      Dirichlet_bc_set = true;


    }

    break;

    case SOFT_CYLINDER_SCATTER_WAVE:

    {

      boost::shared_ptr<SoftCylinderScatterWave> function_evaluator = boost::shared_ptr<SoftCylinderScatterWave>(new SoftCylinderScatterWave(wavenumber));
      ierr = analytical_bc_real.setApproxOps(m_field,"rePRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::REAL); CHKERRQ(ierr);
      ierr = analytical_bc_imag.setApproxOps(m_field,"imPRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::IMAG); CHKERRQ(ierr);
      Dirichlet_bc_set = true;

    }

    break;

    case INCIDENT_WAVE:

    {

      boost::shared_ptr<IncidentWave> function_evaluator =
      boost::shared_ptr<IncidentWave>(new IncidentWave(wavenumber,wave_direction,isRayleigh,aTtenuation,helmholtz_element.globalParameters.rAdius.first,helmholtz_element.globalParameters.complexWaveNumber.first,
        helmholtz_element.globalParameters.fRequency.first,helmholtz_element.globalParameters.vElocity.first,helmholtz_element.globalParameters.transmissionCoefficient.first,power_of_incident_wave));
      ierr = analytical_bc_real.setApproxOps(m_field,"rePRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::REAL); CHKERRQ(ierr);
      ierr = analytical_bc_imag.setApproxOps(m_field,"imPRES",analytical_bc_tris,function_evaluator,GenericAnalyticalSolution::IMAG); CHKERRQ(ierr);
      Dirichlet_bc_set = true;

    }

    break;

    case NO_ANALYTICAL_SOLUTION:

    {
      // Dirichlet_bc_set = false;
    }

    break;

  }

  // Analytical boundary conditions
  AnalyticalDirichletHelmholtzBC::DirichletBC analytical_ditihlet_bc_real(m_field,"rePRES",A,T,F);
  AnalyticalDirichletHelmholtzBC::DirichletBC analytical_ditihlet_bc_imag(m_field,"imPRES",A,T,F);


  PetscBool monochromatic_wave = PETSC_TRUE;
  ierr = PetscOptionsGetBool(PETSC_NULL,PETSC_NULL,"-monochromatic_wave",&monochromatic_wave,NULL); CHKERRQ(ierr);

  PetscBool add_incident_wave = PETSC_FALSE;
  ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-add_incident_wave",&add_incident_wave,NULL); CHKERRQ(ierr);
  if(add_incident_wave) {

    // define problem
    ierr = m_field.add_problem("INCIDENT_WAVE"); CHKERRQ(ierr);
    // set finite elements for problem
    ierr = m_field.modify_problem_add_finite_element("INCIDENT_WAVE","HELMHOLTZ_RERE_FE"); CHKERRQ(ierr);
    // set refinment level for problem
    ierr = m_field.modify_problem_ref_level_add_bit("INCIDENT_WAVE",bit_level0); CHKERRQ(ierr);

    // build porblems
    if(is_partitioned) {
      // if mesh is partitioned
      ierr = m_field.build_problem_on_distributed_mesh("INCIDENT_WAVE",true); CHKERRQ(ierr);
      ierr = m_field.partition_finite_elements("INCIDENT_WAVE",true); CHKERRQ(ierr);
    } else {
      // if not partitioned mesh is load to all processes
      ierr = m_field.build_problem("INCIDENT_WAVE",true); CHKERRQ(ierr);
      ierr = m_field.partition_problem("INCIDENT_WAVE"); CHKERRQ(ierr);
      ierr = m_field.partition_finite_elements("INCIDENT_WAVE"); CHKERRQ(ierr);
      }
    ierr = m_field.partition_ghost_dofs("INCIDENT_WAVE"); CHKERRQ(ierr);

  }


  KSP solver;
  ierr = KSPCreate(PETSC_COMM_WORLD,&solver); CHKERRQ(ierr);

  if(monochromatic_wave) {

    if(Dirichlet_bc_set) {

      {

        map<int,PlaneIncidentWaveSacttrerData>::iterator mit = planeWaveScatterData.begin();

        boost::shared_ptr<IncidentWave> function_evaluator;
        boost::shared_ptr<IncidentWavePointSource> functionEvaluator;
        for(;mit!=planeWaveScatterData.end();mit++) {

          if(!helmholtz_element.globalParameters.sourceCoordinate.second) {
            if(helmholtz_element.globalParameters.isRadiation.first) {
              // note negative field, scatter field should cancel incident wave
              function_evaluator = boost::shared_ptr<IncidentWave>(
                new IncidentWave(wavenumber,wave_direction,isRayleigh,aTtenuation,helmholtz_element.globalParameters.rAdius.first,helmholtz_element.globalParameters.complexWaveNumber.first,
                  helmholtz_element.globalParameters.fRequency.first,helmholtz_element.globalParameters.vElocity.first,helmholtz_element.globalParameters.transmissionCoefficient.first,-helmholtz_element.globalParameters.amplitudeOfIncidentWaveReal.first)
              );
            } else {
              function_evaluator = boost::shared_ptr<IncidentWave>(
                new IncidentWave(wavenumber,wave_direction,isRayleigh,aTtenuation,helmholtz_element.globalParameters.rAdius.first,helmholtz_element.globalParameters.complexWaveNumber.first,
                  helmholtz_element.globalParameters.fRequency.first,helmholtz_element.globalParameters.vElocity.first,helmholtz_element.globalParameters.transmissionCoefficient.first,helmholtz_element.globalParameters.amplitudeOfIncidentWaveReal.first)
              );
            }
            ierr = analytical_bc_real.setApproxOps(
              m_field,"rePRES",mit->second.tRis,function_evaluator,GenericAnalyticalSolution::REAL
            ); CHKERRQ(ierr);
            ierr = analytical_bc_imag.setApproxOps(
              m_field,"imPRES",mit->second.tRis,function_evaluator,GenericAnalyticalSolution::IMAG
            ); CHKERRQ(ierr);
          } else {
            if(helmholtz_element.globalParameters.isRadiation.first) {
              // note negative field, scatter field should cancel incident wave
              functionEvaluator = boost::shared_ptr<IncidentWavePointSource>(
                new IncidentWavePointSource(wavenumber,helmholtz_element.globalParameters.sourceCoordinate.first,isRayleigh,aTtenuation,helmholtz_element.globalParameters.rAdius.first,helmholtz_element.globalParameters.complexWaveNumber.first,
                  helmholtz_element.globalParameters.fRequency.first,helmholtz_element.globalParameters.vElocity.first,helmholtz_element.globalParameters.transmissionCoefficient.first,-helmholtz_element.globalParameters.amplitudeOfIncidentWaveReal.first)
              );
            } else {
              functionEvaluator = boost::shared_ptr<IncidentWavePointSource>(
                new IncidentWavePointSource(wavenumber,helmholtz_element.globalParameters.sourceCoordinate.first,isRayleigh,aTtenuation,helmholtz_element.globalParameters.rAdius.first,helmholtz_element.globalParameters.complexWaveNumber.first,
                  helmholtz_element.globalParameters.fRequency.first,helmholtz_element.globalParameters.vElocity.first,helmholtz_element.globalParameters.transmissionCoefficient.first,helmholtz_element.globalParameters.amplitudeOfIncidentWaveReal.first)
              );
            }
            ierr = analytical_bc_real.setApproxOps(
              m_field,"rePRES",mit->second.tRis,functionEvaluator,GenericAnalyticalSolution::REAL
            ); CHKERRQ(ierr);
            ierr = analytical_bc_imag.setApproxOps(
              m_field,"imPRES",mit->second.tRis,functionEvaluator,GenericAnalyticalSolution::IMAG
            ); CHKERRQ(ierr);
          }

        }

      }
      // Solve for analytical Dirichlet bc dofs
      ierr = analytical_bc_real.setProblem(m_field,"BCREAL_PROBLEM"); CHKERRQ(ierr);
      ierr = analytical_bc_imag.setProblem(m_field,"BCIMAG_PROBLEM"); CHKERRQ(ierr);
      ierr = analytical_bc_real.solveProblem(
        m_field,"BCREAL_PROBLEM","BCREAL_FE",analytical_ditihlet_bc_real,bc_dirichlet_tris
      ); CHKERRQ(ierr);
      ierr = analytical_bc_imag.solveProblem(
        m_field,"BCIMAG_PROBLEM","BCIMAG_FE",analytical_ditihlet_bc_imag,bc_dirichlet_tris
      ); CHKERRQ(ierr);

      ierr = analytical_bc_real.destroyProblem(); CHKERRQ(ierr);
      ierr = analytical_bc_imag.destroyProblem(); CHKERRQ(ierr);

    }

    /* calculate the interior problem to set the fulid velocity */
    if(helmholtz_element.globalParameters.fRequency.second) {
      // const complex< double > i( 0.0, 1.0 );
      complex< double > wave_number = (2*M_PI*helmholtz_element.globalParameters.fRequency.first / helmholtz_element.globalParameters.vElocity.first);
      // FIXME Why it is not working with first method ?
      // map<int,helmholtz_element::VolumeData>::iterator vit = helmholtz_element.volumeData.begin();
      //
      // for(;vit != helmholtz_element.volumeData.end();vit++) {
      //   vit->second.waveNumber = wave_number;
      // }

      for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,BLOCKSET,it)) {
        if(it->getName().compare(0,13,"MAT_HELMHOLTZ") == 0) {
          helmholtz_element.volumeData[it->getMeshsetId()].waveNumber = wave_number;
        }
      }

    }

    // Zero vectors
    ierr = VecZeroEntries(T); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(T,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(T,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecZeroEntries(F); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = MatZeroEntries(A); CHKERRQ(ierr);

    // Assemble problem
    if(Dirichlet_bc_set) {
      ierr = m_field.problem_basic_method_preProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_real); CHKERRQ(ierr);
      ierr = m_field.problem_basic_method_preProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_imag); CHKERRQ(ierr);
    }

    ierr = helmholtz_element.calculateA("ACOUSTIC_PROBLEM"); CHKERRQ(ierr);
    ierr = helmholtz_element.calculateF("ACOUSTIC_PROBLEM"); CHKERRQ(ierr);
    // ierr = MatView(A,PETSC_VIEWER_DRAW_WORLD);
    // std::string wait;
    // std::cin >> wait;

    if(Dirichlet_bc_set) {
      ierr = m_field.problem_basic_method_postProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_real); CHKERRQ(ierr);
      ierr = m_field.problem_basic_method_postProcess("ACOUSTIC_PROBLEM",analytical_ditihlet_bc_imag); CHKERRQ(ierr);
    }

    // FIXME Point Source Right hand sides for vertex source for Total Acoustic potential
    // \Phi = \Phi_{S} + \Phi_{I}
    //  const Problem *problem_ptr;
    // ierr = m_field.get_problem("ACOUSTIC_PROBLEM",&problem_ptr); CHKERRQ(ierr);
    // for(_IT_NUMEREDDOF_ROW_BY_NAME_ENT_PART_FOR_LOOP_(problem_ptr,"rePRES",node,m_field.getCommRank(),dit)) {
      // int global_idx = dit-> getPetscGlobalDofIdx();
      // cerr << "\n global_idx = \n\n " << global_idx << endl;
      // ierr = VecSetValue(F,global_idx,helmholtz_element.globalParameters.amplitudeOfIncidentWaveReal.first,INSERT_VALUES); CHKERRQ(ierr);
    // }


    ierr = VecAssemblyBegin(F); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(F); CHKERRQ(ierr);
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    // if(monochromatic_wave) {
      /* get the sparsity information of matrix */
      MatInfo info;
      // double nonZeroes;
      ierr = MatGetInfo(A,MAT_GLOBAL_SUM,&info);
      // nonZeroes = info.nz_allocated;
      // MatGetSize(A,&m,&n);
      if(!pcomm->rank()) {
        printf("info.nz_used %g\n",info.nz_used);
        printf("info.memory %g\n",info.memory/2073741824);
        // printf("info.factor_mallocs %g\n",info.factor_mallocs);
        // printf("info.assemblies %g\n",info.assemblies);
      }
    // }
    // ierr = VecView(F,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

    ierr = VecScale(F,-1); CHKERRQ(ierr);
    // Solve problem
    ierr = KSPSetOperators(solver,A,A); CHKERRQ(ierr);
    ierr = KSPSetFromOptions(solver); CHKERRQ(ierr);
    ierr = KSPSetUp(solver); CHKERRQ(ierr);

    // /* flops count */
    // PetscLogEvent USER_EVENT;
    // ierr = PetscLogEventRegister("User event",0,&USER_EVENT); CHKERRQ(ierr);
    // ierr = PetscLogEventBegin(USER_EVENT,solver,0,0,0); CHKERRQ(ierr);
    // /* flops count */

    ierr = KSPSolve(solver,F,T); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(T,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(T,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

    // /* flops count */
    // PetscScalar flops,user_flops;
    // // ierr = PetscGetFlops(&flops); CHKERRQ(ierr);
    // ierr = PetscLogFlops(user_flops); CHKERRQ(ierr);
    // ierr = PetscLogEventEnd(USER_EVENT,solver,0,0,0); CHKERRQ(ierr);
    // // PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Total flops %d \n",pcomm->rank(),flops);
    // PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Total Log flops %d \n",pcomm->rank(),user_flops);
    // /* flops count */



    //Save data on mesh
    if(is_partitioned) {

      // no need for global communication
      ierr = m_field.set_local_ghost_vector("ACOUSTIC_PROBLEM",ROW,T,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);

    } else {

      ierr = m_field.set_global_ghost_vector("ACOUSTIC_PROBLEM",ROW,T,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);

    }

  } else {

    // define problem
    ierr = m_field.add_problem("PRESSURE_IN_TIME"); CHKERRQ(ierr);
    // set finite elements for problem
    ierr = m_field.modify_problem_add_finite_element("PRESSURE_IN_TIME","PRESSURE_FE"); CHKERRQ(ierr);
    // set refinment level for problem
    ierr = m_field.modify_problem_ref_level_add_bit("PRESSURE_IN_TIME",bit_level0); CHKERRQ(ierr);

    // build porblems
    if(is_partitioned) {
      // if mesh is partitioned
      ierr = m_field.build_problem_on_distributed_mesh("PRESSURE_IN_TIME",true); CHKERRQ(ierr);
      ierr = m_field.partition_finite_elements("PRESSURE_IN_TIME",true); CHKERRQ(ierr);
    } else {
      // if not partitioned mesh is load to all processes
      ierr = m_field.build_problem("PRESSURE_IN_TIME",true); CHKERRQ(ierr);
      ierr = m_field.partition_problem("PRESSURE_IN_TIME"); CHKERRQ(ierr);
      ierr = m_field.partition_finite_elements("PRESSURE_IN_TIME"); CHKERRQ(ierr);
    }
    ierr = m_field.partition_ghost_dofs("PRESSURE_IN_TIME"); CHKERRQ(ierr);
    /* FIXME Could the mfield.set_field_order used in the struct TimeSeries.hpp ? passed as
       argument */
    TimeSeries time_series(m_field,helmholtz_element,
      analytical_bc_real,analytical_bc_imag,
      analytical_ditihlet_bc_real,analytical_ditihlet_bc_imag,
      Dirichlet_bc_set,bc_dirichlet_tris,planeWaveScatterData);

    ierr = time_series.readData(); CHKERRQ(ierr);
    ierr = time_series.createPressureSeries(T); CHKERRQ(ierr);
    /* FIXME It is probably need to change the order for real and
    imaginary fields here in forwardSpaceDft();, before solver solves the incident wave
    field problem.... But the time steps, how to determined the k ?
    wave_number = 2*M_PI*t/signal_length ? */
    ierr = time_series.forwardSpaceDft(); CHKERRQ(ierr);
    // ierr = time_series.saveFrequencyData(); // save the frequency data to file
    ierr = time_series.solveForwardDFT(solver,A,F,T); CHKERRQ(ierr);
    ierr = time_series.pressureForwardDft(); CHKERRQ(ierr);
    ierr = time_series.pressureInverseDft(); CHKERRQ(ierr);
    ierr = time_series.generateReferenceElementMesh(); CHKERRQ(ierr);
    ierr = time_series.saveResults(); CHKERRQ(ierr);
    ierr = time_series.destroyPressureSeries(); CHKERRQ(ierr);

  }
  // PetscInt its;
  // ierr = KSPGetIterationNumber(solver,&its);
  ierr = PetscTime(&v2);CHKERRQ(ierr);
  ierr = PetscGetCPUTime(&t2);CHKERRQ(ierr);
  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Total Rank %d Time = %f S CPU Time = %f S \n",pcomm->rank(),v2-v1,t2-t1);
  ierr = PetscMemoryGetCurrentUsage(&m2); CHKERRQ(ierr);
  /* PetscMemoryGetMaximumUsage */
  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Total Rank %d Memory usage = %f GBytes \n",pcomm->rank(),(m2-m1)/1073741824);



  //Vec P,M;
  //ierr = m_field.VecCreateGhost("EX1_PROBLEM",COL,&M); CHKERRQ(ierr);
  //ierr = VecDuplicate(M,&P); CHKERRQ(ierr);

  //ierr = m_field.set_local_ghost_vector("EX1_PROBLEM",COL,M,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
  //ierr = m_field.set_other_global_ghost_vector("EX1_PROBLEM","reEX","imEX",COL,P,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

  //double nrm2_M;
  //ierr = VecNorm(M,NORM_2,&nrm2_M);  CHKERRQ(ierr);

  //Vec V;
  //ierr = m_field.VecCreateGhost("ACOUSTIC_PROBLEM",COL,&V); CHKERRQ(ierr);
  //ierr = m_field.set_local_ghost_vector("ACOUSTIC_PROBLEM",COL,V,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

  //VecScatter scatter_real,scatter_imag;

  //ierr = m_field.VecScatterCreate(V,"ACOUSTIC_PROBLEM","rePRES",COL,M,"EX1_PROBLEM","reEX",COL,&scatter_real); CHKERRQ(ierr);

  //ierr = m_field.VecScatterCreate(V,"ACOUSTIC_PROBLEM","imPRES",COL,P,"EX1_PROBLEM","reEX",COL,&scatter_imag); CHKERRQ(ierr);

  //VecScale(V,-1);

  //ierr = VecScatterBegin(scatter_real,V,M,ADD_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
  //ierr = VecScatterEnd(scatter_real,V,M,ADD_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);

  //double nrm2_ErrM;
  //ierr = VecNorm(M,NORM_2,&nrm2_ErrM);  CHKERRQ(ierr);
  //PetscPrintf(PETSC_COMM_WORLD,"L2 relative error on real field of acoustic problem %6.4e\n",(nrm2_ErrM)/(nrm2_M));


  //ierr = VecDestroy(&M); CHKERRQ(ierr);
  //ierr = VecDestroy(&P); CHKERRQ(ierr);
  //ierr = VecDestroy(&V); CHKERRQ(ierr);



  if(monochromatic_wave) {

    if(add_incident_wave) {
      /* FIXME problem with HELMHOLTZ_RERE_FE since this element contains 2 fields,and
      create big matrix did not consistent with field_approximation.hpp calculation method. */

      if(helmholtz_element.globalParameters.sourceCoordinate.second) {
        IncidentWavePointSource function_evaluator(wavenumber,helmholtz_element.globalParameters.sourceCoordinate.first,isRayleigh,aTtenuation,helmholtz_element.globalParameters.rAdius.first,helmholtz_element.globalParameters.complexWaveNumber.first,
          helmholtz_element.globalParameters.fRequency.first,helmholtz_element.globalParameters.vElocity.first,helmholtz_element.globalParameters.transmissionCoefficient.first,power_of_incident_wave);
        ierr = solve_problem(m_field,"INCIDENT_WAVE","HELMHOLTZ_RERE_FE","rePRES","imPRES",
          ADD_VALUES,function_evaluator,is_partitioned); CHKERRQ(ierr);
      } else {
        if(helmholtz_element.globalParameters.complexWaveNumber.second) {
          wave_direction = helmholtz_element.globalParameters.waveOscilationDirection.first;
        }
        IncidentWave function_evaluator(wavenumber,wave_direction,isRayleigh,aTtenuation,helmholtz_element.globalParameters.rAdius.first,helmholtz_element.globalParameters.complexWaveNumber.first,
          helmholtz_element.globalParameters.fRequency.first,helmholtz_element.globalParameters.vElocity.first,helmholtz_element.globalParameters.transmissionCoefficient.first,power_of_incident_wave);
        ierr = solve_problem(m_field,"INCIDENT_WAVE","HELMHOLTZ_RERE_FE","rePRES","imPRES",
          ADD_VALUES,function_evaluator,is_partitioned); CHKERRQ(ierr);
      }

    }

    if(is_partitioned) {

      rval = moab.write_file("fe_solution.h5m","MOAB","PARALLEL=WRITE_PART"); CHKERRQ_MOAB(rval);

    } else {

      if(!pcomm->rank()) {
        rval = moab.write_file("fe_solution.h5m"); CHKERRQ_MOAB(rval);
      }

    }



    PetscBool save_postproc_mesh = PETSC_TRUE;
    ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-save_postproc_mesh",&save_postproc_mesh,NULL); CHKERRQ(ierr);
    if(save_postproc_mesh) {

      PostProcVolumeOnRefinedMesh post_proc(m_field);
      // PetscBool pressure_field = PETSC_FALSE;
      /* FIXME post-process the pressure field from acoustic potential */
      // ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-pressure_field",&pressure_field,NULL); CHKERRQ(ierr);
      ierr = post_proc.generateReferenceElementMesh(); CHKERRQ(ierr);
      // PetscScalar scaling_pressure;
      // if(pressure_field) {
      //   scaling_pressure = - 2.0 * M_PI * helmholtzElement.globalParameters.fRequency.first * helmholtzElement.globalParameters.dEnsity.first;
      //   ierr = VecScale(p_scatter_wave_real,scaling_pressure);
      // }
      ierr = post_proc.addFieldValuesPostProc("rePRES"); CHKERRQ(ierr);
      // if(pressure_field) {
      //   scaling_pressure *= - 1.0;
      //   ierr = VecScale(p_scatter_wave_imag,scaling_pressure);
      // }
      ierr = post_proc.addFieldValuesPostProc("imPRES"); CHKERRQ(ierr);
      ierr = post_proc.addFieldValuesPostProc("MESH_NODE_POSITIONS"); CHKERRQ(ierr);
      /* Particle Velocity  The velocity of the
         movement of these material particles is called particle velocity */
      // ierr = post_proc.addFieldValuesGradientPostProc("rePRES","reVELOCITY"); CHKERRQ(ierr);
      // ierr = post_proc.addFieldValuesGradientPostProc("imPRES","imVELOCITY"); CHKERRQ(ierr);

      /* the potential and velocity of analytical solution fields */
      if(m_field.check_field("reEX") && m_field.check_field("imEX")) {
        ierr = post_proc.addFieldValuesPostProc("reEX"); CHKERRQ(ierr);
        ierr = post_proc.addFieldValuesPostProc("imEX"); CHKERRQ(ierr);
        // ierr = post_proc.addFieldValuesGradientPostProc("reEX","reVELOCITYex"); CHKERRQ(ierr);
        // ierr = post_proc.addFieldValuesGradientPostProc("imEX","imVELOCITYex"); CHKERRQ(ierr);
      }

      PetscBool reynolds_stress = PETSC_FALSE;
      ierr = PetscOptionsGetBool(PETSC_NULL,NULL,"-reynolds_stress",&reynolds_stress,NULL); CHKERRQ(ierr);

      if(reynolds_stress) {

        post_proc.getOpPtrVector().push_back(
          new ReynoldsStress(
            m_field,
            helmholtz_element,
            post_proc.postProcMesh,
            post_proc.mapGaussPts,
            "imPRES",
            "imPRES",
            post_proc.commonData));

        post_proc.getOpPtrVector().push_back(
          new ReynoldsStress(
            m_field,
            helmholtz_element,
            post_proc.postProcMesh,
            post_proc.mapGaussPts,
            "rePRES",
            "rePRES",
            post_proc.commonData));

      }

      struct OpPostProcOrder: public VolumeElementForcesAndSourcesCore::UserDataOperator {

        string FieldName;
        moab::Interface &postProcMesh;
        vector<EntityHandle> &mapGaussPts;

        OpPostProcOrder(
          const string& FieldName,moab::Interface &post_proc_mesh,vector<EntityHandle> &map_gauss_pts):VolumeElementForcesAndSourcesCore::UserDataOperator(FieldName,ForcesAndSurcesCore::UserDataOperator::OPROW),
          postProcMesh(post_proc_mesh),
          mapGaussPts(map_gauss_pts) { }

        PetscErrorCode doWork(
          int side,
          EntityType type,
          DataForcesAndSurcesCore::EntData &data) {

          PetscFunctionBegin;
          MoABErrorCode rval;
          int nb_gauss_pts = data.getN().size1();

          int order = const_cast<VolumeElementForcesAndSourcesCore*>(getVolumeFE())->getMaxDataOrder();
          // cerr << "\n order = \n" << order << endl;
          Tag th_post_proc_order;
          int tag_length = 1;
          // double def_VAL[tag_length];
          // bzero(def_VAL,tag_length*sizeof(double));
          double def_VAL = 1;
          rval = postProcMesh.tag_get_handle(
            "ORDER_ADAPT",tag_length,MB_TYPE_INTEGER,th_post_proc_order,MB_TAG_CREAT|MB_TAG_SPARSE,&def_VAL
          ); CHKERRQ_MOAB(rval);

          for(int gg = 0;gg<nb_gauss_pts;gg++) {
            rval = postProcMesh.tag_set_data(
              th_post_proc_order,&mapGaussPts[gg],1,&order
            ); CHKERRQ_MOAB(rval);
          }

          PetscFunctionReturn(0);
        }

      };

      if(aDaptivity) {
        post_proc.getOpPtrVector().push_back(
          new OpPostProcOrder("rePRES",
          post_proc.postProcMesh,
          post_proc.mapGaussPts)
        );
      }
      // ierr = m_field.loop_finite_elements("ACOUSTIC_PROBLEM","HELMHOLTZ_IMIM_FE",post_proc); CHKERRQ(ierr);
      ierr = m_field.loop_finite_elements("ACOUSTIC_PROBLEM","HELMHOLTZ_RERE_FE",post_proc); CHKERRQ(ierr);

      // if(!pcomm->rank()) {
        ierr = post_proc.writeFile("fe_solution_mesh_post_proc.h5m"); CHKERRQ(ierr);
      // }

    }


  }

  // Destroy the KSP solvers
  ierr = MatDestroy(&A); CHKERRQ(ierr);
  ierr = VecDestroy(&F); CHKERRQ(ierr);
  ierr = VecDestroy(&T); CHKERRQ(ierr);
  ierr = KSPDestroy(&solver); CHKERRQ(ierr);

  ierr = PetscOptionsEnd(); CHKERRQ(ierr);
  ierr = PetscFinalize(); CHKERRQ(ierr);

  return 0;

}
