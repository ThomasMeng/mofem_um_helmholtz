#!/bin/sh
# " chmod -x FILE_NAME " before run the bach script
# DIR_NAME="error_mesh_file"
NB_PROC="8"
ERROR_TYPE="l2"
WAVE_NUMBER="5"
ODISP="1"
ORDER="1"

FIELD_SPLIT="true"

# for WAVE_NUMBER in {3,5,10}
# do
partition="false"
DUFFY="true"
LOBATTO="true"

file1="impinging_cylinder_$ODISP.cub"
# file2="analytical_solution.h5m"
#ERROR_TYPE="h1"

# BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
# SRC_PATH="@CMAKE_CURRENT_SOURCE_DIR@"
# BIN_PATH="./"
# SRC_PATH="./src"

MPIRUN="mpirun -np $NB_PROC"

# cd $BIN_PATH

# for WAVE_NUMBER in 3 5 10 15
# do




rm -rf error_hard_cylinder_field_split_true_h_*_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt | rm -rf hard_cylinder_field_split_true_h_*_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
rm -rf error_hard_cylinder_field_split_false_h_*_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt | rm -rf hard_cylinder_field_split_false_h_*_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt









for ODISP in "1" "2" "3" "4" "5" "6" "7"
do



# echo "Start P enrichment analytical, field split is $FIELD_SPLIT !!!!! best_approximation calculation with $BEST_NB_PRC processes, mesh h $ODISP and order 8 and wavenumber $WAVE_NUMBER, error type $ERROR_TYPE"
#
# #BEGIN analytical solution
# mpirun -np $BEST_NB_PRC ./best_approximation \
# -my_file impinging_cylinder_$ODISP.cub \
# -my_is_partitioned $partition \
# -wave_number $WAVE_NUMBER \
# -wave_direction 1,0,0 \
# -analytical_solution_type hard_cylinder_incident_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 8 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -lobatto true



FIELD_SPLIT="true"

~/build_moFEM1/usr/tools/mofem_part -my_file impinging_cylinder_$ODISP.cub -my_nparts 8


# echo LOG: sound hard cylinder P convergence test
echo | tee -a hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# echo | tee -a error_hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
echo " nze DOFs memory unit time CPUtime              "| tee -a hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# echo "${ERROR_TYPE}relative error                "| tee -a error_hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt



echo | tee -a hard_cylinder_field_split_false_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# echo | tee -a error_hard_cylinder_field_split_false_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
echo " nze DOFs memory unit time CPUtime              "| tee -a hard_cylinder_field_split_false_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# echo "${ERROR_TYPE}relative error                "| tee -a error_hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
echo "Start p convergence test for sound hard cylinder ..."


for ORDER in "1" "2" "3" "4" "5" "6" "7" "8"
do

# if [ $partition = true ]; then mbpart -t -p PARTKWAY $NB_PROC  impinging_cylinder.cub  impinging_cylinder_parts$NB_PROC.h5m; fi
#
# if [ $partition = true ]
# then file1="impinging_cylinder_parts$NB_PROC.h5m" && file2="best_solution.h5m"
# fi

# for ORDER in {1..7..1};
# for ORDER in `seq 1 8`;
# do
 # let NB_PROC+=${ORDER}
 # NB_PROC=`expr $ORDER + 1`

NB_PROC=`expr $ORDER`
 ###########################
# if [ "$ORDER" -gt "$ODISP" ]
# then NB_PROC=`expr $ORDER + 0`
# else NB_PROC=`expr $ODISP`
# fi
#  #NB_PROC=$ORDER
# MPIRUN="mpirun -np $NB_PROC"
################################

 #To use partitioned mesh, first mbpart -t -p PARTKWAY 4  cylinder4.cub  cylinder4_4parts.h5m,
#   let NB_PROC=${ORDER}+1
#   #echo $NB_PROC
  # #BEGIN analytical solution
  # $MPIRUN $BIN_PATH/best_approximation \
  # -my_file $file1 \
  # -my_is_partitioned $partition \
  # -wave_number $WAVE_NUMBER \
  # -wave_direction 1,0,0 \
  # -analytical_solution_type hard_cylinder_incident_wave \
  # -save_postproc_mesh false \
  # -ksp_type fgmres \
  # -pc_type lu \
  # -pc_factor_mat_solver_package mumps \
  # -ksp_monitor \
  # -my_order $ORDER \
  # -my_max_post_proc_ref_level 0 \
  # -add_incident_wave false \

#
#BEGIN numerical solution
#echo "                      "| tee -a hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
#echo "${ERROR_TYPE}relative error                "| tee -a error_hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
#echo "                                   "| tee -a hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
#echo "Start numerical calculation with $NB_PROC processes, mesh h$ODISP and order $ORDER ..." | tee -a hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt

# if [ ${FIELD_SPLIT} = false ]
# then

FIELD_SPLIT="true"




echo "Start numerical calculation field split is $FIELD_SPLIT !!! with $NB_PROC processes, mesh h $ODISP and order p $ORDER ..."
mpirun -np 8 ./fe_field_split \
-my_file out.h5m \
-my_is_partitioned true \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-save_postproc_mesh false \
-my_order $ORDER \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-duffy $DUFFY \
-lobatto $LOBATTO \
-fieldsplit_1_ksp_type fgmres \
-fieldsplit_1_pc_type lu \
-fieldsplit_1_pc_factor_mat_solver_package superlu_dist \
-fieldsplit_1_ksp_max_it 30 \
-fieldsplit_0_ksp_type fgmres \
-fieldsplit_0_pc_type lu \
-fieldsplit_0_pc_factor_mat_solver_package superlu_dist \
-fieldsplit_0_ksp_max_it 30 \
-pc_fieldsplit_type schur \
-ksp_type fgmres \
-ksp_atol 1e-8 \
-ksp_rtol 1e-8 \
-ksp_max_it 800 \
-ksp_monitor \
-add_incident_wave false 2>&1 |
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$4,$6,$8,$10,$15,$25,$32,$37}' | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$15,$22,$23,$27,$28}' | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$5,$9,$16,$21}' | tee -a -i hard_cylinder_field_split_true_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
echo "   "





FIELD_SPLIT="false"


echo "Start numerical calculation field split is $FIELD_SPLIT !!! with $NB_PROC processes, mesh h $ODISP and order p $ORDER ..."
mpirun -np $NB_PROC ./fe_approximation \
-my_file impinging_cylinder_$ODISP.cub \
-my_is_partitioned $partition \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package superlu_dist \
-ksp_monitor \
-my_order $ORDER \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-duffy $DUFFY \
-lobatto $LOBATTO \
-add_incident_wave false 2>&1 |
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|info.memory\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$4,$6,$8,$10,$15,$25,$32,$37}' | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
# echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$15,$22,$23,$27,$28}' | tee -a -i hard_cylinder_field_split_${FIELD_SPLIT}_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$29}' | tee -a -i hard_cylinder_field_split_false_h_${ODISP}_k${WAVE_NUMBER}_lobatto_${LOBATTO}.txt
echo "   "




done

done
