#!/bin/sh
BEST_NB_PRC="8"
# " chmod +x FILE_NAME " before run the bach script
# DIR_NAME="error_mesh_file"
NB_PROC="8"
ERROR_TYPE="l2"  #change the error type here to h1
WAVE_NUMBER="0"
ODISP="1"
# ORDER="adaptivity"
ADAPTIVITY="true"

# for WAVE_NUMBER in {3,5,10}
# do
partition="false"
DUFFY="true"
LOBATTO="true"
ERRORLV="0"
PRIORI_TYPE="1"
# file1="impinging_droplet_$ODISP.cub"
# file2="analytical_solution.h5m"
#ERROR_TYPE="h1"

# BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
# SRC_PATH="@CMAKE_CURRENT_SOURCE_DIR@"
# BIN_PATH="./"
# SRC_PATH="./src"

# MPIRUN="mpirun -np $NB_PROC"

# cd $BIN_PATH



# echo LOG: plane wave guide P convergence test
# echo | tee -a wave_guide_p.txt
# echo "Start p convergence test for plane wave guide ..." | tee -a wave_guide_p.txt
# $MPIRUN $BIN_PATH/best_approximation \
# -my_file plane_wave_cube.cub \
# -my_is_partitioned false \
# -wave_number $WAVE_NUMBER \
# -wave_direction 0.7071,0.7071,0 \
# -analytical_solution_type plane_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package mumps \
# -ksp_monitor \
# -my_order 7 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -wave_guide_angle 45
#
# for ODISP in {1..3..1};
# do
#   let NB_PROC=${ODISP}+1
#   #echo $NB_PROC
#   #BEGIN analytical solution
#   # $MPIRUN $BIN_PATH/best_approximation \
#   # -my_file plane_wave_cube.cub \
#   # -my_is_partitioned false \
#   # -wave_number $WAVE_NUMBER \
#   # -wave_direction 0.7071,0.7071,0 \
#   # -analytical_solution_type plane_wave \
#   # -save_postproc_mesh false \
#   # -ksp_type fgmres \
#   # -pc_type lu \
#   # -pc_factor_mat_solver_package mumps \
#   # -ksp_monitor \
#   # -my_order $ODISP \
#   # -my_max_post_proc_ref_level 0 \
#   # -add_incident_wave false \
#   # -wave_guide_angle 45
#
#   #BEGIN numerical solution
#   $MPIRUN $BIN_PATH/fe_approximation \
#   -my_file analytical_solution.h5m \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 0.7071,0.7071,0 \
#   -analytical_solution_type plane_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps \
#   -ksp_monitor \
#   -my_order $ODISP \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -wave_guide_angle 45 |
#   grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =" | tee -a -i wave_guide_p.txt
#   #BEGIN error calculation
#
#   $MPIRUN $BIN_PATH/error_norm \
#   -my_file $BIN_PATH/fe_solution.h5m \
#   -norm_type $ERROR_TYPE \
#   -relative_error false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps  \
#   -ksp_monitor  \
#   -my_order 1 \
#   -my_max_post_proc_ref_level 0 \
#   -save_postproc_mesh true \
#   2>&1 | grep --line-buffered -i " $ERROR_TYPE realtive error " | tee -a -i wave_guide_p.txt
#   #RENAME files inot their order and wave number.
#   mbconvert norm_error.h5m norm_error.vtk
#   mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ODISP}.vtk
# done
#
# #SAVE the results to different directory
# mkdir ${DIR_NAME}_${ERROR_TYPE}_waveguide | mv norm_error_k${WAVE_NUMBER}_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_waveguide
# rm -rf h_refinement_test_best_approximation_k_errorlv_*.txt
# rm -rf ${DIR_NAME}_${ERROR_TYPE}_droplet_p
# rm -rf h_refinement_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_*_type_*.txt
rm -rf h_refinement_hard_droplet_k_lobatto_${LOBATTO}_errorlv_*_type_*.txt

# | rm -rf norm_error_k*_order_*.vtk

# rm -rf p_refinement_adaptivity_*_test_best_approximation_k_errorlv_*.txt
# rm -rf ${DIR_NAME}_${ERROR_TYPE}_droplet_p
# rm -rf p_refinement_adaptivity_*_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_*_type_*.txt
rm -rf p_refinement_adaptivity_*_hard_droplet_k_lobatto_${LOBATTO}_errorlv_*_type_*.txt
# rm -rf adaptivity_p_refinement_adaptivity_*_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_*_type_*.txt
rm -rf adaptivity_p_refinement_adaptivity_*_hard_droplet_k_lobatto_${LOBATTO}_errorlv_*_type_*.txt


# for batman in `seq 1 1`;
# do

  #echo $NB_PROC
  #BEGIN analytical solution
# mpirun -np 8 ./best_approximation \
#   -my_file impinging_droplet.cub \
#   -my_is_partitioned false \
#   -wave_number $WAVE_NUMBER \
#   -wave_direction 1,0,0 \
#   -analytical_solution_type hard_droplet_incident_wave \
#   -save_postproc_mesh false \
#   -ksp_type fgmres \
#   -pc_type lu \
#   -pc_factor_mat_solver_package mumps \
#   -ksp_monitor \
#   -my_order 7 \
#   -my_max_post_proc_ref_level 0 \
#   -add_incident_wave false \
#   -amplitude_of_incident_wave 1
# done

# for ODISP in `seq 1 4`;

#Frequencies : 1 5 10 20 30 40 50 70



for ERROR_TYPE in "l2" "h1"
do

if [ $ERROR_TYPE = "h1" ]
then PRIORI_TYPE="2"
fi


# for WAVE_NUMBER in "0.3142" "1.5708" "3.1416" "6.2832" "9.4248" "12.5664" "15.7080"
# 1 5 10 30 50 70
# for WAVE_NUMBER in "1" "3" "6" "8" "15" "20"
# "3" "6" "8" fails

for ERRORLV in "2" "1" "0"
do

# echo LOG: sound hard droplet P convergence test
# echo | tee -a p_refinement_adaptivity_${ADAPTIVITY}_test_best_approximation_k_errorlv_${ERRORLV}.txt
echo | tee -a p_refinement_adaptivity_${ADAPTIVITY}_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# echo | tee -a p_refinement_adaptivity_${ADAPTIVITY}_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo | tee -a adaptivity_p_refinement_adaptivity_true_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# echo | tee -a adaptivity_p_refinement_adaptivity_true_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo | tee -a h_refinement_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# echo | tee -a h_refinement_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo " nze memory time CPUtime              "| tee -a p_refinement_adaptivity_${ADAPTIVITY}_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo " nze memory time CPUtime              "| tee -a h_refinement_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo " avg nze max min DOFs memory unit time CPUtime              "| tee -a adaptivity_p_refinement_adaptivity_true_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# echo "${ERROR_TYPE}relative error                "| tee -a p_refinement_adaptivity_${ADAPTIVITY}_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# echo "${ERROR_TYPE}relative error                "| tee -a h_refinement_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# echo "${ERROR_TYPE}relative error                "| tee -a adaptivity_p_refinement_adaptivity_true_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt



COUNT="0"

# for WAVE_NUMBER in "0.2" "0.4" "0.6" "0.8" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28" "29" "30"
# do

for WAVE_NUMBER in "0" "1083.3" "2166.6" "3249.9" "4333.2" "5416.5" "6499.8" "7583.2" "8666.5" "9749.8" "10833" "11916" "13000" "14083" "15166" "16250" "17333" "18416" "19500" "20583" "21666" "22749" "23833" "24916" "25999" "27083" "28166" "29249" "30333" "31416"
do


# if [ "$WAVE_NUMBER" -gt "6" ]
# then NB_PROC=`expr 8`
# else NB_PROC=`expr 4`
# fi
# # echo NB_PROC = $NB_PROC
#
# # COUNT is the representation for uniform adaptivity vavenumber
# COUNT=`expr $COUNT + 1`





echo "Start convergence test for sound hard droplet ..."

# if [ $partition = true ]; then mbpart -t -p PARTKWAY $NB_PROC  impinging_droplet.cub  impinging_droplet_parts$NB_PROC.h5m; fi

# if [ $partition = true ]
# then file1="impinging_droplet_parts$NB_PROC.h5m" && file2="best_solution.h5m"
# fi



# BEST_NB_PRC=`expr 2 + $ODISP`

# echo "Start P enrichment analytical best_approximation calculation with $BEST_NB_PRC processes, mesh h$ODISP and order 8 and wavenumber $WAVE_NUMBER, error level $ERRORLV"
#
# #BEGIN analytical solution
# mpirun -np $BEST_NB_PRC ./best_approximation \
# -my_file impinging_droplet.cub \
# -my_is_partitioned false \
# -wave_number $WAVE_NUMBER \
# -wave_direction 1,0,0 \
# -analytical_solution_type hard_droplet_scatter_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 8 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -lobatto true 2>&1 |
# echo $(grep --line-buffered -i "Residual norm\|Time =" | sort | uniq) | tee -a -i adaptivity_p_refinement_adaptivity_${ADAPTIVITY}_test_best_approximation_k_errorlv_${ERRORLV}.txt


# for ORDER in {1..7..1};
# for ORDER in `seq 1 8`;
# do
 # let NB_PROC+=${ORDER}
 # NB_PROC=`expr $ORDER + 1`
# if [ "$ORDER" -gt "$ODISP" ]
# then NB_PROC=`expr $ORDER + 1`
# else NB_PROC=`expr $ODISP + 1`
# fi
# $(echo $(( 2 * $ODISP )))
 # NB_PROC=`expr $ODISP + $(echo $(( 2 * $ERRORLV )))`
 MPIRUN="mpirun -np $NB_PROC"


 #To use partitioned mesh, first mbpart -t -p PARTKWAY 4  droplet4.cub  droplet4_4parts.h5m,
#   let NB_PROC=${ORDER}+1
#   #echo $NB_PROC
  # #BEGIN analytical solution
  # $MPIRUN $BIN_PATH/best_approximation \
  # -my_file $file1 \
  # -my_is_partitioned $partition \
  # -wave_number $WAVE_NUMBER \
  # -wave_direction 1,0,0 \
  # -analytical_solution_type hard_droplet_incident_wave \
  # -save_postproc_mesh false \
  # -ksp_type fgmres \
  # -pc_type lu \
  # -pc_factor_mat_solver_package mumps \
  # -ksp_monitor \
  # -my_order $ORDER \
  # -my_max_post_proc_ref_level 0 \
  # -add_incident_wave false \
ADAPTIVITY="true"
#
#BEGIN numerical solution
#echo "                      "| tee -a hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo "${ERROR_TYPE}relative error                "| tee -a error_hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo "                                   "| tee -a hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo "Start numerical calculation with $NB_PROC processes, mesh h$ODISP and order $ORDER ..." | tee -a hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo "Start automatic adaptivity P enrichment numerical calculation with $NB_PROC processes, P Non-uniform ? true, mesh h$ODISP and order $ORDER and wavenumber $WAVE_NUMBER, error level $ERRORLV"
mpirun -np 8 ./fe_approximation \
-my_file non_uniform_droplet.cub \
-my_is_partitioned false \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-material_coefficient1 -1348.8 \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package mumps \
-ksp_monitor \
-my_order 1 \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-duffy true \
-lobatto true \
-adaptivity true \
-error_level $ERRORLV \
-error_type $PRIORI_TYPE \
-add_incident_wave false 2>&1 |
echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$4,$6,$8,$13,$23,$30,$35}' | tee -a -i adaptivity_p_refinement_adaptivity_true_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo "   "
# echo "                       begin error calculation           "
echo " check if data is correct  "
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$25,$29,$30}' | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#BEGIN error calculation   awk '{print $6,$12}'  grep first 10 lines |head -10|
#grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =\|Memory usage =" | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt

# mpirun -np 2 ./error_norm \
# -my_file ./fe_solution.h5m \
# -my_is_partitioned false \
# -norm_type $ERROR_TYPE \
# -relative_error false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 1 \
# -my_max_post_proc_ref_level 0 \
# -save_postproc_mesh false 2>&1 | echo $(grep --line-buffered -i " realtive error ") | awk '{print $6,$12}' | tee -a -i adaptivity_p_refinement_adaptivity_true_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
# #RENAME files inot their order and wave number.
# # mbconvert norm_error.h5m norm_error.vtk |
# # mv norm_error.vtk norm_error_k${WAVE_NUMBER}_order_${ORDER}.vtk

mbpart -t -p PARTKWAY 8 coarse_droplet.cub coarse_droplet_8.h5m

############### START UNIFORM ADAPTIVITY calculation ####################
ADAPTIVITY="false"

echo "Start uniform P enrichment numerical calculation with $NB_PROC processes, P Non-uniform ? ${ADAPTIVITY}.  mesh coarse_droplet.cub and order  7 ($COUNT) and wavenumber $WAVE_NUMBER, error level $ERRORLV"
mpirun -np $NB_PROC ./fe_approximation \
-my_file coarse_droplet_8.h5m \
-my_is_partitioned true \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-material_coefficient1 -1348.8 \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package mumps \
-ksp_monitor \
-my_order 7 \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-duffy true \
-lobatto $LOBATTO \
-adaptivity false \
-error_level $ERRORLV \
-error_type $PRIORI_TYPE \
-add_incident_wave false 2>&1 |
echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$9,$16,$21}' | tee -a -i p_refinement_adaptivity_${ADAPTIVITY}_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo "   "
# echo "                       begin error calculation           "
echo " check if data is correct  "
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$25,$29,$30}' | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#BEGIN error calculation   awk '{print $6,$12}'  grep first 10 lines |head -10|
#grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =\|Memory usage =" | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt

# mpirun -np 2 ./error_norm \
# -my_file ./fe_solution.h5m \
# -my_is_partitioned false \
# -norm_type $ERROR_TYPE \
# -relative_error false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 1 \
# -my_max_post_proc_ref_level 0 \
# -save_postproc_mesh false 2>&1 | echo $(grep --line-buffered -i " realtive error ") | awk '{print $6,$12}' | tee -a -i p_refinement_adaptivity_${ADAPTIVITY}_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#


mbpart -t -p PARTKWAY 8 dense_droplet.cub dense_droplet.h5m


############### START H REFINEMENT calculation ####################

# echo "Start H analytical calculation with $BEST_NB_PRC processes, mesh h${COUNT} and order 8 and wavenumber $WAVE_NUMBER, error level $ERRORLV"
#
# #BEGIN analytical solution
# mpirun -np $BEST_NB_PRC ./best_approximation \
# -my_file dense_droplet.h5m \
# -my_is_partitioned true \
# -wave_number $WAVE_NUMBER \
# -wave_direction 1,0,0 \
# -wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
# -material_coefficient1 -1348.8 \
# -analytical_solution_type hard_droplet_scatter_wave \
# -save_postproc_mesh false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 2 \
# -my_max_post_proc_ref_level 0 \
# -add_incident_wave false \
# -lobatto true 2>&1 |
# echo $(grep --line-buffered -i "Residual norm\|Time =" | sort | uniq) | tee -a -i h_refinement_test_best_approximation_k_errorlv_${ERRORLV}.txt


echo "Start H refinement numerical calculation with $NB_PROC processes, order = 2,  H Refinement mesh dense_droplet.h5m and order 2 and wavenumber $WAVE_NUMBER, error level $ERRORLV"
mpirun -np $NB_PROC ./fe_approximation \
-my_file dense_droplet.h5m \
-my_is_partitioned true \
-wave_number $WAVE_NUMBER \
-wave_direction 1,0,0 \
-wave_oscilation_direction -0.3756,-0.9268,3.2051e-09 \
-material_coefficient1 -1348.8 \
-save_postproc_mesh false \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package mumps \
-ksp_monitor \
-my_order 2 \
-my_max_post_proc_ref_level 0 \
-amplitude_of_incident_wave 1 \
-duffy true \
-lobatto $LOBATTO \
-adaptivity false \
-add_incident_wave false 2>&1 |
echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|max_value_order_p\|min_value_order_p\|avg_value_order_p\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$9,$16,$21}' | tee -a -i h_refinement_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
echo "   "
# echo "                       begin error calculation           "
echo " check if data is correct  "
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | awk '{print $2,$7,$17,$24,$25,$29,$30}' | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#echo $(grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|nz_used\|Time =\|Memory usage =" | sort | uniq) | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt
#BEGIN error calculation   awk '{print $6,$12}'  grep first 10 lines |head -10|
#grep --line-buffered -i "Problem ACOUSTIC_PROBLEM Nb. rows\|Time =\|Memory usage =" | tee -a -i hard_droplet_k_${WAVE_NUMBER}_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt

# mpirun -np 2 ./error_norm \
# -my_file ./fe_solution.h5m \
# -my_is_partitioned false \
# -norm_type $ERROR_TYPE \
# -relative_error false \
# -ksp_type fgmres \
# -pc_type lu \
# -pc_factor_mat_solver_package superlu_dist \
# -ksp_monitor \
# -my_order 1 \
# -my_max_post_proc_ref_level 0 \
# -save_postproc_mesh false 2>&1 | echo $(grep --line-buffered -i " realtive error ") | awk '{print $6,$12}' | tee -a -i h_refinement_error_hard_droplet_k_lobatto_${LOBATTO}_errorlv_${ERRORLV}_type_${ERROR_TYPE}.txt



# for ODISP in "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12"
# do


# done

done

done

done

#SAVE the results to different directory
# mkdir ${DIR_NAME}_${ERROR_TYPE}_droplet_p
# mv norm_error_k*_order_*.vtk ./${DIR_NAME}_${ERROR_TYPE}_droplet_p


# done
#foo="Hello"
#foo="$foo World"
#echo $foo
#> Hello World
